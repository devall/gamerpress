Forum from [LiveStreet CMS](http://livestreetcms.com/ "LiveStreet CMS")
=======================================================================

ATTENTION
---------

**To use this plugin requires Livestreet CMS version 0.5.1 or later.**

Simple Install
--------------

To install forum do this:

* [Download the latest build](https://github.com/Xmk/forum/zipball/master "Download as zip")
* Unpack archive
* Rename dir to `forum`
* Upload to FTP, in the plugins folder
* Activate plugin

License
-------
[ ![CC BY-NC](http://i.creativecommons.org/l/by-nc/3.0/88x31.png "CC BY-NC") ](http://creativecommons.org/licenses/by-nc/3.0/ "CC BY-NC")