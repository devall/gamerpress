DROP TABLE IF EXISTS `prefix_forum_read`;
DROP TABLE IF EXISTS `prefix_forum_post`;
DROP TABLE IF EXISTS `prefix_forum_topic`;
DROP TABLE IF EXISTS `prefix_forum_moderator`;
DROP TABLE IF EXISTS `prefix_forum_moderator_rel`;
DROP TABLE IF EXISTS `prefix_forum`;