--
-- Database Gamepress version 0.1
--

-- --------------------------------------------------------

--
-- Структура таблицы `prefix_game`
--

CREATE TABLE IF NOT EXISTS `prefix_game` (
  `game_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_owner_id` int(11) unsigned NOT NULL,
  `game_title` varchar(200) NOT NULL,
  `game_description` text NOT NULL,
  `game_genres` varchar(250) NOT NULL COMMENT 'genres separated by a comma',
  `game_developers` varchar(250) NOT NULL COMMENT 'developers separated by a comma',
  `game_publishers` varchar(250) NOT NULL COMMENT 'publishers separated by a comma',
  `game_date_release` datetime DEFAULT NULL,
  `game_date_add` datetime NOT NULL,
  `game_date_edit` datetime DEFAULT NULL,
  `game_rating` float(9,3) NOT NULL DEFAULT '0.000',
  `game_count_vote` int(11) unsigned NOT NULL DEFAULT '0',
  `game_count_user` int(11) unsigned NOT NULL DEFAULT '0',
  `game_count_topic` int(10) unsigned NOT NULL DEFAULT '0',
--  `game_limit_rating_topic` float(9,3) NOT NULL DEFAULT '0.000',
  `game_link` varchar(255) DEFAULT NULL,
  `game_url` varchar(200) DEFAULT NULL,
  `game_avatar` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`game_id`),
  KEY `user_owner_id` (`user_owner_id`),
  KEY `game_url` (`game_url`),
  KEY `game_title` (`game_title`),
  KEY `game_count_topic` (`game_count_topic`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Структура таблицы `prefix_game_user`
--

CREATE TABLE IF NOT EXISTS `prefix_game_user` (
  `game_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `user_role` int(3) DEFAULT '1',
  UNIQUE KEY `game_id_user_id_uniq` (`game_id`,`user_id`),
  KEY `game_id` (`game_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `prefix_album` (
  `album_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `album_user_id` int(11) unsigned NOT NULL,
  `album_title` varchar(200) NOT NULL,
  `album_description` text NOT NULL,
  `album_access` enum('personal','open','friend') NOT NULL DEFAULT 'open',
  `album_type` enum('photo','video') NOT NULL,
  `album_target_id` int(11) unsigned NOT NULL,
  `album_date_add` datetime NOT NULL,
  `album_date_edit` datetime NOT NULL,
  `album_cover_image_id` int(11) unsigned DEFAULT NULL,
  `image_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`album_id`),
  KEY `album_user_id` (`album_user_id`),
  KEY `album_cover_image_id` (`album_cover_image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `prefix_game_video` (
  `video_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `album_id` int(11) unsigned NOT NULL,
  `video_description` text NOT NULL,
  `video_tags` varchar(200) NOT NULL,
  `video_filename` varchar(255) NOT NULL,
  `video_date_add` datetime NOT NULL,
  `video_date_edit` datetime DEFAULT NULL,
  `video_count_comment` int(11) NOT NULL DEFAULT '0',
  `video_rating` float(9,3) NOT NULL DEFAULT '0.000',
  `video_count_vote` int(11) unsigned NOT NULL DEFAULT '0',
  `video_count_favourite` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`video_id`),
  KEY `album_id` (`album_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE utf8_general_ci ;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `prefix_game`
--
ALTER TABLE `prefix_game`
  ADD CONSTRAINT `prefix_game_fk` FOREIGN KEY (`user_owner_id`) REFERENCES `prefix_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `prefix_game_user`
--
ALTER TABLE `prefix_game_user`
  ADD CONSTRAINT `prefix_game_user_fk` FOREIGN KEY (`game_id`) REFERENCES `prefix_game` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prefix_game_user_fk1` FOREIGN KEY (`user_id`) REFERENCES `prefix_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `prefix_topic` MODIFY `blog_id` int(11) unsigned DEFAULT NULL;
ALTER TABLE `prefix_topic_tag` MODIFY `blog_id` int(11) unsigned DEFAULT NULL;
ALTER TABLE `prefix_topic` ADD `game_id` int(11) unsigned DEFAULT NULL AFTER `blog_id`;
ALTER TABLE `prefix_topic_tag` ADD `game_id` int(11) unsigned DEFAULT NULL AFTER `blog_id`;

ALTER TABLE `prefix_topic`
  ADD CONSTRAINT `prefix_topic_fk3` FOREIGN KEY (`game_id`) REFERENCES `prefix_game` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `prefix_topic_tag`
  ADD CONSTRAINT `prefix_topic_tag_fk3` FOREIGN KEY (`game_id`) REFERENCES `prefix_game` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO `prefix_forum` (`forum_id`, `forum_parent_id`, `forum_title`, `forum_description`, `forum_url`, `forum_moder`, `forum_sort`, `forum_can_post`, `forum_quick_reply`, `forum_type`, `forum_password`, `forum_limit_rating_topic`, `forum_redirect_url`, `forum_redirect_on`, `forum_redirect_hits`, `forum_count_topic`, `forum_count_post`, `last_post_id`) VALUES
(2, 15, 'Тестовый форум', 'Тестовый форум для проверки возможностей данного плагина', 'tester', '', 1, 0, 1, 1, '', -5.000, '', 0, 0, 1, 0, 1),
(3, 0, 'О сайте', NULL, 'about_site', '', 0, 1, 1, 1, NULL, 0.000, '', 0, 0, 0, 0, NULL),
(4, 3, 'Наш сайт', 'Обсуждаем сайт и форум, предложения, пожелания и замечания', 'our_site', '', 1, 0, 1, 1, '', -5.000, '', 0, 0, 0, 0, NULL),
(5, 0, 'Игры', NULL, 'games', '', 0, 1, 1, 1, NULL, 0.000, '', 0, 0, 0, 0, NULL),
(6, 5, 'Интересное', 'Игровые новости от пользователей, ставим ссылки на источник! Копипаст запрещен!', 'good', '', 1, 0, 1, 1, '', -5.000, '', 0, 0, 0, 0, NULL),
(7, 5, 'Все игры', 'Обсуждаем все игры PC, XBOX360, PS3 и другие!', 'all_games', '', 1, 0, 1, 1, '', -5.000, '', 0, 0, 0, 0, NULL),
(8, 5, 'Гайды и прохождения', 'Не забываем ставить ссылку на источник, если автор не вы!', 'guides', '', 2, 0, 1, 1, '', -5.000, '', 0, 0, 0, 0, NULL),
(9, 0, 'Железо', NULL, 'hardware', '', 0, 1, 1, 1, NULL, 0.000, '', 0, 0, 0, 0, NULL),
(10, 9, 'PC', 'Персональный компьютер', 'pc', '', 1, 0, 1, 1, '', -5.000, '', 0, 0, 0, 0, NULL),
(11, 9, 'XBOX 360', 'Игровая приставка компании Microsoft', 'xbox360', '', 1, 0, 1, 1, '', -5.000, '', 0, 0, 0, 0, NULL),
(12, 9, 'PlayStation 3', 'Игровая приставка компании Sony', 'ps3', '', 2, 0, 1, 1, '', -5.000, '', 0, 0, 0, 0, NULL),
(13, 9, 'Другое', 'Другие платформы и консоли, предназначенные для игр', 'others', '', 3, 0, 1, 1, '', -5.000, '', 0, 0, 0, 0, NULL),
(14, 0, 'Разное', NULL, 'other', '', 0, 1, 1, 1, NULL, 0.000, '', 0, 0, 0, 0, NULL),
(15, 14, 'Болталка', 'Свободное общение', 'flood', '', 1, 0, 1, 1, '', -5.000, '', 0, 0, 0, 0, NULL);
