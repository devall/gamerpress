<?php
/**
 * Конфиг
 */

$config = array();

// Определение экшенов
Config::Set('router.page.gamerpress', 'PluginGamerpress_ActionGamerpress');
Config::Set('router.page.games', 'PluginGamerpress_ActionGames');
Config::Set('router.page.game', 'PluginGamerpress_ActionGame');
Config::Set('router.page.review', 'PluginGamerpress_ActionReview');
Config::Set('router.page.video', 'PluginGamerpress_ActionVideo');

// Параметры модулей
$config['module']['game']['per_page'] = 30;
$config['module']['game']['users_per_page'] = 30;
$config['module']['game']['avatar_size'] = array(100,64,48,24,0); // Список размеров аватаров у игры. 0 - исходный размер

/**
 * Настройки ACL(Access Control List — список контроля доступа)
 */
$config['acl']['create']['game']['rating']                =  1;  // порог рейтинга при котором юзер может создать игры
$config['acl']['vote']['game']['rating']                  = -5;  // порог рейтинга при котором юзер может голосовать за игру

// Таблицы
$config['db']['table']['game'] = '___db.table.prefix___game';
$config['db']['table']['game_user'] = '___db.table.prefix___game_user';

Config::Set('block.rule_game', array(
  'action'  => array(
      'game' => array('{game}')
    ),
  'blocks'  => array(
      'right' => array(
        'gameInfo'=>array('params'=>array('plugin'=>'gamerpress'),'priority'=>100)
      )
    ),
  'clear' => false,
));

/**
 * Вырубаем этот блок, т.к у нас нет коллективных блогов на которые можно подписаться.
 */
Config::Set('block.userfeedBlogs', array());
/**
 * И врубаем блок на подписку игр
 */
Config::Set('block.userfeedGames', array(
  'action'  => array('feed'),
  'blocks'  => array(
      'right' => array(
          'userfeedGames'=> array('params'=>array('plugin'=>'gamerpress'),'priority'=>100)
      )
  )
));

Config::Set('head.rules.gamerpress', array(
    'path'=> array(
        Config::Get('path.root.web').'/game',
    ),
    'js' => array(
        'include' => array(
            "___path.static.root___/plugins/gamerpress/classes/libs/jRating/jquery/jRating.jquery.js",
        )
    ),
    'css' => array(
        'include' => array(
            "___path.static.root___/plugins/gamerpress/classes/libs/jRating/jquery/jRating.jquery.css",
        )
    ),
));

// Javascript'ы плагина
$aJs = Config::Get('head.default.js');
$aJs[] = '___path.static.root___/plugins/gamerpress/templates/skin/default/js/template.js';
$aJs[] = '___path.static.root___/plugins/gamerpress/templates/skin/default/js/game.js';
$aJs[] = '___path.static.root___/plugins/gamerpress/templates/skin/default/js/vote.js';
$aJs[] = '___path.static.root___/plugins/gamerpress/templates/skin/default/js/comments.js';
$aJs[] = '___path.static.root___/plugins/gamerpress/templates/skin/default/js/infobox.js';
Config::Set('head.default.js',$aJs);

Config::Set('compress.css.merge',false);       // указывает на необходимость слияния файлов по указанным блокам.
Config::Set('compress.js.merge',false);        // указывает на необходимость слияния файлов по указанным блокам.

return $config;
