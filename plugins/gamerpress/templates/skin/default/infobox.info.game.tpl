{* <img src="{$oGame->getAvatarPath(48)}" alt="avatar" /><br /><br /> *}

<ul class="blog-info">
  <li><span>{$aLang.plugin.gamerpress.infobox_game_create}</span> <strong>{date_format date=$oGame->getDateAdd() format="j F Y"}</strong></li>
  <li><span>{$aLang.plugin.gamerpress.infobox_game_topics}</span> <strong>{$oGame->getCountTopic()}</strong></li>
  <li><span><a href="{$oGame->getUrlFull()}users/">{$aLang.plugin.gamerpress.infobox_game_users}</a></span> <strong>{$oGame->getCountUser()}</strong></li>

  <li class="rating"><span>{$aLang.plugin.gamerpress.infobox_game_rating}</span> <strong>{$oGame->getRating()}</strong></li>
</ul>


{if $oTopicLast}
  {$aLang.plugin.gamerpress.infobox_game_topic_last}:<br/>
  <a href="{$oTopicLast->getUrl()}" class="infobox-topic">{$oTopicLast->getTitle()|escape:'html'}</a>
{/if}

<br/>
<br/>
<div class="infobox-actions">
  <a href="{$oGame->getUrlFull()}">{$aLang.plugin.gamerpress.infobox_game_url}</a><br/>
  <a href="{router page='rss'}blog/{$oGame->getUrl()}/">{$aLang.plugin.gamerpress.infobox_game_rss}</a>
</div>
