{include file='header.tpl' menu='game'}

<div class="show-video clearfix">
  <div class="video">
    <iframe width="560" height="315" src="http://www.youtube.com/embed/MmB9b5njVbA" frameborder="0" allowfullscreen></iframe>
  </div>

  <div class="video-info">
    <ul>
      <li><i class="icon icon-heart"></i> </li>
      <li><i class="icon icon-share"></i> </li>
      <li><i class="icon icon-share-alt"></i> </li>
      <li><i class="icon icon-eye-open"></i> 8,784</li>
    </ul>
  </div>
</div>

{include
  file='comment_tree.tpl'
  iTargetId=$oVideo->getId()
  iAuthorId=$oVideo->getUserId()
  sAuthorNotice=$aLang.topic_author
  sTargetType='video'
  iCountComment=$oVideo->getCountComment()
  sDateReadLast=$oVideo->getDateRead()
  bAllowNewComment=$oVideo->getForbidComment()
  sNoticeNotAllow=$aLang.topic_comment_notallow
  sNoticeCommentAdd=$aLang.topic_comment_add
  bAllowSubscribe=true
  oSubscribeComment=$oVideo->getSubscribeNewComment()
  aPagingCmt=$aPagingCmt}

{include file='footer.tpl'}
