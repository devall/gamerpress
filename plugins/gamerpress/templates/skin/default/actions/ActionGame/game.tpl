{include file='header.tpl' menu='game'}
{assign var="oUserOwner" value=$oGame->getOwner()}
{assign var="oVote" value=$oGame->getVote()}

{if $oUserCurrent and $oUserCurrent->isAdministrator()}
  <div id="game_delete_form" class="modal">
    <header class="modal-header">
      <h3>{$aLang.blog_admin_delete_title}</h3>
      <a href="#" class="close jqmClose"></a>
    </header>


    <form action="{router page='game'}delete/{$oGame->getId()}/" method="POST" class="modal-content">
      <p><label for="topic_move_to">{$aLang.blog_admin_delete_move}:</label>
      <select name="topic_move_to" id="topic_move_to" class="input-width-full">
        <option value="-1">{$aLang.blog_delete_clear}</option>
        {if $aGames}
          <optgroup label="{$aLang.blogs}">
            {foreach from=$aGames item=oGameDelete}
              <option value="{$oGameDelete->getId()}">{$oGameDelete->getTitle()|escape:'html'}</option>
            {/foreach}
          </optgroup>
        {/if}
      </select></p>

      <input type="hidden" value="{$LIVESTREET_SECURITY_KEY}" name="security_ls_key" />
      <button type="submit"  class="button button-primary">{$aLang.blog_delete}</button>
    </form>
  </div>
{/if}


<div class="game-top">
  <h2 class="page-header">{$oGame->getTitle()|escape:'html'} {if $oGame->getType()=='close'} <i title="{$aLang.blog_closed}" class="icon-synio-topic-private"></i>{/if}</h2>
</div>

<div class="game-media">
  <div class="game-video">
    <div class="video-header">Видео</div>
    <div class="video-slider">
      <div class="video-thumbnail">
        <a href="{$sMenuSubGameUrl}video/1" title="Заголовок">
          <img src="http://img.youtube.com/vi/MmB9b5njVbA/default.jpg"/>
          <img src="{$aTemplateWebPathPlugin.gamerpress}images/play_hover.png" class="hover"/>
        </a>
      </div>
      <div class="video-thumbnail">
        <a href="{$sMenuSubGameUrl}video/2" title="Заголовок">
          <img src="http://img.youtube.com/vi/k5uhbCxM6Ns/default.jpg"/>
          <img src="{$aTemplateWebPathPlugin.gamerpress}images/play_hover.png" class="hover"/>
        </a>
      </div>
    </div>
  </div>
  <div class="game-image">
    <div class="image-header">Фото</div>
    <div class="image-slider">
      <div class="image-thumbnail">
        <a href="{$sMenuSubGameUrl}photo/1" title="Заголовок">
          <img src="http://img.youtube.com/vi/MmB9b5njVbA/default.jpg"/>
        </a>
      </div>
      <div class="image-thumbnail">
        <a href="{$sMenuSubGameUrl}photo/2" title="Заголовок">
          <img src="http://img.youtube.com/vi/MmB9b5njVbA/default.jpg"/>
        </a>
      </div>
    </div>
  </div>
</div>


<div class="game">
  <div class="game-inner">
    <div class="game-content">
      <p class="game-description">{$oGame->getDescription()}</p>
    </div>
  </div>

  <footer class="game-footer" id="game-footer">
    {if $oUserCurrent and $oUserCurrent->getId()!=$oGame->getOwnerId()}
      <button type="submit"  class="button button-small" id="button-game-join-second-{$oGame->getId()}" data-button-additional="button-game-join-first-{$oGame->getId()}" data-only-text="1" onclick="ls.game.toggleJoin(this, {$oGame->getId()}); return false;">{if $oGame->getUserIsJoin()}{$aLang.blog_leave}{else}{$aLang.blog_join}{/if}</button>
    {/if}
    <a href="#" class="link-dotted" onclick="ls.game.toggleInfo(); return false;">{$aLang.plugin.gamerpress.game_expand_info}</a>
    <a href="{router page='rss'}game/{$oGame->getUrl()}/" class="rss link-dotted">RSS</a>


    <div class="admin">
      {$aLang.blogs_owner} —
      <a href="{$oUserOwner->getUserWebPath()}"><img src="{$oUserOwner->getProfileAvatarPath(24)}" alt="avatar" class="avatar" /></a>
      <a href="{$oUserOwner->getUserWebPath()}">{$oUserOwner->getLogin()}</a>
    </div>
  </footer>
</div>


<div class="game" id="game" style="display: none">
  <div class="game-inner">
    <div class="game-content">
      <ul class="game-info">
        {*
        <li><span>{$aLang.infobox_blog_create}</span> <strong>{date_format date=$oGame->getDateAdd() format="j F Y"}</strong></li>
        *}
        <li><span>{$aLang.infobox_blog_topics}</span> <strong>{$oGame->getCountTopic()}</strong></li>
        <li><span><a href="{$oGame->getUrlFull()}users/">{$aLang.infobox_blog_users}</a></span> <strong>{$iCountGameUsers}</strong></li>
        {*
        <li class="rating"><span>{$aLang.infobox_blog_rating}</span> <strong>{$oGame->getRating()}</strong></li>
        *}
      </ul>


      <strong>{$aLang.blog_user_administrators} ({$iCountGameAdministrators})</strong><br />
      <span class="user-avatar">
        <a href="{$oUserOwner->getUserWebPath()}"><img src="{$oUserOwner->getProfileAvatarPath(24)}" alt="avatar" /></a>
        <a href="{$oUserOwner->getUserWebPath()}">{$oUserOwner->getLogin()}</a>
      </span>
      {if $aGameAdministrators}
        {foreach from=$aGameAdministrators item=oGameUser}
          {assign var="oUser" value=$oGameUser->getUser()}
          <span class="user-avatar">
            <a href="{$oUser->getUserWebPath()}"><img src="{$oUser->getProfileAvatarPath(24)}" alt="avatar" /></a>
            <a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a>
          </span>
        {/foreach}
      {/if}<br /><br />


      <strong>{$aLang.blog_user_moderators} ({$iCountGameModerators})</strong><br />
      {if $aGameModerators}
        {foreach from=$aGameModerators item=oGameUser}
          {assign var="oUser" value=$oGameUser->getUser()}
          <span class="user-avatar">
            <a href="{$oUser->getUserWebPath()}"><img src="{$oUser->getProfileAvatarPath(24)}" alt="avatar" /></a>
            <a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a>
          </span>
        {/foreach}
      {else}
        <span class="notice-empty">{$aLang.blog_user_moderators_empty}</span>
      {/if}



      {if $oUserCurrent and ($oUserCurrent->getId()==$oGame->getOwnerId() or $oUserCurrent->isAdministrator() or $oGame->getUserIsAdministrator() )}
        <br /><br />
        <ul class="actions">
          <li>
            <a href="{router page='game'}edit/{$oGame->getId()}/" title="{$aLang.blog_edit}" class="edit">{$aLang.blog_edit}</a></li>
            {if $oUserCurrent->isAdministrator()}
              <li><a href="#" title="{$aLang.blog_delete}" id="game_delete_show" class="delete">{$aLang.blog_delete}</a>
            {else}
              <a href="{router page='game'}delete/{$oGame->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.blog_delete}" onclick="return confirm('{$aLang.blog_admin_delete_confirm}');" >{$aLang.blog_delete}</a>
            {/if}
          </li>
        </ul>
      {/if}
    </div>
  </div>

</div>

<div class="nav-menu-wrapper">
  <ul class="nav nav-pills">
    <li {if $sMenuSubItemSelect=='review'}class="active"{/if}><a href="{$sMenuSubGameUrl}review/">{$aLang.plugin.gamerpress.game_menu_collective_review}</a></li>
    <li {if $sMenuSubItemSelect=='good'}class="active"{/if}><a href="{$sMenuSubGameUrl}">{$aLang.blog_menu_collective_good}</a></li>
    <li {if $sMenuSubItemSelect=='new'}class="active"{/if}><a href="{$sMenuSubGameUrl}newall/">{$aLang.blog_menu_collective_new}</a>{if $iCountTopicsGameNew>0} <a href="{$sMenuSubGameUrl}new/" class="new">+{$iCountTopicsGameNew}</a>{/if}</li>
    <li {if $sMenuSubItemSelect=='discussed'}class="active"{/if}><a href="{$sMenuSubGameUrl}discussed/">{$aLang.blog_menu_collective_discussed}</a></li>
    <li {if $sMenuSubItemSelect=='top'}class="active"{/if}><a href="{$sMenuSubGameUrl}top/">{$aLang.blog_menu_collective_top}</a></li>
  </ul>

  {if $sPeriodSelectCurrent}
    <ul class="nav nav-pills nav-pills-dropdown">
      <li {if $sPeriodSelectCurrent=='1'}class="active"{/if}><a href="{$sPeriodSelectRoot}?period=1">{$aLang.blog_menu_top_period_24h}</a></li>
      <li {if $sPeriodSelectCurrent=='7'}class="active"{/if}><a href="{$sPeriodSelectRoot}?period=7">{$aLang.blog_menu_top_period_7d}</a></li>
      <li {if $sPeriodSelectCurrent=='30'}class="active"{/if}><a href="{$sPeriodSelectRoot}?period=30">{$aLang.blog_menu_top_period_30d}</a></li>
      <li {if $sPeriodSelectCurrent=='all'}class="active"{/if}><a href="{$sPeriodSelectRoot}?period=all">{$aLang.blog_menu_top_period_all}</a></li>
    </ul>
  {/if}
</div>


{include file="`$aTemplatePathPlugin.gamerpress`game_topic_list.tpl"}


{include file='footer.tpl'}
