{if $sEvent=='add'}
  {include file='header.tpl' menu_content='create'}
{else}
  {include file='header.tpl'}
  {include file="`$aTemplatePathPlugin.gamerpress`menu.game_edit.tpl"}
{/if}


{include file='editor.tpl' sImgToLoad='game_description' sSettingsTinymce='ls.settings.getTinymceComment()' sSettingsMarkitup='ls.settings.getMarkitupComment()'}


<form method="post" enctype="multipart/form-data" class="wrapper-content">
  <input type="hidden" name="security_ls_key" value="{$LIVESTREET_SECURITY_KEY}" />


  <p><label for="game_title">{$aLang.plugin.gamerpress.game_create_title}:</label>
  <input type="text" id="game_title" name="game_title" value="{$_aRequest.game_title}" class="input-text input-width-full" />
  <small class="note">{$aLang.plugin.gamerpress.game_create_title_notice}</small></p>


  <p>
    {if $oGameEdit and $oGameEdit->getAvatar()}
      <div class="avatar-edit">
        {foreach from=$oConfig->GetValue('plugin.gamerpress.module.game.avatar_size') item=iSize}
          {if $iSize}<img src="{$oGameEdit->getAvatarPath({$iSize})}">{/if}
        {/foreach}

        <label><input type="checkbox" id="avatar_delete" name="avatar_delete" value="on" class="input-checkbox"> {$aLang.blog_create_avatar_delete}</label>
      </div>
    {/if}

    <label for="avatar">{$aLang.blog_create_avatar}:</label>
    <input type="file" name="avatar" id="avatar">
  </p>
  <p><label for="game_genres">{$aLang.plugin.gamerpress.game_create_genres}:</label>
  <input type="text" id="game_genres" name="game_genres" value="{$_aRequest.game_genres}" class="input-text input-width-300 autocomplete-genres-sep" />
  <small class="note">{$aLang.plugin.gamerpress.game_create_genres_notice}</small></p>

  <p><label for="game_developers">{$aLang.plugin.gamerpress.game_create_developers}:</label>
  <input type="text" id="game_developers" name="game_developers" value="{$_aRequest.game_developers}" class="input-text input-width-300" />
  <small class="note">{$aLang.plugin.gamerpress.game_create_developers_notice}</small></p>

  <p><label for="game_publishers">{$aLang.plugin.gamerpress.game_create_publishers}:</label>
  <input type="text" id="game_publishers" name="game_publishers" value="{$_aRequest.game_publishers}" class="input-text input-width-300" />
  <small class="note">{$aLang.plugin.gamerpress.game_create_publishers_notice}</small></p>

  <p><label for="game_create_date_release">{$aLang.plugin.gamerpress.game_create_date_release}:</label>
  <input type="text" id="game_create_date_release" name="game_create_date_release" value="{$_aRequest.game_create_date_release}" class="input-text input-width-300" />
  <small class="note">{$aLang.plugin.gamerpress.game_create_date_release_notice}</small></p>

  <p><label for="game_link">{$aLang.plugin.gamerpress.game_create_link}:</label>
  <input type="text" id="game_link" name="game_link" value="{$_aRequest.game_link}" class="input-text input-width-300" />
  <small class="note">{$aLang.plugin.gamerpress.game_create_link_notice}</small></p>

  <p><label for="game_url">{$aLang.plugin.gamerpress.game_create_url}:</label>
  <input type="text" id="game_url" name="game_url" value="{$_aRequest.game_url}" class="input-text input-width-full" {if $_aRequest.game_id and !$oUserCurrent->isAdministrator()}disabled{/if} />
  <small class="note">{$aLang.plugin.gamerpress.game_create_url_notice}</small></p>


  <p><label for="game_description">{$aLang.plugin.gamerpress.game_create_description}:</label>
  <textarea name="game_description" id="game_description" rows="15" class="input-text input-width-full mce-editor markitup-editor">{$_aRequest.game_description}</textarea>
  <small class="note">{$aLang.plugin.gamerpress.game_create_description_notice}</small></p>

{*
  <p><label for="game_limit_rating_topic">{$aLang.plugin.gamerpress.game_create_rating}:</label>
  <input type="text" id="game_limit_rating_topic" name="game_limit_rating_topic" value="{$_aRequest.game_limit_rating_topic}" class="input-text input-width-100" />
  <small class="note">{$aLang.plugin.gamerpress.game_create_rating_notice}</small></p>
*}

  <button type="submit"  name="submit_game_add" class="button button-primary">{$aLang.plugin.gamerpress.game_create_submit}</button>
</form>


{include file='footer.tpl'}
