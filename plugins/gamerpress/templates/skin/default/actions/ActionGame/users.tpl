{include file='header.tpl'}



<h2 class="page-header">{$aLang.plugin.gamerpress.game_user_readers_all} ({$iCountGameUsers}): <a href="{$oGame->getUrlFull()}">{$oGame->getTitle()|escape:'html'}</a></h2>

{if $aGameUsers}
  {assign var="aUsersList" value=[]}
  {foreach from=$aGameUsers item=oGameUser}
    {$aUsersList[]=$oGameUser->getUser()}
  {/foreach}
  {include file='user_list.tpl' aUsersList=$aUsersList sUsersRootPage=$sUsersRootPage}
{else}
  {$aLang.plugin.gamerpress.game_user_readers_empty}
{/if}



{include file='footer.tpl'}
