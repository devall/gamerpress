{include file='header.tpl' sMenuHeadItemSelect="games"}

<h2 class="page-header">{$aLang.plugin.gamerpress.games}</h2>

<form action="" method="POST" id="form-games-search" onsubmit="return false;" class="search-item">
  <div class="search-input-wrapper">
    <input type="text" placeholder="{$aLang.plugin.gamerpress.games_search_title_hint}" autocomplete="off" name="game_title" class="input-text" value="" onkeyup="ls.timer.run(ls.game.searchGames,'games_search',['form-games-search'],1000);">
    <div class="input-submit" onclick="jQuery('#form-games-search').submit()"></div>
  </div>
</form>

<div id="games-list-search" style="display:none;"></div>

<div id="games-list-original">
  {router page='games' assign=sGamesRootPage}
  {include file="`$aTemplatePathPlugin.gamerpress`/game_list.tpl" bGamesUseOrder=true sGamesRootPage=$sGamesRootPage}
  {include file='paging.tpl' aPaging=$aPaging}
</div>

{include file='footer.tpl'}
