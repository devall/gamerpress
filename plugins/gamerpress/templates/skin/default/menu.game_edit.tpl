<h2 class="page-header">{$aLang.plugin.gamerpress.game_admin}: <a href="{$oGameEdit->getUrlFull()}">{$oGameEdit->getTitle()|escape:'html'}</a></h2>

<ul class="nav nav-pills">
  <li {if $sMenuItemSelect=='content'}class="active"{/if}><a href="{router page='game'}edit/{$oGameEdit->getId()}/">{$aLang.plugin.gamerpress.game_admin_content}</a></li>
  <li {if $sMenuItemSelect=='admin'}class="active"{/if}><a href="{router page='game'}admin/{$oGameEdit->getId()}/">{$aLang.plugin.gamerpress.game_admin_users}</a></li>
</ul>
