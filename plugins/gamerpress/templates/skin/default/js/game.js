var ls = ls || {};

/**
* JS функционал для игр
*/
ls.game = (function($){

  /**
  * Вступить или покинуть игру
  */
  this.toggleJoin = function(obj, idGame){
    var url = aRouter['game']+'ajaxgamejoin/';
    var params = {idGame: idGame};

    ls.ajax(url,params,function(result) {
      if (result.bStateError) {
        ls.msg.error(null, result.sMsg);
      } else {
        obj = $(obj);
        ls.msg.notice(null, result.sMsg);

        var text = result.bState
          ? ls.lang.get('plugin.gamerpress.game_leave')
          : ls.lang.get('plugin.gamerpress.game_join')
        ;

        obj.children('span').empty().text(text);
        obj.toggleClass('active');

        $('#game_user_count_'+idGame).text(result.iCountUser);
      }
    });
  };

  /**
   * Поиск игр
   */
  this.searchGames = function(form) {
    var url = aRouter['games']+'ajax-search/';
    var inputSearch=$('#'+form).find('input');
    inputSearch.addClass('loader');

    ls.ajaxSubmit(url, form, function(result){
      inputSearch.removeClass('loader');
      if (result.bStateError) {
        $('#games-list-search').hide();
        $('#games-list-original').show();
      } else {
        $('#games-list-original').hide();
        $('#games-list-search').html(result.sText).show();
      }
    });
  };

  /**
   * Показать подробную информацию о блоге
   */
  this.toggleInfo = function() {
    if ($('#game').is(':hidden')) {
      $('#game-mini').hide();
      $('#game').show();
    } else {
      $('#game-mini').show();
      $('#game').hide();
    }

    return false;
  };

  return this;
}).call(ls.game || {},jQuery);
