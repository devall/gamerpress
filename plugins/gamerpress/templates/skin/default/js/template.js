jQuery(document).ready(function($){

  // Автокомплит
  ls.autocomplete.add($(".autocomplete-genres-sep"), aRouter['ajax']+'autocompleter/genre/', true);

  // Всплывающие окна
  $('#blog_delete_form').jqm({trigger: '#blog_delete_show'});

  if(jQuery().jRating) {
    $('.game-rating').jRating({
      bigStarsPath: ls.registry.get('rating-icons-path')+'icons/stars.png',
      smallStarsPath: ls.registry.get('rating-icons-path')+'icons/small.png',
      phpPath: aRouter.ajax+'vote/game',
      isDisabled: parseInt(ls.registry.get('rating-disable')) == 1 ? true : false,
      type: 'small',
      length: 10,
      rateMax: 10,
      decimalLength: 1,
      step: true
    });
  }

  // Дополняем ссылки у встроенных видео
  $("iframe").each(function(){
    var ifr_source = $(this).attr('src');

    if(ifr_source) {
      var params = "vq=hd1080&hd=1&rel=0&autohide=1&showinfo=0";

      if (ifr_source.indexOf('?') != -1)
        $(this).attr('src',ifr_source+'&'+params);
      else
        $(this).attr('src',ifr_source+'?'+params);
    }
  });

  // инизиализация блоков
  ls.blocks.init('gameInfo');
});
