var ls = ls || {};

ls.infobox.showInfoGame = function(oLink,iGameId) {
    if (this.hideIfShow(oLink)) {
      return false;
    }

    this.showProcess(oLink);
    var url = aRouter['ajax']+'infobox/info/game/';
    var params = {iGameId: iGameId};
    ls.ajax(url, params, function(result) {
      if (result.bStateError) {
        ls.msg.error(null, result.sMsg);
        this.hide(oLink);
      } else {
        this.show(oLink,result.sText);
      }
    }.bind(this));
    return false;
};
