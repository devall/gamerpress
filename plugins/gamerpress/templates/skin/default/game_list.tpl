<table class="table table-games" cellspacing="0">
  {if $bGamesUseOrder}
    <thead>
      <tr>
        <th class="cell-info">&nbsp;</th>
        <th class="cell-name cell-tab">
          <div class="cell-tab-inner {if $sGameOrder=='game_title'}active{/if}"><a href="{$sGamesRootPage}?order=game_title&order_way={if $sGameOrder=='game_title'}{$sGameOrderWayNext}{else}{$sGameOrderWay}{/if}" {if $sGameOrder=='game_title'}class="{$sGameOrderWay}"{/if}><span>{$aLang.plugin.gamerpress.games_title}</span></a></div>
        </th>

        {if $oUserCurrent}
          <th class="cell-join">&nbsp;</th>
        {/if}

        <th class="cell-readers cell-tab">
          <div class="cell-tab-inner {if $sGameOrder=='game_count_user'}active{/if}"><a href="{$sGamesRootPage}?order=game_count_user&order_way={if $sGameOrder=='game_count_user'}{$sGameOrderWayNext}{else}{$sGameOrderWay}{/if}" {if $sGameOrder=='game_count_user'}class="{$sGameOrderWay}"{/if}><span>{$aLang.plugin.gamerpress.games_readers}</span></a></div>
        </th>
        <th class="cell-rating cell-tab align-center">
          <div class="cell-tab-inner {if $sGameOrder=='game_rating'}active{/if}"><a href="{$sGamesRootPage}?order=game_rating&order_way={if $sGameOrder=='game_rating'}{$sGameOrderWayNext}{else}{$sGameOrderWay}{/if}" {if $sGameOrder=='game_rating'}class="{$sGameOrderWay}"{/if}><span>{$aLang.plugin.gamerpress.games_rating}</span></a></div>
        </th>
      </tr>
    </thead>
  {else}
    <thead>
      <tr>
        <th class="cell-info">&nbsp;</th>
        <th class="cell-name cell-tab"><div class="cell-tab-inner"><span>{$aLang.plugin.gamerpress.games_title}</span></div></th>

        {if $oUserCurrent}
          <th class="cell-join">&nbsp;</th>
        {/if}

        <th class="cell-readers cell-tab"><div class="cell-tab-inner"><span>{$aLang.plugin.gamerpress.games_readers}</span></div></th>
        <th class="cell-rating cell-tab align-center">
          <div class="cell-tab-inner active"><span>{$aLang.plugin.gamerpress.games_rating}</span></div>
        </th>
      </tr>
    </thead>
  {/if}


  <tbody>
    {if $aGames}
      {foreach from=$aGames item=oGame}
        {assign var="oUserOwner" value=$oGame->getOwner()}

        <tr>
          <td class="cell-info">
            <a href="#" onclick="return ls.infobox.showInfoGame(this,{$oGame->getId()});" class="game-list-info"></a>
          </td>
          <td class="cell-name">
            <p>
              <a href="{$oGame->getUrlFull()}" class="game-name">{$oGame->getTitle()|escape:'html'}</a> {if $oGame->getGenres()}({$oGame->getGenres()}){/if}
            </p>

            <span class="user-avatar">
              <a href="{$oUserOwner->getUserWebPath()}"><img src="{$oUserOwner->getProfileAvatarPath(24)}" alt="avatar" /></a>
              <a href="{$oUserOwner->getUserWebPath()}">{$oUserOwner->getLogin()}</a>
            </span>
          </td>

          {if $oUserCurrent}
            <td class="cell-join">
              {if $oUserCurrent->getId() != $oGame->getOwnerId()}
                <button type="submit"  onclick="ls.game.toggleJoin(this, {$oGame->getId()}); return false;" class="button button-action button-action-join {if $oGame->getUserIsJoin()}active{/if}">
                  <i class="icon-synio-join"></i>
                  <span>{if $oGame->getUserIsJoin()}{$aLang.plugin.gamerpress.game_leave}{else}{$aLang.plugin.gamerpress.game_join}{/if}</span>
                </button>
              {else}
                &mdash;
              {/if}
            </td>
          {/if}

          <td class="cell-readers" id="game_user_count_{$oGame->getId()}">{$oGame->getCountUser()}</td>
          <td class="cell-rating align-center {if $oGame->getRating() < 0}negative{/if}">{$oGame->getRating()}</td>
        </tr>
      {/foreach}
    {else}
      <tr>
        <td colspan="5">
          {if $sGamesEmptyList}
            {$sGamesEmptyList}
          {else}

          {/if}
        </td>
      </tr>
    {/if}
  </tbody>
</table>
