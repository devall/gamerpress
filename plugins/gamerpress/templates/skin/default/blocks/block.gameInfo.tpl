{assign var=oVote value=$oGame->getVote()}

<section class="block">
  <div class="block-content">
  {if $oGame->getAvatarPath(0)}
    <img src="{$oGame->getAvatarPath(0)}" width="100%"/>
  {/if}
  </div>
</section>

<section class="block">
  <script type="text/javascript">
    ls.registry.set('rating-icons-path','{Plugin::GetWebPath('gamerpress')}classes/libs/jRating/jquery/');
    ls.registry.set('rating-disable',{if $oVote}'{$oVote->getDirection()}'{else}0{/if});
  </script>

  <header class="block-header sep">
    <h3>Рейтинг игры</h3>
  </header>

  <div class="block-content">
    <div class="clearfix" style="left: -25px; position: relative;">
      <div style="float: left; background: url({$aTemplateWebPathPlugin.gamerpress}images/ribbon_green.png); width: 124px; height: 56px;">
        <div style="padding: 18px; font-size: 36px; color: #fff; text-shadow: 0 1px 1px #517d0a; ">—</div>
      </div>
      <div style="margin-left: 124px; width: 116px; margin-top: 18px;">
        <div>Обзорская оценка</div>
      </div>
    </div>
    <div class="clearfix" style="left: -25px; position: relative;">
      <div style="float: left; background: url({$aTemplateWebPathPlugin.gamerpress}images/ribbon_green.png); width: 124px; height: 56px;">
        <div style="padding: 18px; font-size: 36px; color: #fff; text-shadow: 0 1px 1px #517d0a; ">{if $oGame->getCountVote()}{number_format(round($oGame->getRating()/$oGame->getCountVote(),1), 1, '.', '')}{else}—{/if}</div>
      </div>
      <div style="margin-left: 124px; width: 116px; margin-top: 18px;">
        <div>Геймерская оценка</div>
      </div>
    </div>
    <div class="clearfix" style="left: -25px; position: relative;">
      <div style="float: left; background: url({$aTemplateWebPathPlugin.gamerpress}images/ribbon_green.png); width: 124px; height: 56px;">
        <div style="padding: 18px; font-size: 36px; color: #fff; text-shadow: 0 1px 1px #517d0a; ">{if $oVote}{number_format(round($oVote->getValue(),1), 1, '.', '')}{else}—{/if}</div>
      </div>
      <div style="margin-left: 124px; width: 116px; margin-top: 13px;">
        <div>Ваша оценка</div>
        <div class="game-rating" data-select="{if $oVote}{$oVote->getValue()}{else}0{/if}" data-id="{$oGame->getId()}"></div>
      </div>
    </div>
  </div>
</section>

<section class="block block-type-gameinfo">
  <header class="block-header sep">
    <h3>{$aLang.plugin.gamerpress.block_game_info}</h3>
  </header>

  <div class="block-content">
    {* <img src="{$oGame->getAvatarPath(48)}" alt="avatar" class="avatar" /> *}
    {assign var=aGenres value=$oGame->getGenresArray()}
    {assign var=aDevelopers value=$oGame->getDevelopersArray()}
    <table class="list">
      <tbody>
        {if $aGenres}
        <tr>
          <td>{$aLang.plugin.gamerpress.game_create_genres}<td>
          <td>{foreach from=$aGenres item=sGenre}<div>{$sGenre}</div>{/foreach}</td>
        </tr>
        {/if}
        {if $aDevelopers}
        <tr>
          <td>{$aLang.plugin.gamerpress.game_create_developers}<td>
          <td>{foreach from=$aDevelopers item=sDeveloper}<div>{$sDeveloper}</div>{/foreach}</td>
        </tr>
        {/if}
        <tr>
          <td>{$aLang.plugin.gamerpress.game_create_publishers}<td>
          <td>{$oGame->getPublishers()}</td>
        </tr>
        <tr>
          <td>{$aLang.plugin.gamerpress.game_create_date_release}<td>
          <td>{if $oGame->getDateRelease()}{date_format date=$oGame->getDateRelease()  format="j F Y"}{else}{$aLang.plugin.gamerpress.game_create_date_release_noexists}{/if}</td>
        </tr>
        <tr>
          <td>{$aLang.plugin.gamerpress.game_create_link}<td>
          <td><a href="{$oGame->getLink()}">{$oGame->getLink()}</a></td>
        </tr>
      </tbody>
    </table>
  </div>
</section>
