{if $oUserCurrent}
  <section class="block block-type-activity">
    <header class="block-header">
      <h3>{$aLang.plugin.gamerpress.userfeed_block_games_title}</h3>
    </header>

    <div class="block-content">
      <small class="note">{$aLang.plugin.gamerpress.userfeed_settings_note_follow_games}</small>

      {if count($aUserfeedGames)}
        <ul class="stream-settings-blogs">
          {foreach from=$aUserfeedGames item=oGame}
            {assign var=iGameId value=$oGame->getId()}
            <li><input class="userfeedBlogCheckbox input-checkbox"
                  type="checkbox"
                  {if isset($aUserfeedSubscribedGames.$iGameId)} checked="checked"{/if}
                  onClick="if (jQuery(this).prop('checked')) { ls.userfeed.subscribe('games',{$iGameId}) } else { ls.userfeed.unsubscribe('games',{$iGameId}) } " />
              <a href="{$oGame->getUrlFull()}">{$oGame->getTitle()|escape:'html'}</a>
            </li>
          {/foreach}
        </ul>
      {else}
        <small class="notice-empty">{$aLang.plugin.gamerpress.userfeed_no_games}</small>
      {/if}
    </div>
  </section>
{/if}
