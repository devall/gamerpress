<?php
/**
 * Русский языковой файл плагина
 */
return array(
  /**
   * Игры
   */
  'games' => 'Игры',
  'games_search_title_hint' => 'Поиск по названию',
  'games_search_empty' => 'Поиск не дал результатов',
  'games_title' => 'Название',
  'games_readers' => 'Читателей',
  'games_rating' => 'Оценка',
  'game_closed' => 'Закрытый блог',
  'game_expand_info' => 'Подробнее &darr;',
  /**
   * Пользователи блога
   */
  /*'blog_user_count' => 'подписчиков',
  'blog_user_administrators' => 'Администраторы',
  'blog_user_moderators' => 'Модераторы',
  'blog_user_moderators_empty' => 'Модераторов здесь не замечено',
  'blog_user_readers' => 'Читатели',*/
  'game_user_readers_all' => 'Все подписчики игры',
  'game_user_readers_empty' => 'Подписчиков здесь не замечено',
  /**
   * Голосование за блог
   */
  /*'blog_vote_up' => 'нравится',
  'blog_vote_down' => 'не нравится',
  'blog_vote_count_text' => 'всего проголосовавших:',*/
  'game_vote_error_already' => 'Вы уже голосовали за эту игру!',
  'game_vote_error_self' => 'Вы не можете голосовать за свою игру!',
  'game_vote_error_acl' => 'У вас не хватает рейтинга и силы для голосования!',
  'game_vote_ok' => 'Ваш голос учтен',
  /**
   * Вступление и выход из игры
   */
  'game_join' => 'Подписаться',
  'game_join_ok' => 'Вы подписались на игру',
  'blog_join_error_self' => 'Зачем вы хотите подписаться на эту игру? Вы и так ее хозяин!',
  'game_leave' => 'Отписаться',
  'game_leave_ok' => 'Вы отписались от игры',

  /**
   * Меню игр
   */
  'game_menu_all_list' => 'Все игры',
  'game_menu_collective_review' => 'Обзоры',
  'game_menu_create' => 'Игру',

  /**
   * Создание/редактирование игры
   */
  'game_edit' => 'Редактировать',
  //'blog_delete' => 'Удалить',
  'game_create' => 'Создание новой игры',
  'game_create_acl' => 'Вы еще не достаточно окрепли, чтобы создавать свою игру',
  'game_create_title' => 'Название игры',
  'game_create_title_notice' => 'Название игры должно совпадать с официальным названием игры.',
  'game_create_title_error' => 'Название игры должно быть от 2 до 200 символов',
  'game_create_title_error_unique' => 'Игра с таким названием уже существует',
  'game_create_genres' => 'Жанры',
  'game_create_genres_notice' => 'Жанры нужно разделять запятой. Например: Action, RPG, Онлайн.',
  'game_create_developers' => 'Разработчики',
  'game_create_developers_notice' => 'Разработчиков нужно разделять запятой.',
  'game_create_publishers' => 'Издатели',
  'game_create_publishers_notice' => 'Издателей нужно разделять запятой.',
  'game_create_date_release' => 'Дата выпуска',
  'game_create_date_release_notice' => 'Дата выпуска игры в формате dd-mm-yyyy.',
  'game_create_date_release_noexists' => 'Не указано',
  'game_create_link' => 'Сайт игры',
  'game_create_link_notice' => 'Официальный сайт игры.',
  'game_create_url' => 'URL страницы игры',
  'game_create_url_notice' => 'URL страницы игры, по которому она будет доступна. Может содержать только буквы латинского алфавита, цифры, дефис; пробелы будут заменены на "_". URL  должен совпадать с названием игры, после ее создания редактирование этого параметра будет недоступно.',
  'game_create_url_error' => 'URL страницы игры должен быть от 2 до 50 символов и только на латинице + цифры и знаки "-", "_"',
  'game_create_url_error_badword' => 'URL страницы игры должен отличаться от:',
  'game_create_url_error_unique' => 'Игра с таким URL уже существует',
  'game_create_description' => 'Описание игры',
  'game_create_description_notice' => 'Между прочим, можно использовать html-теги.',
  'game_create_rating' => 'Ограничение по рейтингу',
  'game_create_rating_notice' => 'Рейтинг, который необходим пользователю, чтобы написать об этой игре.',
  'game_create_submit' => 'Сохранить',
  /**
   * Управление игрой
   */
  'game_admin' => 'Управление игрой',
  'game_admin_content' => 'Содержание',
  'game_admin_users' => 'Пользователи',
  'game_admin_users_submit_ok' => 'Права сохранены',
  /**
   * Создание топика
   */
  'topic_create_game_error_unknown' => 'Пытаетесь запостить топик в неизвестную игру?',
  'topic_create_game_error_noallow' => 'Вы не можете писать в эту игру',
  /**
   * Валидация данных
   */
  'validate_genres_count_more' => 'Поле %%field%% содержит слишком много жанров (максимально допустимо %%count%%)',
  'validate_genres_empty' => 'Поле %%field%% не содержит жанров, либо содержит неверные жанры (размер жанра допустим от %%min%% до %%max%% символов)',
  'validate_developers_count_more' => 'Поле %%field%% содержит слишком много имен разработчиков (максимально допустимо %%count%%)',
  'validate_developers_empty' => 'Поле %%field%% не содержит имен разработчиков, либо содержит неверный формат имен разработчиков (размер имени разработчика допустим от %%min%% до %%max%% символов)',
  'validate_publishers_count_more' => 'Поле %%field%% содержит слишком много имен издателей (максимально допустимо %%count%%)',
  'validate_publishers_empty' => 'Поле %%field%% не содержит имен издателей, либо содержит неверный формат имен издателей (размер имени издателя допустим от %%min%% до %%max%% символов)',
  /**
   * Блоки
   */
  'block_game_info' => 'Общая информация',
  /**
   * Меню топиков
   */
  'topic_menu_add_review' => 'Обзор',
  /**
   * Инфо-блоки
   */
  'infobox_game_create' => 'Создан',
  'infobox_game_topics' => 'Топиков',
  'infobox_game_users' => 'Подписчиков',
  'infobox_game_rating' => 'Рейтинг',
  'infobox_game_topic_last' => 'Последний топик',
  'infobox_game_url' => 'Подписаться на игру',
  'infobox_game_rss' => 'Подписаться на RSS',
  /**
   * Userfeed
   */
  'userfeed_block_games_title' => 'Игры',
  'userfeed_settings_note_follow_games' => 'Выберите игры которые вы хотели бы читать',
  'userfeed_no_games' => 'Вы не подписались ни на одну игру',
);

?>
