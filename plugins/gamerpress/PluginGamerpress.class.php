<?php

/**
 * Запрещаем напрямую через браузер обращение к этому файлу.
 */
if (!class_exists('Plugin')) {
    die('Hacking attemp!');
}

class PluginGamerpress extends Plugin {

    // Объявление делегирований (нужны для того, чтобы назначить свои экшны и шаблоны)
    public $aDelegates = array(
      'template' => array(
        'header_top.tpl',
        'window_write.tpl',
        'nav.tpl',
        'menu.game.tpl',
        'menu.create.content.tpl',
        'topic_part_header.tpl',
        //'actions/ActionTopic/add.tpl',
        //'actions/ActionReview/add.tpl' => '_actions/ActionReview/add.tpl',
        'blocks/block.stream_topic.tpl',
      ),
      /**
       * 'action' => array('ActionIndex'=>'_ActionSomepage'),
       * Замена экшна ActionIndex на ActionSomepage из папки плагина
       *
       * 'template' => array('index.tpl'=>'_my_plugin_index.tpl'),
       * Замена index.tpl из корня скина файлом /plugins/abcplugin/templates/skin/default/my_plugin_index.tpl
       *
       * 'template'=>array('actions/ActionIndex/index.tpl'=>'_actions/ActionTest/index.tpl'),
       * Замена index.tpl из скина из папки actions/ActionIndex/ файлом /plugins/abcplugin/templates/skin/default/actions/ActionTest/index.tpl
       */
    );

    // Объявление переопределений (модули, мапперы и сущности)
    protected $aInherits=array(
        'action'  =>array(
          'ActionAjax',
          'ActionTopic',
          'ActionUserfeed',
        ),
        'module'  =>array(
          'ModuleACL',
          'ModuleRating',
          'ModuleTopic',
          'ModuleUserfeed',
        ),
        'mapper'  =>array(
          'ModuleTopic_MapperTopic',
          'ModuleUserfeed_MapperUserfeed',
        ),
        'entity'  =>array(
          'ModuleValidate_EntityValidatorGenres',
          'ModuleValidate_EntityValidatorDevelopers',
          'ModuleValidate_EntityValidatorPublishers',
          'ModuleTopic_EntityTopic',
          'ModuleTopic_EntityTopicTag',
        ),
       /**
        * Переопределение модулей (функционал):
        * 'module'  =>array('ModuleTopic'=>'_ModuleTopic'),
        *
        * К классу ModuleTopic (/classes/modules/Topic.class.php) добавляются методы из
        * PluginAbcplugin_ModuleTopic (/plugins/abcplugin/classes/modules/Topic.class.php) - новые или замена существующих
        *
        *
        *
        * Переопределение мапперов (запись/чтение объектов в/из БД):
        * 'mapper'  =>array('ModuleTopic_MapperTopic' => '_ModuleTopic_MapperTopic'),
        *
        * К классу ModuleTopic_MapperTopic (/classes/modules/mapper/Topic.mapper.class.php) добавляются методы из
        * PluginAbcplugin_ModuleTopic_EntityTopic (/plugins/abcplugin/classes/modules/mapper/Topic.mapper.class.php) - новые или замена существующих
        *
        *
        *
        * Переопределение сущностей (интерфейс между объектом и записью/записями в БД):
        * 'entity'  =>array('ModuleTopic_EntityTopic' => '_ModuleTopic_EntityTopic'),
        *
        * К классу ModuleTopic_EntityTopic (/classes/modules/entity/Topic.entity.class.php) добавляются методы из
        * PluginAbcplugin_ModuleTopic_EntityTopic (/plugins/abcplugin/classes/modules/entity/Topic.entity.class.php) - новые или замена существующих
        *
        */
    );

    // Активация плагина
    public function Activate() {
        if (!$this->isTableExists('prefix_game')) {
            $this->ExportSQL(dirname(__FILE__).'/install.sql');
            $this->addEnumType('prefix_topic','topic_type','review');
            $this->addEnumType('prefix_vote','topic_type','game');
        }
        return true;
    }

    // Деактивация плагина
    public function Deactivate(){
        /*
        $this->ExportSQL(dirname(__FILE__).'/deinstall.sql'); // Выполнить деактивационный sql, если надо.
        */
        return true;
    }


    // Инициализация плагина
    public function Init() {
        $this->Viewer_AppendStyle(Plugin::GetTemplatePath(__CLASS__).'/css/template_base.css');
        $this->Viewer_AppendStyle(Plugin::GetTemplatePath(__CLASS__).'/css/template_grid.css');
        $this->Viewer_AppendStyle(Plugin::GetTemplatePath(__CLASS__).'/css/tables.css');
        $this->Viewer_AppendStyle(Plugin::GetTemplatePath(__CLASS__).'/css/common.css');
        $this->Viewer_AppendStyle(Plugin::GetTemplatePath(__CLASS__).'/css/navs.css');
        $this->Viewer_AppendStyle(Plugin::GetTemplatePath(__CLASS__).'/css/game.css');
        $this->Viewer_AppendStyle(Plugin::GetTemplatePath(__CLASS__).'/css/video.css');
        $this->Viewer_AppendStyle(Plugin::GetTemplatePath(__CLASS__).'/css/album.css');
        $this->Viewer_AppendStyle(Plugin::GetTemplatePath(__CLASS__).'/css/blocks.css');

        //$this->Viewer_AppendScript(Plugin::GetTemplatePath(__CLASS__).'/js/template.js'); // Добавление своего JS
        //$this->Viewer_AddMenu('blog',Plugin::GetTemplatePath(__CLASS__).'/menu.blog.tpl'); // например, задаем свой вид меню
    }
}
?>
