<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexey Yelizarov
*
---------------------------------------------------------
*/

/**
 * Экшен обработки ajax запросов
 * Ответ отдает в JSON фомате
 *
 * @package actions
 * @since 0.1
 */
class PluginGamerpress_ActionAjax extends PluginGamerpress_Inherit_ActionAjax {

  /**
   * Регистрация евентов
   */
  protected function RegisterEvent() {
    parent::RegisterEvent();
    $this->AddEventPreg('/^vote$/i','/^game$/','EventVoteGame');
    $this->AddEventPreg('/^infobox/i','/^info/','/^game/','EventInfoboxInfoGame');
  }

  /**
   * Голосование за игру
   *
   */
  protected function EventVoteGame() {
    /**
     * Пользователь авторизован?
     */
    if (!$this->oUserCurrent) {
      $this->Message_AddErrorSingle($this->Lang_Get('need_authorization'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Игра существует?
     */
    if (!($oGame=$this->PluginGamerpress_Game_GetGameById(getRequest('idGame',null,'post')))) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Голосует за свою игру? Почему нет :)
     */
    /*if ($oGame->getOwnerId()==$this->oUserCurrent->getId()) {
      $this->Message_AddErrorSingle($this->Lang_Get('plugin.gamerpress.game_vote_error_self'),$this->Lang_Get('attention'));
      return;
    }*/
    /**
     * Уже голосовал?
     */
    if ($oGameVote=$this->Vote_GetVote($oGame->getId(),'game',$this->oUserCurrent->getId())) {
      $this->Message_AddErrorSingle($this->Lang_Get('plugin.gamerpress.game_vote_error_already'),$this->Lang_Get('attention'));
      return;
    }
    /**
     * Имеет право на голосование?
     */
    switch($this->ACL_CanVoteGame($this->oUserCurrent,$oGame)) {
      case PluginGamerpress_ModuleACL::CAN_VOTE_GAME_TRUE:
        $iValue=getRequest('value',null,'post');
        if ($iValue >= 0 and $iValue <= 10) {
          $oGameVote=Engine::GetEntity('Vote');
          $oGameVote->setTargetId($oGame->getId());
          $oGameVote->setTargetType('game');
          $oGameVote->setVoterId($this->oUserCurrent->getId());
          $oGameVote->setDirection(1);
          $oGameVote->setDate(date("Y-m-d H:i:s"));
          $iVal=(float)$this->Rating_VoteGame($this->oUserCurrent,$oGame,$iValue);
          $oGameVote->setValue($iVal);
          $oGame->setCountVote($oGame->getCountVote()+1);
          if ($this->Vote_AddVote($oGameVote) and $this->PluginGamerpress_Game_UpdateGame($oGame)) {
            $this->Viewer_AssignAjax('iCountVote',$oGame->getCountVote());
            $this->Viewer_AssignAjax('iRating',$oGame->getRating());
            $this->Message_AddNoticeSingle($this->Lang_Get('plugin.gamerpress.game_vote_ok'),$this->Lang_Get('attention'));
            /**
             * Добавляем событие в ленту
             */
            $this->Stream_write($oGameVote->getVoterId(), 'vote_game', $oGame->getId());
          } else {
            $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('attention'));
            return;
          }
        } else {
          $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('attention'));
          return;
        }
        break;

      default:
      case PluginGamerpress_ModuleACL::CAN_VOTE_GAME_FALSE:
        $this->Message_AddErrorSingle($this->Lang_Get('plugin.gamerpress.game_vote_error_acl'),$this->Lang_Get('attention'));
        return;
        break;
    }
  }

  /**
   * Вывод информации о игре
   */
  protected function EventInfoboxInfoGame() {
    /**
     * Если игра существует
     */
    if (!is_string(getRequest('iGameId'))) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'));
      return;
    }
    if (!($oGame=$this->PluginGamerpress_Game_GetGameById(getRequest('iGameId')))) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'));
      return;
    }
    /**
     * Получаем локальный вьюер для рендеринга шаблона
     */
    $oViewer=$this->Viewer_GetLocalViewer();

    $oViewer->Assign('oGame',$oGame);
    if ($oGame->getUserIsJoin()) {
      /**
       * Получаем последний топик
       */
      /**
       * TODO: Топики у игр
       */
      $aResult = $this->Topic_GetTopicsByFilter(array('blog_id'=>$oGame->getId(),'topic_publish'=>1),1,1);
      $oViewer->Assign('oTopicLast',reset($aResult['collection']));
    }
    $oViewer->Assign('oUserCurrent',$this->oUserCurrent);
    /**
     * Устанавливаем переменные для ajax ответа
     */
    $this->Viewer_AssignAjax('sText',$oViewer->Fetch(Plugin::GetTemplatePath(__CLASS__).'/infobox.info.game.tpl'));
  }
}
?>
