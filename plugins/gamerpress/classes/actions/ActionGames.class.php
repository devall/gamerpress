<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexey Yelizarov
*
---------------------------------------------------------
*/

/**
 * Экшен обработки УРЛа вида /games/
 *
 * @package actions
 * @since 0.1
 */
class PluginGamerpress_ActionGames extends Action {
  /**
   * Главное меню
   *
   * @var string
   */
  protected $sMenuHeadItemSelect='games';

  /**
   * Инициализация
   */
  public function Init() {
    /**
     * Загружаем в шаблон JS текстовки
     */
    $this->Lang_AddLangJs(array(
      'plugin.gamerpress.game_join',
      'plugin.gamerpress.game_leave'
    ));
  }
  /**
   * Регистрируем евенты
   */
  protected function RegisterEvent() {
    $this->AddEventPreg('/^(page([1-9]\d{0,5}))?$/i','EventShowGames');
    $this->AddEventPreg('/^ajax-search$/i','EventAjaxSearch');
  }


  /**********************************************************************************
   ************************ РЕАЛИЗАЦИЯ ЭКШЕНА ***************************************
   **********************************************************************************
   */

  /**
   * Поиск игр по названию
   */
  protected function EventAjaxSearch() {
    /**
     * Устанавливаем формат Ajax ответа
     */
    $this->Viewer_SetResponseAjax('json');
    /**
     * Получаем из реквеста первые буквы игры
     */
    if ($sTitle=getRequest('game_title') and is_string($sTitle)) {
      $sTitle=str_replace('%','',$sTitle);
    }
    if (!$sTitle) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'));
      return;
    }
    /**
     * Ищем игры
     */
    $aResult=$this->PluginGamerpress_Game_GetGamesByFilter(array(/*'exclude_type' => 'personal',*/'title'=>"%{$sTitle}%"),array('game_title'=>'asc'),1,100);
    /**
     * Формируем и возвращает ответ
     */
    $oViewer=$this->Viewer_GetLocalViewer();
    $oViewer->Assign('aGames',$aResult['collection']);
    $oViewer->Assign('oUserCurrent',$this->User_GetUserCurrent());
    $oViewer->Assign('sGamesEmptyList',$this->Lang_Get('plugin.gamerpress.games_search_empty'));
    $this->Viewer_AssignAjax('sText',$oViewer->Fetch(Plugin::GetTemplatePath(__CLASS__).'game_list.tpl'));
  }
  /**
   * Отображение списка игр
   */
  protected function EventShowGames() {
    /**
     * По какому полю сортировать
     */
    $sOrder='game_rating';
    if (getRequest('order')) {
      $sOrder=(string)getRequest('order');
    }
    /**
     * В каком направлении сортировать
     */
    $sOrderWay='desc';
    if (getRequest('order_way')) {
      $sOrderWay=(string)getRequest('order_way');
    }
    /**
     * Фильтр поиска игр
     */
    $aFilter=array(
      'exclude_type' => 'personal'
    );
    /**
     * Передан ли номер страницы
     */
    $iPage= preg_match("/^\d+$/i",$this->GetEventMatch(2)) ? $this->GetEventMatch(2) : 1;
    /**
     * Получаем список игр
     */
    $aResult=$this->PluginGamerpress_Game_GetGamesByFilter($aFilter,array($sOrder=>$sOrderWay),$iPage,Config::Get('plugin.gamerpress.module.game.per_page'));
    $aGames=$aResult['collection'];
    /**
     * Формируем постраничность
     */
    $aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('plugin.gamerpress.module.game.per_page'),Config::Get('pagination.pages.count'),Router::GetPath('games'),array('order'=>$sOrder,'order_way'=>$sOrderWay));
    /**
     * Загружаем переменные в шаблон
     */
    $this->Viewer_Assign('aPaging',$aPaging);
    $this->Viewer_Assign("aGames",$aGames);
    $this->Viewer_Assign("sGameOrder",htmlspecialchars($sOrder));
    $this->Viewer_Assign("sGameOrderWay",htmlspecialchars($sOrderWay));
    $this->Viewer_Assign("sGameOrderWayNext",htmlspecialchars($sOrderWay=='desc' ? 'asc' : 'desc'));
    /**
     * Устанавливаем title страницы
     */
    $this->Viewer_AddHtmlTitle($this->Lang_Get('plugin.gamerpress.game_menu_all_list'));
    /**
     * Устанавливаем шаблон вывода
     */
    $this->SetTemplateAction('index');
  }

  public function EventShutdown() {
    $this->Viewer_Assign('sMenuHeadItemSelect',$this->sMenuHeadItemSelect);
  }
}
?>
