<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Экшен обработки УРЛа вида /topic/ - управление своими топиками
 *
 * @package actions
 * @since 0.1
 */
class PluginGamerpress_ActionTopic extends PluginGamerpress_Inherit_ActionTopic {
  /**
   * Регистрируем евенты
   *
   */
  protected function RegisterEvent() {
    parent::RegisterEvent();
    $this->AddEvent('add','EventAdd');
  }


  /**********************************************************************************
   ************************ РЕАЛИЗАЦИЯ ЭКШЕНА ***************************************
   **********************************************************************************
   */

  /**
   * Добавление топика
   *
   */
  protected function EventAdd() {
    /**
     * Загружаем переменные в шаблон
     */
    $this->Viewer_AddHtmlTitle($this->Lang_Get('topic_topic_create'));
    $this->Viewer_Assign('sEventAction','add');
    /**
     * Обрабатываем отправку формы
     */
    return $this->SubmitAdd();
  }
  /**
   * Удаление топика
   *
   */
  protected function EventDelete() {
    $this->Security_ValidateSendForm();
    /**
     * Получаем номер топика из УРЛ и проверяем существует ли он
     */
    $sTopicId=$this->GetParam(0);
    if (!($oTopic=$this->Topic_GetTopicById($sTopicId))) {
      return parent::EventNotFound();
    }
    /**
     * проверяем есть ли право на удаление топика
     */
    if (!$this->ACL_IsAllowDeleteTopic($oTopic,$this->oUserCurrent)) {
      return parent::EventNotFound();
    }
    /**
     * Удаляем топик
     */
    $this->Topic_DeleteTopic($oTopic);
    /**
     * Перенаправляем на страницу со списком топиков из блога или из игры этого топика
     */
    if($oTopic->getGame()) {
      Router::Location($oTopic->getGame()->getUrlFull());
    } else {
      Router::Location($oTopic->getBlog()->getUrlFull());
    }
  }

  /**
   * Обработка добавления топика
   *
   */
  protected function SubmitAdd() {
    /**
     * Проверяем отправлена ли форма с данными(хотяб одна кнопка)
     */
    if (!isPost('submit_topic_publish') and !isPost('submit_topic_save')) {
      return false;
    }
    $oTopic=Engine::GetEntity('Topic');
    $oTopic->_setValidateScenario('topic');
    /**
     * Заполняем поля для валидации
     */
    $oTopic->setBlogId(getRequest('blog_id'));
    $oTopic->setGameId(getRequest('game_id'));
    $oTopic->setTitle(strip_tags(getRequest('topic_title')));
    $oTopic->setTextSource(getRequest('topic_text'));
    $oTopic->setTags(getRequest('topic_tags'));
    $oTopic->setUserId($this->oUserCurrent->getId());
    $oTopic->setType('topic');
    $oTopic->setDateAdd(date("Y-m-d H:i:s"));
    $oTopic->setUserIp(func_getIp());
    /**
     * Проверка корректности полей формы
     */
    if (!$this->checkTopicFields($oTopic)) {
      return false;
    }
    /**
     * Определяем в какой блог или игру делаем запись
     */
    $iBlogId=$oTopic->getBlogId();
    $iGameId=$oTopic->getGameId();
    if ($iBlogId==0 and $iGameId==0) {
      $oBlog=$this->Blog_GetPersonalBlogByUserId($this->oUserCurrent->getId());
    } else if ($iGameId > 0) {
      $oGame=$this->PluginGamerpress_Game_GetGameById($iGameId);
    } else {
      $oBlog=$this->Blog_GetBlogById($iBlogId);
    }

    if($iGameId==0) {
      /**
       * Если блог не определен выдаем предупреждение
       */
      if (!$oBlog) {
        $this->Message_AddErrorSingle($this->Lang_Get('topic_create_blog_error_unknown'),$this->Lang_Get('error'));
        return false;
      }
      /**
       * Проверяем права на постинг в блог
       */
      if (!$this->ACL_IsAllowBlog($oBlog,$this->oUserCurrent)) {
        $this->Message_AddErrorSingle($this->Lang_Get('topic_create_blog_error_noallow'),$this->Lang_Get('error'));
        return false;
      }

      $oTopic->setBlogId($oBlog->getId());
      $oTopic->setGameId(null);
    } else {
      /**
       * Если игра не определена выдаем предупреждение
       */
      if (!$oGame) {
        $this->Message_AddErrorSingle($this->Lang_Get('plugin.gamerpress.topic_create_game_error_unknown'),$this->Lang_Get('error'));
        return false;
      }
      /**
       * Проверяем права на постинг в игру
       */
      if (!$this->ACL_IsAllowGame($oGame,$this->oUserCurrent)) {
        $this->Message_AddErrorSingle($this->Lang_Get('plugin.gamerpress.topic_create_game_error_noallow'),$this->Lang_Get('error'));
        return false;
      }

      $oTopic->setBlogId(null);
      $oTopic->setGameId($oGame->getId());
    }
    /**
     * Проверяем разрешено ли постить топик по времени
     */
    if (isPost('submit_topic_publish') and !$this->ACL_CanPostTopicTime($this->oUserCurrent)) {
      $this->Message_AddErrorSingle($this->Lang_Get('topic_time_limit'),$this->Lang_Get('error'));
      return;
    }

    /**
     * Получаемый и устанавливаем разрезанный текст по тегу <cut>
     */
    list($sTextShort,$sTextNew,$sTextCut) = $this->Text_Cut($oTopic->getTextSource());

    $oTopic->setCutText($sTextCut);
    $oTopic->setText($this->Text_Parser($sTextNew));
    $oTopic->setTextShort($this->Text_Parser($sTextShort));
    /**
     * Публикуем или сохраняем
     */
    if (isset($_REQUEST['submit_topic_publish'])) {
      $oTopic->setPublish(1);
      $oTopic->setPublishDraft(1);
    } else {
      $oTopic->setPublish(0);
      $oTopic->setPublishDraft(0);
    }
    /**
     * Принудительный вывод на главную
     */
    $oTopic->setPublishIndex(0);
    if ($this->ACL_IsAllowPublishIndex($this->oUserCurrent))  {
      if (getRequest('topic_publish_index')) {
        $oTopic->setPublishIndex(1);
      }
    }
    /**
     * Запрет на комментарии к топику
     */
    $oTopic->setForbidComment(0);
    if (getRequest('topic_forbid_comment')) {
      $oTopic->setForbidComment(1);
    }
    /**
     * Добавляем топик
     */
    if ($this->Topic_AddTopic($oTopic)) {
      /**
       * Получаем топик, чтоб подцепить связанные данные
       */
      $oTopic=$this->Topic_GetTopicById($oTopic->getId());
      /**
       * Добавляем автора топика в подписчики на новые комментарии к этому топику
       */
      $this->Subscribe_AddSubscribeSimple('topic_new_comment',$oTopic->getId(),$this->oUserCurrent->getMail());
      /**
       * Делаем рассылку спама всем, кто состоит в этом блоге
       */
      if(isset($oBlog)) {
        /**
         * Обновляем количество топиков в блоге
         */
        $this->Blog_RecalculateCountTopicByBlogId($oTopic->getBlogId());
        if ($oTopic->getPublish()==1 and $oBlog->getType()!='personal') {
          $this->Topic_SendNotifyTopicNew($oBlog,$oTopic,$this->oUserCurrent);
        }
      } else {
        /**
         * Обновляем количество топиков в игре
         */
        $this->PluginGamerpress_Game_RecalculateCountTopicByGameId($oTopic->getGameId());
      }
      /**
       * Добавляем событие в ленту
       */
      //$this->Stream_write($oTopic->getUserId(), 'add_topic', $oTopic->getId(),$oTopic->getPublish() && $oBlog->getType()!='close');
      Router::Location($oTopic->getUrl());
    } else {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'));
      return Router::Action('error');
    }
  }
}
?>
