<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Экшен обработки УРЛа вида /video/ - управление видео
 *
 * @package actions
 * @since 0.1
 */
class PluginGamerpress_ActionVideo extends Action {
  /**
   * Текущий юзер
   *
   * @var ModuleUser_EntityUser|null
   */
  protected $oUserCurrent=null;

  /**
   * Инициализация
   *
   */
  public function Init() {
    /**
     * Проверяем авторизован ли юзер
     */
    if (!$this->User_IsAuthorization()) {
      return parent::EventNotFound();
    }
    $this->oUserCurrent=$this->User_GetUserCurrent();
    /**
     * Усанавливаем дефолтный евент
     */
    $this->SetDefaultEvent('add');
    /**
     * Устанавливаем title страницы
     */
    $this->Viewer_AddHtmlTitle($this->Lang_Get('topic_title'));
  }
  /**
   * Регистрируем евенты
   *
   */
  protected function RegisterEvent() {
    $this->AddEvent('add','EventAdd');
    $this->AddEvent('ajaxaddcomment','AjaxAddComment');
    $this->AddEvent('ajaxresponsecomment','AjaxResponseComment');
  }


  /**********************************************************************************
   ************************ РЕАЛИЗАЦИЯ ЭКШЕНА ***************************************
   **********************************************************************************
   */

  /**
   * Добавление топика
   *
   */
  protected function EventAdd() {
    /**
     * Загружаем переменные в шаблон
     */
    $this->Viewer_AddHtmlTitle($this->Lang_Get('topic_topic_create'));
    /**
     * Обрабатываем отправку формы
     */
    return $this->SubmitAdd();
  }
  /**
   * Удаление топика
   *
   */
  protected function EventDelete() {
    $this->Security_ValidateSendForm();
    /**
     * Получаем номер топика из УРЛ и проверяем существует ли он
     */
    $sTopicId=$this->GetParam(0);
    if (!($oTopic=$this->Topic_GetTopicById($sTopicId))) {
      return parent::EventNotFound();
    }
    /**
     * проверяем есть ли право на удаление топика
     */
    if (!$this->ACL_IsAllowDeleteTopic($oTopic,$this->oUserCurrent)) {
      return parent::EventNotFound();
    }
    /**
     * Удаляем топик
     */
    $this->Topic_DeleteTopic($oTopic);
    /**
     * Перенаправляем на страницу со списком топиков из блога или из игры этого топика
     */
    if($oTopic->getGame()) {
      Router::Location($oTopic->getGame()->getUrlFull());
    } else {
      Router::Location($oTopic->getBlog()->getUrlFull());
    }
  }

  /**
   * Обработка добавления топика
   *
   */
  protected function SubmitAdd() {
    /**
     * Проверяем отправлена ли форма с данными(хотяб одна кнопка)
     */
    if (!isPost('submit_topic_publish') and !isPost('submit_topic_save')) {
      return false;
    }
    $oTopic=Engine::GetEntity('Topic');
    $oTopic->_setValidateScenario('topic');
    /**
     * Заполняем поля для валидации
     */
    $oTopic->setBlogId(getRequest('blog_id'));
    $oTopic->setGameId(getRequest('game_id'));
    $oTopic->setTitle(strip_tags(getRequest('topic_title')));
    $oTopic->setTextSource(getRequest('topic_text'));
    $oTopic->setTags(getRequest('topic_tags'));
    $oTopic->setUserId($this->oUserCurrent->getId());
    $oTopic->setType('topic');
    $oTopic->setDateAdd(date("Y-m-d H:i:s"));
    $oTopic->setUserIp(func_getIp());
    /**
     * Проверка корректности полей формы
     */
    if (!$this->checkTopicFields($oTopic)) {
      return false;
    }
    /**
     * Определяем в какой блог или игру делаем запись
     */
    $iBlogId=$oTopic->getBlogId();
    $iGameId=$oTopic->getGameId();
    if ($iBlogId==0 and $iGameId==0) {
      $oBlog=$this->Blog_GetPersonalBlogByUserId($this->oUserCurrent->getId());
    } else if ($iGameId > 0) {
      $oGame=$this->PluginGamerpress_Game_GetGameById($iGameId);
    } else {
      $oBlog=$this->Blog_GetBlogById($iBlogId);
    }

    if($iGameId==0) {
      /**
       * Если блог не определен выдаем предупреждение
       */
      if (!$oBlog) {
        $this->Message_AddErrorSingle($this->Lang_Get('topic_create_blog_error_unknown'),$this->Lang_Get('error'));
        return false;
      }
      /**
       * Проверяем права на постинг в блог
       */
      if (!$this->ACL_IsAllowBlog($oBlog,$this->oUserCurrent)) {
        $this->Message_AddErrorSingle($this->Lang_Get('topic_create_blog_error_noallow'),$this->Lang_Get('error'));
        return false;
      }

      $oTopic->setBlogId($oBlog->getId());
      $oTopic->setGameId(null);
    } else {
      /**
       * Если игра не определена выдаем предупреждение
       */
      if (!$oGame) {
        $this->Message_AddErrorSingle($this->Lang_Get('plugin.gamerpress.topic_create_game_error_unknown'),$this->Lang_Get('error'));
        return false;
      }
      /**
       * Проверяем права на постинг в игру
       */
      if (!$this->ACL_IsAllowGame($oGame,$this->oUserCurrent)) {
        $this->Message_AddErrorSingle($this->Lang_Get('plugin.gamerpress.topic_create_game_error_noallow'),$this->Lang_Get('error'));
        return false;
      }

      $oTopic->setBlogId(null);
      $oTopic->setGameId($oGame->getId());
    }
    /**
     * Проверяем разрешено ли постить топик по времени
     */
    if (isPost('submit_topic_publish') and !$this->ACL_CanPostTopicTime($this->oUserCurrent)) {
      $this->Message_AddErrorSingle($this->Lang_Get('topic_time_limit'),$this->Lang_Get('error'));
      return;
    }

    /**
     * Получаемый и устанавливаем разрезанный текст по тегу <cut>
     */
    list($sTextShort,$sTextNew,$sTextCut) = $this->Text_Cut($oTopic->getTextSource());

    $oTopic->setCutText($sTextCut);
    $oTopic->setText($this->Text_Parser($sTextNew));
    $oTopic->setTextShort($this->Text_Parser($sTextShort));
    /**
     * Публикуем или сохраняем
     */
    if (isset($_REQUEST['submit_topic_publish'])) {
      $oTopic->setPublish(1);
      $oTopic->setPublishDraft(1);
    } else {
      $oTopic->setPublish(0);
      $oTopic->setPublishDraft(0);
    }
    /**
     * Принудительный вывод на главную
     */
    $oTopic->setPublishIndex(0);
    if ($this->ACL_IsAllowPublishIndex($this->oUserCurrent))  {
      if (getRequest('topic_publish_index')) {
        $oTopic->setPublishIndex(1);
      }
    }
    /**
     * Запрет на комментарии к топику
     */
    $oTopic->setForbidComment(0);
    if (getRequest('topic_forbid_comment')) {
      $oTopic->setForbidComment(1);
    }
    /**
     * Добавляем топик
     */
    if ($this->Topic_AddTopic($oTopic)) {
      /**
       * Получаем топик, чтоб подцепить связанные данные
       */
      $oTopic=$this->Topic_GetTopicById($oTopic->getId());
      /**
       * Добавляем автора топика в подписчики на новые комментарии к этому топику
       */
      $this->Subscribe_AddSubscribeSimple('topic_new_comment',$oTopic->getId(),$this->oUserCurrent->getMail());
      /**
       * Делаем рассылку спама всем, кто состоит в этом блоге
       */
      if(isset($oBlog)) {
        /**
         * Обновляем количество топиков в блоге
         */
        $this->Blog_RecalculateCountTopicByBlogId($oTopic->getBlogId());
        if ($oTopic->getPublish()==1 and $oBlog->getType()!='personal') {
          $this->Topic_SendNotifyTopicNew($oBlog,$oTopic,$this->oUserCurrent);
        }
      } else {
        /**
         * Обновляем количество топиков в игре
         */
        $this->PluginGamerpress_Game_RecalculateCountTopicByGameId($oTopic->getGameId());
      }
      /**
       * Добавляем событие в ленту
       */
      //$this->Stream_write($oTopic->getUserId(), 'add_topic', $oTopic->getId(),$oTopic->getPublish() && $oBlog->getType()!='close');
      Router::Location($oTopic->getUrl());
    } else {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'));
      return Router::Action('error');
    }
  }

  /**
   * Обработка добавление комментария к видео через ajax
   *
   */
  protected function AjaxAddComment() {
    /**
     * Устанавливаем формат Ajax ответа
     */
    $this->Viewer_SetResponseAjax('json');
    $this->SubmitComment();
  }

  /**
   * Обработка добавление комментария к видео
   *
   */
  protected function SubmitComment() {
    /**
     * Проверям авторизован ли пользователь
     */
    if (!$this->User_IsAuthorization()) {
      $this->Message_AddErrorSingle($this->Lang_Get('need_authorization'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверяем топик
     */
    if (!($oVideo=$this->PluginGamerpress_Video_GetVideoById(getRequest('cmt_target_id')))) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Возможность постить коммент в топик в черновиках
     */
    if (!$oVideo->getPublish() and $this->oUserCurrent->getId()!=$oVideo->getUserId() and !$this->oUserCurrent->isAdministrator()) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверяем разрешено ли постить комменты
     */
    if (!$this->ACL_CanPostComment($this->oUserCurrent) and !$this->oUserCurrent->isAdministrator()) {
      $this->Message_AddErrorSingle($this->Lang_Get('topic_comment_acl'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверяем разрешено ли постить комменты по времени
     */
    if (!$this->ACL_CanPostCommentTime($this->oUserCurrent) and !$this->oUserCurrent->isAdministrator()) {
      $this->Message_AddErrorSingle($this->Lang_Get('topic_comment_limit'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверяем запрет на добавления коммента автором топика
     */
    if ($oVideo->getForbidComment()) {
      $this->Message_AddErrorSingle($this->Lang_Get('topic_comment_notallow'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверяем текст комментария
     */
    $sText=$this->Text_Parser(getRequest('comment_text'));
    if (!func_check($sText,'text',2,10000)) {
      $this->Message_AddErrorSingle($this->Lang_Get('topic_comment_add_text_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверям на какой коммент отвечаем
     */
    $sParentId=(int)getRequest('reply');
    if (!func_check($sParentId,'id')) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    $oCommentParent=null;
    if ($sParentId!=0) {
      /**
       * Проверяем существует ли комментарий на который отвечаем
       */
      if (!($oCommentParent=$this->Comment_GetCommentById($sParentId))) {
        $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
        return;
      }
      /**
       * Проверяем из одного топика ли новый коммент и тот на который отвечаем
       */
      if ($oCommentParent->getTargetId()!=$oVideo->getId()) {
        $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
        return;
      }
    } else {
      /**
       * Корневой комментарий
       */
      $sParentId=null;
    }
    /**
     * Проверка на дублирующий коммент
     */
    if ($this->Comment_GetCommentUnique($oVideo->getId(),'video',$this->oUserCurrent->getId(),$sParentId,md5($sText))) {
      $this->Message_AddErrorSingle($this->Lang_Get('topic_comment_spam'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Создаём коммент
     */
    $oCommentNew=Engine::GetEntity('Comment');
    $oCommentNew->setTargetId($oVideo->getId());
    $oCommentNew->setTargetType('video');
    $oCommentNew->setTargetParentId($oVideo->getAlbum()->getId());
    $oCommentNew->setUserId($this->oUserCurrent->getId());
    $oCommentNew->setText($sText);
    $oCommentNew->setDate(date("Y-m-d H:i:s"));
    $oCommentNew->setUserIp(func_getIp());
    $oCommentNew->setPid($sParentId);
    $oCommentNew->setTextHash(md5($sText));
    $oCommentNew->setPublish($oVideo->getPublish());
    /**
     * Добавляем коммент
     */
    if ($this->Comment_AddComment($oCommentNew)) {

      $this->Viewer_AssignAjax('sCommentId',$oCommentNew->getId());
      if ($oVideo->getPublish()) {
        /**
         * Добавляем коммент в прямой эфир если топик не в черновиках
         */
        $oCommentOnline=Engine::GetEntity('Comment_CommentOnline');
        $oCommentOnline->setTargetId($oCommentNew->getTargetId());
        $oCommentOnline->setTargetType($oCommentNew->getTargetType());
        $oCommentOnline->setTargetParentId($oCommentNew->getTargetParentId());
        $oCommentOnline->setCommentId($oCommentNew->getId());

        $this->Comment_AddCommentOnline($oCommentOnline);
      }
      /**
       * Сохраняем дату последнего коммента для юзера
       */
      $this->oUserCurrent->setDateCommentLast(date("Y-m-d H:i:s"));
      $this->User_Update($this->oUserCurrent);

      /**
       * Список емайлов на которые не нужно отправлять уведомление
       */
      $aExcludeMail=array($this->oUserCurrent->getMail());
      /**
       * Отправляем уведомление тому на чей коммент ответили
       */
      if ($oCommentParent and $oCommentParent->getUserId()!=$oVideo->getUserId() and $oCommentNew->getUserId()!=$oCommentParent->getUserId()) {
        $oUserAuthorComment=$oCommentParent->getUser();
        $aExcludeMail[]=$oUserAuthorComment->getMail();
        $this->Notify_SendCommentReplyToAuthorParentComment($oUserAuthorComment,$oVideo,$oCommentNew,$this->oUserCurrent);
      }
      /**
       * Отправка уведомления автору топика
       */
      $this->Subscribe_Send('topic_new_comment',$oVideo->getId(),'notify.comment_new.tpl',$this->Lang_Get('notify_subject_comment_new'),array(
        'oVideo' => $oVideo,
        'oComment' => $oCommentNew,
        'oUserComment' => $this->oUserCurrent,
      ),$aExcludeMail);
      /**
       * Добавляем событие в ленту
       */
      $this->Stream_write($oCommentNew->getUserId(), 'add_comment_video', $oCommentNew->getId(), $oVideo->getPublish() && $oVideo->getAlbum()->getAccess()!='close');
    } else {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
    }
  }
  /**
   * Получение новых комментариев
   *
   */
  protected function AjaxResponseComment() {
    /**
     * Устанавливаем формат Ajax ответа
     */
    $this->Viewer_SetResponseAjax('json');
    /**
     * Пользователь авторизован?
     */
    if (!$this->oUserCurrent) {
      $this->Message_AddErrorSingle($this->Lang_Get('need_authorization'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Топик существует?
     */
    $idTopic=getRequest('idTarget',null,'post');
    if (!($oTopic=$this->Topic_GetTopicById($idTopic))) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }

    $idCommentLast=getRequest('idCommentLast',null,'post');
    $selfIdComment=getRequest('selfIdComment',null,'post');
    $aComments=array();
    /**
     * Если используется постраничность, возвращаем только добавленный комментарий
     */
    if (getRequest('bUsePaging',null,'post') and $selfIdComment) {
      if ($oComment=$this->Comment_GetCommentById($selfIdComment) and $oComment->getTargetId()==$oTopic->getId() and $oComment->getTargetType()=='topic') {
        $oViewerLocal=$this->Viewer_GetLocalViewer();
        $oViewerLocal->Assign('oUserCurrent',$this->oUserCurrent);
        $oViewerLocal->Assign('bOneComment',true);

        $oViewerLocal->Assign('oComment',$oComment);
        $sText=$oViewerLocal->Fetch($this->Comment_GetTemplateCommentByTarget($oTopic->getId(),'topic'));
        $aCmt=array();
        $aCmt[]=array(
          'html' => $sText,
          'obj'  => $oComment,
        );
      } else {
        $aCmt=array();
      }
      $aReturn['comments']=$aCmt;
      $aReturn['iMaxIdComment']=$selfIdComment;
    } else {
      $aReturn=$this->Comment_GetCommentsNewByTargetId($oTopic->getId(),'topic',$idCommentLast);
    }
    $iMaxIdComment=$aReturn['iMaxIdComment'];

    $oTopicRead=Engine::GetEntity('Topic_TopicRead');
    $oTopicRead->setTopicId($oTopic->getId());
    $oTopicRead->setUserId($this->oUserCurrent->getId());
    $oTopicRead->setCommentCountLast($oTopic->getCountComment());
    $oTopicRead->setCommentIdLast($iMaxIdComment);
    $oTopicRead->setDateRead(date("Y-m-d H:i:s"));
    $this->Topic_SetTopicRead($oTopicRead);

    $aCmts=$aReturn['comments'];
    if ($aCmts and is_array($aCmts)) {
      foreach ($aCmts as $aCmt) {
        $aComments[]=array(
          'html' => $aCmt['html'],
          'idParent' => $aCmt['obj']->getPid(),
          'id' => $aCmt['obj']->getId(),
        );
      }
    }

    $this->Viewer_AssignAjax('iMaxIdComment',$iMaxIdComment);
    $this->Viewer_AssignAjax('aComments',$aComments);
  }
}
