<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Обрабатывает пользовательские ленты контента
 *
 * @package actions
 * @since 0.1
 */
class PluginGamerpress_ActionUserfeed extends PluginGamerpress_Inherit_ActionUserfeed {

  /**
   * Подписка на контент блога или пользователя
   *
   */
  protected function EventSubscribe() {
    /**
     * Устанавливаем формат Ajax ответа
     */
    $this->Viewer_SetResponseAjax('json');
    /**
     * Проверяем наличие ID блога или пользователя
     */
    if (!getRequest('id')) {
      $this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'));
    }
    $sType = (string)getRequest('type');
    $iType = null;
    /**
     * Определяем тип подписки
     */
    switch($sType) {
      case 'blogs':
        $iType = ModuleUserfeed::SUBSCRIBE_TYPE_BLOG;
        /**
         * Проверяем существование блога
         */
        if (!$this->Blog_GetBlogById(getRequest('id'))) {
          $this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'));
          return;
        }
        break;
      case 'users':
        $iType = ModuleUserfeed::SUBSCRIBE_TYPE_USER;
        /**
         * Проверяем существование пользователя
         */
        if (!$this->User_GetUserById(getRequest('id'))) {
          $this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'));
          return;
        }
        if ($this->oUserCurrent->getId() == getRequest('id')) {
          $this->Message_AddError($this->Lang_Get('userfeed_error_subscribe_to_yourself'),$this->Lang_Get('error'));
          return;
        }
        break;
      case 'games':
        $iType = PluginGamerpress_ModuleUserfeed::SUBSCRIBE_TYPE_GAME;
        /**
         * Проверяем существование игры
         */
        if (!$this->PluginGamerpress_Game_GetGameById(getRequest('id'))) {
          $this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'));
          return;
        }
        break;
      default:
        $this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'));
        return;
    }
    /**
     * Подписываем
     */
    $this->Userfeed_subscribeUser($this->oUserCurrent->getId(), $iType, getRequest('id'));
    $this->Message_AddNotice($this->Lang_Get('userfeed_subscribes_updated'), $this->Lang_Get('attention'));
  }
  /**
   * Отписка от блога или пользователя
   *
   */
  protected function EventUnsubscribe() {
    /**
     * Устанавливаем формат Ajax ответа
     */
    $this->Viewer_SetResponseAjax('json');
    if (!getRequest('id')) {
      $this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    $sType = (string)getRequest('type');
    $iType = null;
    /**
     * Определяем от чего отписываемся
     */
    switch($sType) {
      case 'blogs':
        $iType = ModuleUserfeed::SUBSCRIBE_TYPE_BLOG;
        break;
      case 'users':
        $iType = ModuleUserfeed::SUBSCRIBE_TYPE_USER;
        break;
      case 'games':
        $iType = PluginGamerpress_ModuleUserfeed::SUBSCRIBE_TYPE_GAME;
        break;
      default:
        $this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'));
        return;
    }
    /**
     * Отписываем пользователя
     */
    $this->Userfeed_unsubscribeUser($this->oUserCurrent->getId(), $iType, getRequest('id'));
    $this->Message_AddNotice($this->Lang_Get('userfeed_subscribes_updated'), $this->Lang_Get('attention'));
  }
}
