<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexey Yelizarov
*
---------------------------------------------------------
*/

/**
 * Экшен обработки URL'ов вида /game/
 *
 * @package actions
 * @since 0.1
 */
class PluginGamerpress_ActionGame extends ActionPlugin {
  /**
   * Главное меню
   *
   * @var string
   */
  protected $sMenuHeadItemSelect='games';
  /**
   * Какое меню активно
   *
   * @var string
   */
  protected $sMenuItemSelect='game';
  /**
   * Какое подменю активно
   *
   * @var string
   */
  protected $sMenuSubItemSelect='good';
  /**
   * УРЛ блога который подставляется в меню
   *
   * @var string
   */
  protected $sMenuSubBlogUrl;
  /**
   * Текущий пользователь
   *
   * @var ModuleUser_EntityUser|null
   */
  protected $oUserCurrent=null;
  /**
   * Число новых топиков в коллективных блогах
   *
   * @var int
   */
  protected $iCountTopicsCollectiveNew=0;
  /**
   * Число новых топиков в персональных блогах
   *
   * @var int
   */
  protected $iCountTopicsPersonalNew=0;
  /**
   * Число новых топиков в конкретном блоге
   *
   * @var int
   */
  protected $iCountTopicsBlogNew=0;
  /**
   * Число новых топиков
   *
   * @var int
   */
  protected $iCountTopicsNew=0;
  /**
   * Инизиализация экшена
   *
   */
  public function Init() {
    /**
     * Устанавливаем евент по дефолту, т.е. будем показывать хорошие топики из коллективных игр
     */
    $this->SetDefaultEvent('good');
    $this->sMenuSubGameUrl=Router::GetPath('game');
    /**
     * Достаём текущего пользователя
     */
    $this->oUserCurrent=$this->User_GetUserCurrent();
    /**
     * Подсчитываем новые топики
     */
    /*$this->iCountTopicsCollectiveNew=$this->Topic_GetCountTopicsCollectiveNew();
    $this->iCountTopicsPersonalNew=$this->Topic_GetCountTopicsPersonalNew();
    $this->iCountTopicsBlogNew=$this->iCountTopicsCollectiveNew;
    $this->iCountTopicsNew=$this->iCountTopicsCollectiveNew+$this->iCountTopicsPersonalNew;*/
    /**
     * Загружаем в шаблон JS текстовки
     */
    $this->Lang_AddLangJs(array(
      'plugin.gamerpress.game_join',
      'plugin.gamerpress.game_leave'
    ));
  }
  /**
   * Регистрируем евенты, по сути определяем УРЛы вида /blog/.../
   *
   */
  protected function RegisterEvent() {
    /*
    $this->AddEventPreg('/^good$/i','/^(page([1-9]\d{0,5}))?$/i',array('EventTopics','topics'));
    $this->AddEvent('good',array('EventTopics','topics'));
    $this->AddEventPreg('/^bad$/i','/^(page([1-9]\d{0,5}))?$/i',array('EventTopics','topics'));
    $this->AddEventPreg('/^new$/i','/^(page([1-9]\d{0,5}))?$/i',array('EventTopics','topics'));
    $this->AddEventPreg('/^newall$/i','/^(page([1-9]\d{0,5}))?$/i',array('EventTopics','topics'));
    $this->AddEventPreg('/^discussed$/i','/^(page([1-9]\d{0,5}))?$/i',array('EventTopics','topics'));
    $this->AddEventPreg('/^top$/i','/^(page([1-9]\d{0,5}))?$/i',array('EventTopics','topics'));
    */

    $this->AddEvent('add','EventAddGame');
    $this->AddEvent('edit','EventEditGame');
    //$this->AddEvent('delete','EventDeleteBlog');
    $this->AddEventPreg('/^admin$/i','/^\d+$/i','/^(page([1-9]\d{0,5}))?$/i','EventAdminGame');
    //$this->AddEvent('invite','EventInviteBlog');

    /*$this->AddEvent('ajaxaddcomment','AjaxAddComment');
    $this->AddEvent('ajaxresponsecomment','AjaxResponseComment');
    /*$this->AddEvent('ajaxaddbloginvite', 'AjaxAddBlogInvite');
    $this->AddEvent('ajaxrebloginvite', 'AjaxReBlogInvite');
    $this->AddEvent('ajaxremovebloginvite', 'AjaxRemoveBlogInvite');
    $this->AddEvent('ajaxbloginfo', 'AjaxBlogInfo');*/
    $this->AddEvent('ajaxgamejoin', 'AjaxGameJoin');

    $this->AddEventPreg('/^(\d+)\.html$/i','/^$/i',array('EventShowTopic','topic'));
    $this->AddEventPreg('/^[\w\-\_]+$/i','/^(\d+)\.html$/i',array('EventShowTopic','topic'));

    $this->AddEventPreg('/^[\w\-\_]+$/i','/^(page([1-9]\d{0,5}))?$/i',array('EventShowGame','game'));
    $this->AddEventPreg('/^[\w\-\_]+$/i','/^bad$/i','/^(page([1-9]\d{0,5}))?$/i',array('EventShowGame','game'));
    $this->AddEventPreg('/^[\w\-\_]+$/i','/^new$/i','/^(page([1-9]\d{0,5}))?$/i',array('EventShowGame','game'));
    $this->AddEventPreg('/^[\w\-\_]+$/i','/^newall$/i','/^(page([1-9]\d{0,5}))?$/i',array('EventShowGame','game'));
    $this->AddEventPreg('/^[\w\-\_]+$/i','/^discussed$/i','/^(page([1-9]\d{0,5}))?$/i',array('EventShowGame','game'));
    $this->AddEventPreg('/^[\w\-\_]+$/i','/^top$/i','/^(page([1-9]\d{0,5}))?$/i',array('EventShowGame','game'));

    $this->AddEventPreg('/^[\w\-\_]+$/i','/^videos$/i','/^(page([1-9]\d{0,5}))?$/i',array('EventShowVideos','videos'));

    $this->AddEventPreg('/^[\w\-\_]+$/i','/^topic/i','/^add$/i','EventAddTopic');
    $this->AddEventPreg('/^[\w\-\_]+$/i','/^review/i','/^add$/i','EventAddReview');
    $this->AddEventPreg('/^[\w\-\_]+$/i','/^video/i','/^add$/i','EventAddVideo');
    $this->AddEventPreg('/^[\w\-\_]+$/i','/^video/i','/^(\d+)$/i',array('EventShowVideo','video'));
    $this->AddEventPreg('/^[\w\-\_]+$/i','/^users$/i','/^(page([1-9]\d{0,5}))?$/i','EventShowUsers');
  }


  /**********************************************************************************
   ************************ РЕАЛИЗАЦИЯ ЭКШЕНА ***************************************
   **********************************************************************************
   */

  /**
   * Добавление топика
   *
   */
  protected function EventAddTopic() {
    /**
     * Проверяем авторизован ли пользователь
     */
    if (!$this->User_IsAuthorization()) {
      $this->Message_AddErrorSingle($this->Lang_Get('not_access'),$this->Lang_Get('error'));
      return Router::Action('error');
    }
    /**
     * Меню
     */
    $this->sMenuSubItemSelect='topic';
    $this->sMenuItemSelect='topic';

    $oGame=$this->PluginGamerpress_Game_GetGameByUrl($this->sCurrentEvent);
    if (!$oGame) {
      $this->Message_AddErrorSingle($this->Lang_Get('plugin.gamerpress.topic_create_game_error_unknown'),$this->Lang_Get('error'));
      return false;
    }

    $this->Viewer_Assign('sEventAction','add');
    $this->SetTemplate($this->getTemplatePathPlugin().'actions/ActionTopic/add.tpl');

    if (!isPost('submit_topic_publish') and !isPost('submit_topic_save')) {
      $_REQUEST['game_id'] = $oGame->getId();
      return false;
    }

    return Router::Action('topic','add');
  }

  /**
   * Добавление обзора
   *
   */
  protected function EventAddReview() {
    /**
     * Проверяем авторизован ли пользователь
     */
    if (!$this->User_IsAuthorization()) {
      $this->Message_AddErrorSingle($this->Lang_Get('not_access'),$this->Lang_Get('error'));
      return Router::Action('error');
    }
    /**
     * Меню
     */
    $this->sMenuSubItemSelect='review';
    $this->sMenuItemSelect='topic';

    $oGame=$this->PluginGamerpress_Game_GetGameByUrl($this->sCurrentEvent);
    if (!$oGame) {
      $this->Message_AddErrorSingle($this->Lang_Get('plugin.gamerpress.topic_create_game_error_unknown'),$this->Lang_Get('error'));
      return false;
    }

    $this->Viewer_Assign('sEventAction','add');
    $this->SetTemplate($this->getTemplatePathPlugin().'actions/ActionReview/add.tpl');

    if (!isPost('submit_review_publish') and !isPost('submit_review_save')) {
      $_REQUEST['game_id'] = $oGame->getId();
      return false;
    }

    return Router::Action('review','add');
  }

  /**
   * Добавление видео
   *
   */
  protected function EventAddVideo() {
    /**
     * Проверяем авторизован ли пользователь
     */
    if (!$this->User_IsAuthorization()) {
      $this->Message_AddErrorSingle($this->Lang_Get('not_access'),$this->Lang_Get('error'));
      return Router::Action('error');
    }
    /**
     * Меню
     */
    $this->sMenuSubItemSelect='video';
    $this->sMenuItemSelect='topic';

    $oGame=$this->PluginGamerpress_Game_GetGameByUrl($this->sCurrentEvent);
    if (!$oGame) {
      $this->Message_AddErrorSingle($this->Lang_Get('plugin.gamerpress.topic_create_game_error_unknown'),$this->Lang_Get('error'));
      return false;
    }

    $this->Viewer_Assign('sEventAction','add');
    $this->SetTemplate($this->getTemplatePathPlugin().'actions/ActionVideo/add.tpl');

    if (!isPost('submit_video_publish') and !isPost('submit_video_save')) {
      $_REQUEST['game_id'] = $oGame->getId();
      return false;
    }

    return Router::Action('video','add');
  }

  /**
   * Добавление новой игры
   *
   */
  protected function EventAddGame() {
    /**
     * Устанавливаем title страницы
     */
    $this->Viewer_AddHtmlTitle($this->Lang_Get('plugin.gamerpress.game_create'));
    /**
     * Меню
     */
    $this->sMenuSubItemSelect='add';
    $this->sMenuItemSelect='game';
    /**
     * Проверяем авторизован ли пользователь
     */
    if (!$this->User_IsAuthorization()) {
      $this->Message_AddErrorSingle($this->Lang_Get('not_access'),$this->Lang_Get('error'));
      return Router::Action('error');
    }

    /**
     * Проверяем хватает ли рейтинга юзеру чтоб создать игру
     */
    /*if (!$this->ACL_CanCreateGame($this->oUserCurrent) and !$this->oUserCurrent->isAdministrator()) {
      $this->Message_AddErrorSingle($this->Lang_Get('plugin.gamerpress.game_create_acl'),$this->Lang_Get('error'));
      return Router::Action('error');
    }*/

    /**
     * Проверяем отправлена ли форма с данными (методом POST)
     */
    if (!isPost('submit_game_add')) {
      /* $_REQUEST['blog_limit_rating_topic']=0; */
      return false;
    }
    $oGame=Engine::GetEntity('PluginGamerpress_Game');
    $oGame->setOwnerId($this->oUserCurrent->getId());
    $oGame->setDateAdd(date("Y-m-d H:i:s"));
    $oGame->setAvatar(null);
    /**
     * Заполняем поля для валидации
     */
    $oGame->setTitle(strip_tags(getRequest('game_title')));
    $oGame->setGenres(getRequest('game_genres'));
    $oGame->setDevelopers(getRequest('game_developers'));
    $oGame->setPublishers(getRequest('game_publishers'));
    $oGame->setDateRelease(getRequest('game_create_date_release'));
    $oGame->setLink(getRequest('game_link'));
    /**
     * Заменяем пробельные символы в URL на "_"
     */
    $sGameUrl=preg_replace("/\s+/",'_',(string)getRequest('game_url'));
    $oGame->setUrl($sGameUrl);
    /**
     * Парсим текст на предмет разных ХТМЛ тегов
     */
    $sText=$this->Text_Parser(getRequest('game_description'));
    $oGame->setDescription($sText);
    /* $oGame->setLimitRatingTopic(getRequest('game_limit_rating_topic')); */

    /**
     * Проверка корректности полей формы
     */
    if (!$this->checkGameFields($oGame)) {
      return false;
    }

    $sDateRelease = (getRequest('game_create_date_release')=='') ? null : date("Y-m-d H:i:s",strtotime(getRequest('game_create_date_release')));
    $oGame->setDateRelease($sDateRelease);

    /**
     * Загрузка аватара, делаем ресайзы
     */
    if (isset($_FILES['avatar']) and is_uploaded_file($_FILES['avatar']['tmp_name'])) {
      if ($sPath=$this->PluginGamerpress_Game_UploadGameAvatar($_FILES['avatar'],$oGame)) {
        $oGame->setAvatar($sPath);
      } else {
        $this->Message_AddError($this->Lang_Get('blog_create_avatar_error'),$this->Lang_Get('error'));
        return false;
      }
    }

    /**
     * Создаём игру
     */
    if ($this->PluginGamerpress_Game_AddGame($oGame)) {
      /**
       * Добавляем событие в ленту
       */
      /**
       * TODO: Stream
       */
      /*$this->Stream_write($oBlog->getOwnerId(), 'add_blog', $oBlog->getId());*/
      Router::Location($oGame->getUrlFull());
    } else {
      $this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'));
    }
  }
  /**
   * Редактирование игры
   *
   */
  protected function EventEditGame() {
    /**
     * Меню
     */
    $this->sMenuSubItemSelect='';
    $this->sMenuItemSelect='content';
    /**
     * Проверяем передан ли в УРЛе номер игры
     */
    $sGameId=$this->GetParam(0);
    if (!$oGame=$this->PluginGamerpress_Game_GetGameById($sGameId)) {
      return parent::EventNotFound();
    }
    /**
     * Проверям авторизован ли пользователь
     */
    if (!$this->User_IsAuthorization()) {
      $this->Message_AddErrorSingle($this->Lang_Get('not_access'),$this->Lang_Get('error'));
      return Router::Action('error');
    }
    /**
     * Проверка на право редактировать игру
     */
    if (!$this->ACL_IsAllowEditGame($oGame, $this->oUserCurrent)) {
      return parent::EventNotFound();
    }

    /**
     * Устанавливаем title страницы
     */
    $this->Viewer_AddHtmlTitle($oGame->getTitle());
    $this->Viewer_AddHtmlTitle($this->Lang_Get('plugin.gamerpress.game_edit'));

    $this->Viewer_Assign('oGameEdit',$oGame);
    /**
     * Устанавливаем шалон для вывода
     */
    $this->SetTemplateAction('add');
    /**
     * Если нажали кнопку "Сохранить"
     */
    if (isPost('submit_game_add')) {

      $oGame->setTitle(strip_tags(getRequest('game_title')));
      $oGame->setGenres(getRequest('game_genres'));
      $oGame->setDevelopers(getRequest('game_developers'));
      $oGame->setPublishers(getRequest('game_publishers'));
      $oGame->setDateRelease(getRequest('game_create_date_release'));
      $oGame->setLink(getRequest('game_link'));
      //$oGame->setLimitRatingTopic(getRequest('game_limit_rating_topic'));
      if ($this->oUserCurrent->isAdministrator()) {
        $oGame->setUrl((string)getRequest('game_url')); // разрешаем смену URL игры только админу
      }
      /**
       * Парсим описание блога на предмет ХТМЛ тегов
       */
      $sText=$this->Text_Parser(getRequest('game_description'));
      $oGame->setDescription($sText);

      /**
       * Запускаем проверку корректности ввода полей при редактировании игры
       */
      if (!$this->checkGameFields($oGame)) {
        return false;
      }

      $sDateRelease = (getRequest('game_create_date_release')=='') ? null : date("Y-m-d H:i:s",strtotime(getRequest('game_create_date_release')));
      $oGame->setDateRelease($sDateRelease);

      /**
       * Загрузка аватара, делаем ресайзы
       */
      if (isset($_FILES['avatar']) and is_uploaded_file($_FILES['avatar']['tmp_name'])) {
        if ($sPath=$this->PluginGamerpress_Game_UploadGameAvatar($_FILES['avatar'],$oGame)) {
          $oGame->setAvatar($sPath);
        } else {
          $this->Message_AddError($this->Lang_Get('blog_create_avatar_error'),$this->Lang_Get('error'));
          return false;
        }
      }
      /**
       * Удалить аватар
       */
      if (isset($_REQUEST['avatar_delete'])) {
        $this->PluginGamerpress_Game_DeleteGameAvatar($oGame);
        $oGame->setAvatar(null);
      }

      /**
       * Обновляем игру
       */
      if ($this->PluginGamerpress_Game_UpdateGame($oGame)) {
        Router::Location($oGame->getUrlFull());
      } else {
        $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
        return Router::Action('error');
      }
    } else {
      /**
       * Загружаем данные в форму редактирования блога
       */
      $_REQUEST['game_title']=$oGame->getTitle();
      $_REQUEST['game_genres']=$oGame->getGenres();
      $_REQUEST['game_developers']=$oGame->getDevelopers();
      $_REQUEST['game_publishers']=$oGame->getPublishers();
      $_REQUEST['game_create_date_release']=date("d-m-Y",strtotime($oGame->getDateRelease()));
      $_REQUEST['game_link']=$oGame->getLink();
      $_REQUEST['game_url']=$oGame->getUrl();
      $_REQUEST['game_description']=$oGame->getDescription();
      //$_REQUEST['game_limit_rating_topic']=$oGame->getLimitRatingTopic();
      $_REQUEST['game_id']=$oGame->getId();
    }
  }
  /**
   * Управление пользователями игры
   *
   */
  protected function EventAdminGame() {
    /**
     * Меню
     */
    $this->sMenuItemSelect='admin';
    $this->sMenuSubItemSelect='';
    /**
     * Проверяем передан ли в УРЛе номер игры
     */
    $sGameId=$this->GetParam(0);
    if (!$oGame=$this->PluginGamerpress_Game_GetGameById($sGameId)) {
      return parent::EventNotFound();
    }
    /**
     * Проверям авторизован ли пользователь
     */
    if (!$this->User_IsAuthorization()) {
      $this->Message_AddErrorSingle($this->Lang_Get('not_access'),$this->Lang_Get('error'));
      return Router::Action('error');
    }
    /**
     * Проверка на право управлением пользователями игры
     */
    if (!$this->ACL_IsAllowAdminGame($oGame, $this->oUserCurrent)) {
      return parent::EventNotFound();
    }
    /**
     * Обрабатываем сохранение формы
     */
    if (isPost('submit_game_admin')) {
      $this->Security_ValidateSendForm();

      $aUserRank=getRequest('user_rank',array());
      if (!is_array($aUserRank)) {
        $aUserRank=array();
      }
      foreach ($aUserRank as $sUserId => $sRank) {
        if (!($oGameUser=$this->PluginGamerpress_Game_GetGameUserByGameIdAndUserId($oGame->getId(),$sUserId))) {
          $this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'));
          break;
        }
        /**
         * Увеличиваем число читателей игры
         */
        if (in_array($sRank,array('administrator','moderator','reader')) and $oGameUser->getUserRole()==PluginGamerpress_ModuleGame::GAME_USER_ROLE_BAN) {
          $oGame->setCountUser($oBlog->getCountUser()+1);
        }

        switch ($sRank) {
          case 'administrator':
            $oGameUser->setUserRole(PluginGamerpress_ModuleGame::GAME_USER_ROLE_ADMINISTRATOR);
            break;
          case 'moderator':
            $oGameUser->setUserRole(PluginGamerpress_ModuleGame::GAME_USER_ROLE_MODERATOR);
            break;
          case 'reader':
            $oGameUser->setUserRole(PluginGamerpress_ModuleGame::GAME_USER_ROLE_USER);
            break;
          case 'ban':
            if ($oGameUser->getUserRole()!=PluginGamerpress_ModuleGame::GAME_USER_ROLE_BAN) {
              $oGame->setCountUser($oGame->getCountUser()-1);
            }
            $oGameUser->setUserRole(PluginGamerpress_ModuleGame::GAME_USER_ROLE_BAN);
            break;
          default:
            $oGameUser->setUserRole(PluginGamerpress_ModuleGame::GAME_USER_ROLE_GUEST);
        }
        $this->PluginGamerpress_Game_UpdateRelationGameUser($oGameUser);
        $this->Message_AddNoticeSingle($this->Lang_Get('plugin.gamerpress.game_admin_users_submit_ok'));
      }
      $this->PluginGamerpress_Game_UpdateGame($oGame);
    }
    /**
     * Текущая страница
     */
    $iPage= $this->GetParamEventMatch(1,2) ? $this->GetParamEventMatch(1,2) : 1;
    /**
     * Получаем список подписчиков игры
     */
    $aResult=$this->PluginGamerpress_Game_GetGameUsersByGameId(
      $oGame->getId(),
      array(
        PluginGamerpress_ModuleGame::GAME_USER_ROLE_BAN,
        PluginGamerpress_ModuleGame::GAME_USER_ROLE_USER,
        PluginGamerpress_ModuleGame::GAME_USER_ROLE_MODERATOR,
        PluginGamerpress_ModuleGame::GAME_USER_ROLE_ADMINISTRATOR
      ),$iPage,Config::Get('plugin.gamerpress.module.game.users_per_page')
    );
    $aGameUsers=$aResult['collection'];
    /**
     * Формируем постраничность
     */
    $aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('plugin.gamerpress.module.game.users_per_page'),Config::Get('pagination.pages.count'),Router::GetPath('game')."admin/{$oGame->getId()}");
    $this->Viewer_Assign('aPaging',$aPaging);
    /**
     * Устанавливаем title страницы
     */
    $this->Viewer_AddHtmlTitle($oGame->getTitle());
    $this->Viewer_AddHtmlTitle($this->Lang_Get('plugin.gamerpress.game_admin'));

    $this->Viewer_Assign('oGameEdit',$oGame);
    $this->Viewer_Assign('aGameUsers',$aGameUsers);
    /**
     * Устанавливаем шалон для вывода
     */
    $this->SetTemplateAction('admin');
  }
  /**
   * Проверка полей игры
   *
   * @param ModuleGame_EntityGame|null $oGame
   * @return bool
   */
  protected function checkGameFields($oGame) {
    $this->Security_ValidateSendForm();

    $bOk=true;
    /**
     * Валидируем игру
     */
    if (!$oGame->_Validate()) {
      $this->Message_AddError($oGame->_getValidateError(),$this->Lang_Get('error'));
      $bOk=false;
    }

    /**
     * Преобразуем ограничение по рейтингу в число
     */
    /*if (!func_check(getRequest('blog_limit_rating_topic'),'float')) {
      $this->Message_AddError($this->Lang_Get('blog_create_rating_error'),$this->Lang_Get('error'));
      $bOk=false;
    }*/
    return $bOk;
  }


  /**
   * Показ всех топиков
   *
   */
  protected function EventTopics() {
    $sPeriod=1; // по дефолту 1 день
    if (in_array(getRequest('period'),array(1,7,30,'all'))) {
      $sPeriod=getRequest('period');
    }
    $sShowType=$this->sCurrentEvent;
    if (!in_array($sShowType,array('discussed','top'))) {
      $sPeriod='all';
    }
    /**
     * Меню
     */
    $this->sMenuSubItemSelect=$sShowType=='newall' ? 'new' : $sShowType;
    /**
     * Передан ли номер страницы
     */
    $iPage=$this->GetParamEventMatch(0,2) ? $this->GetParamEventMatch(0,2) : 1;
    if ($iPage==1 and !getRequest('period')) {
      $this->Viewer_SetHtmlCanonical(Router::GetPath('blog').$sShowType.'/');
    }
    /**
     * Получаем список топиков
     */
    $aResult=$this->Topic_GetTopicsCollective($iPage,Config::Get('module.topic.per_page'),$sShowType,$sPeriod=='all' ? null : $sPeriod*60*60*24);
    /**
     * Если нет топиков за 1 день, то показываем за неделю (7)
     */
    if (in_array($sShowType,array('discussed','top')) and !$aResult['count'] and $iPage==1 and !getRequest('period')) {
      $sPeriod=7;
      $aResult=$this->Topic_GetTopicsCollective($iPage,Config::Get('module.topic.per_page'),$sShowType,$sPeriod=='all' ? null : $sPeriod*60*60*24);
    }
    $aTopics=$aResult['collection'];
    /**
     * Формируем постраничность
     */
    $aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.topic.per_page'),Config::Get('pagination.pages.count'),Router::GetPath('blog').$sShowType,in_array($sShowType,array('discussed','top')) ? array('period'=>$sPeriod) : array());
    /**
     * Загружаем переменные в шаблон
     */
    $this->Viewer_Assign('aTopics',$aTopics);
    $this->Viewer_Assign('aPaging',$aPaging);
    if (in_array($sShowType,array('discussed','top'))) {
      $this->Viewer_Assign('sPeriodSelectCurrent',$sPeriod);
      $this->Viewer_Assign('sPeriodSelectRoot',Router::GetPath('blog').$sShowType.'/');
    }
    /**
     * Устанавливаем шаблон вывода
     */
    $this->SetTemplateAction('index');
  }
  /**
   * Показ топика
   *
   */
  protected function EventShowTopic() {
    $sGameUrl='';
    if ($this->GetParamEventMatch(0,1)) {
      $sGameUrl=$this->sCurrentEvent;
      $iTopicId=$this->GetParamEventMatch(0,1);
      $this->sMenuItemSelect='game';
    }
    $this->sMenuSubItemSelect='';
    /**
     * Проверяем есть ли такой топик
     */
    if (!($oTopic=$this->Topic_GetTopicById($iTopicId))) {
      return parent::EventNotFound();
    }
    /**
     * Проверяем права на просмотр топика
     */
    if (!$oTopic->getPublish() and (!$this->oUserCurrent or ($this->oUserCurrent->getId()!=$oTopic->getUserId() and !$this->oUserCurrent->isAdministrator()))) {
      return parent::EventNotFound();
    }
    /**
     * Если запросили не персональный топик то перенаправляем на страницу для вывода коллективного топика
     */
    if ($sGameUrl=='') {
      Router::Location($oTopic->getUrl());
    }
    /**
     * Если номер топика правильный но УРЛ блога косяный то корректируем его и перенаправляем на нужный адрес
     */
    if ($sGameUrl!='' and $oTopic->getGame()->getUrl()!=$sGameUrl) {
      Router::Location($oTopic->getUrl());
    }
    /**
     * Обрабатываем добавление коммента
     */
    if (isset($_REQUEST['submit_comment'])) {
      $this->SubmitComment();
    }
    /**
     * Достаём комменты к топику
     */
    if (!Config::Get('module.comment.nested_page_reverse') and Config::Get('module.comment.use_nested') and Config::Get('module.comment.nested_per_page')) {
      $iPageDef=ceil($this->Comment_GetCountCommentsRootByTargetId($oTopic->getId(),'topic')/Config::Get('module.comment.nested_per_page'));
    } else {
      $iPageDef=1;
    }
    $iPage=getRequest('cmtpage',0) ? (int)getRequest('cmtpage',0) : $iPageDef;
    $aReturn=$this->Comment_GetCommentsByTargetId($oTopic->getId(),'topic',$iPage,Config::Get('module.comment.nested_per_page'));
    $iMaxIdComment=$aReturn['iMaxIdComment'];
    $aComments=$aReturn['comments'];
    /**
     * Если используется постраничность для комментариев - формируем ее
     */
    if (Config::Get('module.comment.use_nested') and Config::Get('module.comment.nested_per_page')) {
      $aPaging=$this->Viewer_MakePaging($aReturn['count'],$iPage,Config::Get('module.comment.nested_per_page'),Config::Get('pagination.pages.count'),'');
      if (!Config::Get('module.comment.nested_page_reverse') and $aPaging) {
        // переворачиваем страницы в обратном порядке
        $aPaging['aPagesLeft']=array_reverse($aPaging['aPagesLeft']);
        $aPaging['aPagesRight']=array_reverse($aPaging['aPagesRight']);
      }
      $this->Viewer_Assign('aPagingCmt',$aPaging);
    }
    /**
     * Отмечаем дату прочтения топика
     */
    if ($this->oUserCurrent) {
      $oTopicRead=Engine::GetEntity('Topic_TopicRead');
      $oTopicRead->setTopicId($oTopic->getId());
      $oTopicRead->setUserId($this->oUserCurrent->getId());
      $oTopicRead->setCommentCountLast($oTopic->getCountComment());
      $oTopicRead->setCommentIdLast($iMaxIdComment);
      $oTopicRead->setDateRead(date("Y-m-d H:i:s"));
      $this->Topic_SetTopicRead($oTopicRead);
    }
    /**
     * Выставляем SEO данные
     */
    $sTextSeo=strip_tags($oTopic->getText());
    $this->Viewer_SetHtmlDescription(func_text_words($sTextSeo, Config::Get('seo.description_words_count')));
    $this->Viewer_SetHtmlKeywords($oTopic->getTags());
    /**
     * Загружаем переменные в шаблон
     */
    $this->Viewer_Assign('oTopic',$oTopic);
    $this->Viewer_Assign('aComments',$aComments);
    $this->Viewer_Assign('iMaxIdComment',$iMaxIdComment);
    /**
     * Устанавливаем title страницы
     */
    $this->Viewer_AddHtmlTitle($oTopic->getGame()->getTitle());
    $this->Viewer_AddHtmlTitle($oTopic->getTitle());
    $this->Viewer_SetHtmlRssAlternate(Router::GetPath('rss').'comments/'.$oTopic->getId().'/',$oTopic->getTitle());
    /**
     * Устанавливаем шаблон вывода
     */
    $this->SetTemplateAction('topic');
  }
  /**
   * Страница со списком подписчиков игры
   *
   */
  protected function EventShowUsers() {
    $sGameUrl=$this->sCurrentEvent;
    /**
     * Проверяем есть ли игра с таким УРЛ
     */
    if (!($oGame=$this->PluginGamerpress_Game_GetGameByUrl($sGameUrl))) {
      return parent::EventNotFound();
    }
    /**
     * Меню
     */
    $this->sMenuSubItemSelect='';
    $this->sMenuSubGameUrl=$oGame->getUrlFull();
    /**
     * Текущая страница
     */
    $iPage= $this->GetParamEventMatch(1,2) ? $this->GetParamEventMatch(1,2) : 1;
    $aGameUsersResult=$this->PluginGamerpress_Game_GetGameUsersByGameId($oGame->getId(),PluginGamerpress_ModuleGame::GAME_USER_ROLE_USER,$iPage,Config::Get('plugin.gamerpress.module.game.users_per_page'));
    $aGameUsers=$aGameUsersResult['collection'];
    /**
     * Формируем постраничность
     */
    $aPaging=$this->Viewer_MakePaging($aGameUsersResult['count'],$iPage,Config::Get('plugin.gamerpress.module.game.users_per_page'),Config::Get('pagination.pages.count'),$oGame->getUrlFull().'users');
    $this->Viewer_Assign('aPaging',$aPaging);
    /**
     * Загружаем переменные в шаблон
     */
    $this->Viewer_Assign('aGameUsers',$aGameUsers);
    $this->Viewer_Assign('iCountGameUsers',$aGameUsersResult['count']);
    $this->Viewer_Assign('oGame',$oGame);
    /**
     * Устанавливаем title страницы
     */
    $this->Viewer_AddHtmlTitle($oGame->getTitle());
    /**
     * Устанавливаем шаблон вывода
     */
    $this->SetTemplateAction('users');
  }
  /**
   * Показ видео определенной игры
   *
   */
  protected function EventShowVideo() {
    $sGameUrl=$this->sCurrentEvent;
    /**
     * Проверяем есть ли игра с таким УРЛ
     */
    if (!($oGame=$this->PluginGamerpress_Game_GetGameByUrl($sGameUrl))) {
      return parent::EventNotFound();
    }
    /**
     * Меню
     */
    $this->sMenuSubItemSelect='';
    $this->sMenuSubGameUrl=$oGame->getUrlFull();
    $this->sMenuItemSelect='videos';

    $oVideo = Engine::GetEntity('PluginGamerpress_Video');
    $oVideo->setId(1);
    $oVideo->setUserId(1);
    $oVideo->setCountComment(1);
    $oVideo->setDateRead(date("Y-m-d H:i:s"));
    $oVideo->setForbidComment(false);
    //$oVideo->setSubscribeNewComment(o);

    /**
     * Обрабатываем добавление коммента
     */
    if (isset($_REQUEST['submit_comment'])) {
      $this->SubmitComment();
    }
    /**
     * Достаём комменты к топику
     */
    if (!Config::Get('module.comment.nested_page_reverse') and Config::Get('module.comment.use_nested') and Config::Get('module.comment.nested_per_page')) {
      $iPageDef=ceil($this->Comment_GetCountCommentsRootByTargetId($oVideo->getId(),'topic')/Config::Get('module.comment.nested_per_page'));
    } else {
      $iPageDef=1;
    }
    $iPage=getRequest('cmtpage',0) ? (int)getRequest('cmtpage',0) : $iPageDef;
    $aReturn=$this->Comment_GetCommentsByTargetId($oVideo->getId(),'video',$iPage,Config::Get('module.comment.nested_per_page'));
    $iMaxIdComment=$aReturn['iMaxIdComment'];
    $aComments=$aReturn['comments'];
    /**
     * Если используется постраничность для комментариев - формируем ее
     */
    if (Config::Get('module.comment.use_nested') and Config::Get('module.comment.nested_per_page')) {
      $aPaging=$this->Viewer_MakePaging($aReturn['count'],$iPage,Config::Get('module.comment.nested_per_page'),Config::Get('pagination.pages.count'),'');
      if (!Config::Get('module.comment.nested_page_reverse') and $aPaging) {
        // переворачиваем страницы в обратном порядке
        $aPaging['aPagesLeft']=array_reverse($aPaging['aPagesLeft']);
        $aPaging['aPagesRight']=array_reverse($aPaging['aPagesRight']);
      }
      $this->Viewer_Assign('aPagingCmt',$aPaging);
    }
    /**
     * Отмечаем дату прочтения топика
     */
    /*if ($this->oUserCurrent) {
      $oTopicRead=Engine::GetEntity('Topic_TopicRead');
      $oTopicRead->setTopicId($oTopic->getId());
      $oTopicRead->setUserId($this->oUserCurrent->getId());
      $oTopicRead->setCommentCountLast($oTopic->getCountComment());
      $oTopicRead->setCommentIdLast($iMaxIdComment);
      $oTopicRead->setDateRead(date("Y-m-d H:i:s"));
      $this->Topic_SetTopicRead($oTopicRead);
    }
    /**
     * Выставляем SEO данные
     */
    /*$sTextSeo=strip_tags($oTopic->getText());
    $this->Viewer_SetHtmlDescription(func_text_words($sTextSeo, Config::Get('seo.description_words_count')));
    $this->Viewer_SetHtmlKeywords($oTopic->getTags());
    /**
     * Загружаем переменные в шаблон
     */
    $this->Viewer_Assign('oVideo',$oVideo);
    $this->Viewer_Assign('aComments',$aComments);
    $this->Viewer_Assign('iMaxIdComment',$iMaxIdComment);
    /**
     * Устанавливаем title страницы
     */
    /*$this->Viewer_AddHtmlTitle($oTopic->getGame()->getTitle());
    $this->Viewer_AddHtmlTitle($oTopic->getTitle());
    $this->Viewer_SetHtmlRssAlternate(Router::GetPath('rss').'comments/'.$oTopic->getId().'/',$oTopic->getTitle());
    /**
     * Устанавливаем шаблон вывода
     */
    $this->SetTemplateAction('video');
  }
  /**
   * Показ всех видеозаписей определенной игры
   *
   */
  protected function EventShowVideos() {
    $sGameUrl=$this->sCurrentEvent;
    /**
     * Проверяем есть ли игра с таким УРЛ
     */
    if (!($oGame=$this->PluginGamerpress_Game_GetGameByUrl($sGameUrl))) {
      return parent::EventNotFound();
    }
    /**
     * Меню
     */
    $this->sMenuSubItemSelect='';
    $this->sMenuSubGameUrl=$oGame->getUrlFull();
    $this->sMenuItemSelect='videos';
    /**
     * Устанавливаем шаблон вывода
     */
    $this->SetTemplateAction('videos');
  }


  /**
   * Вывод топиков определенной игры
   *
   */
  protected function EventShowGame() {
    $sPeriod=1; // по дефолту 1 день
    if (in_array(getRequest('period'),array(1,7,30,'all'))) {
      $sPeriod=getRequest('period');
    }
    $sGameUrl=$this->sCurrentEvent;
    $sShowType=in_array($this->GetParamEventMatch(0,0),array('bad','new','newall','discussed','top')) ? $this->GetParamEventMatch(0,0) : 'good';
    if (!in_array($sShowType,array('discussed','top'))) {
      $sPeriod='all';
    }
    /**
     * Проверяем есть ли игра с таким УРЛ
     */
    if (!($oGame=$this->PluginGamerpress_Game_GetGameByUrl($sGameUrl))) {
      return parent::EventNotFound();
    }
    /**
     * Меню
     */
    $this->sMenuSubItemSelect=$sShowType=='newall' ? 'new' : $sShowType;
    $this->sMenuSubGameUrl=$oGame->getUrlFull();
    $this->sMenuItemSelect='main';
    /**
     * Передан ли номер страницы
     */
    $iPage= $this->GetParamEventMatch(($sShowType=='good')?0:1,2) ? $this->GetParamEventMatch(($sShowType=='good')?0:1,2) : 1;
    if ($iPage==1 and !getRequest('period') and in_array($sShowType,array('discussed','top'))) {
      $this->Viewer_SetHtmlCanonical($oGame->getUrlFull().$sShowType.'/');
    }

    /**
     * Получаем список топиков
     */
    $aResult=$this->Topic_GetTopicsByGame($oGame,$iPage,Config::Get('module.topic.per_page'),$sShowType,$sPeriod=='all' ? null : $sPeriod*60*60*24);
    /**
     * Если нет топиков за 1 день, то показываем за неделю (7)
     */
    if (in_array($sShowType,array('discussed','top')) and !$aResult['count'] and $iPage==1 and !getRequest('period')) {
      $sPeriod=7;
      $aResult=$this->Topic_GetTopicsByGame($oGame,$iPage,Config::Get('module.topic.per_page'),$sShowType,$sPeriod=='all' ? null : $sPeriod*60*60*24);
    }
    $aTopics=$aResult['collection'];
    /**
     * Формируем постраничность
     */
    $aPaging=($sShowType=='good')
      ? $this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.topic.per_page'),Config::Get('pagination.pages.count'),rtrim($oGame->getUrlFull(),'/'))
      : $this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.topic.per_page'),Config::Get('pagination.pages.count'),$oGame->getUrlFull().$sShowType,array('period'=>$sPeriod));
    /**
     * Получаем число новых топиков в текущей игре
     */
    $this->iCountTopicsGameNew=$this->Topic_GetCountTopicsByGameNew($oGame);

    $this->Viewer_Assign('aPaging',$aPaging);
    $this->Viewer_Assign('aTopics',$aTopics);
    if (in_array($sShowType,array('discussed','top'))) {
      $this->Viewer_Assign('sPeriodSelectCurrent',$sPeriod);
      $this->Viewer_Assign('sPeriodSelectRoot',$oGame->getUrlFull().$sShowType.'/');
    }

    /**
     * Выставляем SEO данные
     */
    $sTextSeo=strip_tags($oGame->getDescription());
    $this->Viewer_SetHtmlDescription(func_text_words($sTextSeo, Config::Get('seo.description_words_count')));
    /**
     * Получаем список юзеров игры
     */
    $aGameUsersResult=$this->PluginGamerpress_Game_GetGameUsersByGameId($oGame->getId(),PluginGamerpress_ModuleGame::GAME_USER_ROLE_USER,1,Config::Get('plugin.gamerpress.module.game.users_per_page'));
    $aGameUsers=$aGameUsersResult['collection'];
    $aGameModeratorsResult=$this->PluginGamerpress_Game_GetGameUsersByGameId($oGame->getId(),PluginGamerpress_ModuleGame::GAME_USER_ROLE_MODERATOR);
    $aGameModerators=$aGameModeratorsResult['collection'];
    $aGameAdministratorsResult=$this->PluginGamerpress_Game_GetGameUsersByGameId($oGame->getId(),PluginGamerpress_ModuleGame::GAME_USER_ROLE_ADMINISTRATOR);
    $aGameAdministrators=$aGameAdministratorsResult['collection'];
    /**
     * Для админов проекта получаем список игр и передаем их во вьювер
     */
    if($this->oUserCurrent and $this->oUserCurrent->isAdministrator()) {
      $aGames = $this->PluginGamerpress_Game_GetGames();
      unset($aGames[$oGame->getId()]);

      $this->Viewer_Assign('aGames',$aGames);
    }
    /**
     * Загружаем переменные в шаблон
     */
    $this->Viewer_Assign('aGameUsers',$aGameUsers);
    $this->Viewer_Assign('aGameModerators',$aGameModerators);
    $this->Viewer_Assign('aGameAdministrators',$aGameAdministrators);
    $this->Viewer_Assign('iCountGameUsers',$aGameUsersResult['count']);
    $this->Viewer_Assign('iCountGameModerators',$aGameModeratorsResult['count']);
    $this->Viewer_Assign('iCountGameAdministrators',$aGameAdministratorsResult['count']+1);
    $this->Viewer_Assign('oGame',$oGame);
    /*$this->Viewer_Assign('bCloseBlog',$bCloseBlog);
    /**
     * Устанавливаем title страницы
     */
    $this->Viewer_AddHtmlTitle($oGame->getTitle());
    $this->Viewer_SetHtmlRssAlternate(Router::GetPath('rss').'game/'.$oGame->getUrl().'/',$oGame->getTitle());
    /**
     * Устанавливаем шаблон вывода
     */
    $this->SetTemplateAction('game');
  }
  /**
   * Обработка добавление комментария к топику через ajax
   *
   */
  /*protected function AjaxAddComment() {
    /**
     * Устанавливаем формат Ajax ответа
     */
    /*$this->Viewer_SetResponseAjax('json');
    $this->SubmitComment();
  }*/
  /**
   * Обработка добавление комментария к топику
   *
   */
  /*protected function SubmitComment() {
    /**
     * Проверям авторизован ли пользователь
     */
    /*if (!$this->User_IsAuthorization()) {
      $this->Message_AddErrorSingle($this->Lang_Get('need_authorization'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверяем топик
     */
    /*if (!($oTopic=$this->Topic_GetTopicById(getRequest('cmt_target_id')))) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Возможность постить коммент в топик в черновиках
     */
    /*if (!$oTopic->getPublish() and $this->oUserCurrent->getId()!=$oTopic->getUserId() and !$this->oUserCurrent->isAdministrator()) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверяем разрешено ли постить комменты
     */
    /*if (!$this->ACL_CanPostComment($this->oUserCurrent) and !$this->oUserCurrent->isAdministrator()) {
      $this->Message_AddErrorSingle($this->Lang_Get('topic_comment_acl'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверяем разрешено ли постить комменты по времени
     */
    /*if (!$this->ACL_CanPostCommentTime($this->oUserCurrent) and !$this->oUserCurrent->isAdministrator()) {
      $this->Message_AddErrorSingle($this->Lang_Get('topic_comment_limit'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверяем запрет на добавления коммента автором топика
     */
    /*if ($oTopic->getForbidComment()) {
      $this->Message_AddErrorSingle($this->Lang_Get('topic_comment_notallow'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверяем текст комментария
     */
    /*$sText=$this->Text_Parser(getRequest('comment_text'));
    if (!func_check($sText,'text',2,10000)) {
      $this->Message_AddErrorSingle($this->Lang_Get('topic_comment_add_text_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверям на какой коммент отвечаем
     */
    /*$sParentId=(int)getRequest('reply');
    if (!func_check($sParentId,'id')) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    $oCommentParent=null;
    if ($sParentId!=0) {
      /**
       * Проверяем существует ли комментарий на который отвечаем
       */
      /*if (!($oCommentParent=$this->Comment_GetCommentById($sParentId))) {
        $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
        return;
      }
      /**
       * Проверяем из одного топика ли новый коммент и тот на который отвечаем
       */
      /*if ($oCommentParent->getTargetId()!=$oTopic->getId()) {
        $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
        return;
      }
    } else {
      /**
       * Корневой комментарий
       */
      /*$sParentId=null;
    }
    /**
     * Проверка на дублирующий коммент
     */
    /*if ($this->Comment_GetCommentUnique($oTopic->getId(),'topic',$this->oUserCurrent->getId(),$sParentId,md5($sText))) {
      $this->Message_AddErrorSingle($this->Lang_Get('topic_comment_spam'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Создаём коммент
     */
    /*$oCommentNew=Engine::GetEntity('Comment');
    $oCommentNew->setTargetId($oTopic->getId());
    $oCommentNew->setTargetType('topic');
    $oCommentNew->setTargetParentId($oTopic->getBlog()->getId());
    $oCommentNew->setUserId($this->oUserCurrent->getId());
    $oCommentNew->setText($sText);
    $oCommentNew->setDate(date("Y-m-d H:i:s"));
    $oCommentNew->setUserIp(func_getIp());
    $oCommentNew->setPid($sParentId);
    $oCommentNew->setTextHash(md5($sText));
    $oCommentNew->setPublish($oTopic->getPublish());
    /**
     * Добавляем коммент
     */
    /*if ($this->Comment_AddComment($oCommentNew)) {

      $this->Viewer_AssignAjax('sCommentId',$oCommentNew->getId());
      if ($oTopic->getPublish()) {
        /**
         * Добавляем коммент в прямой эфир если топик не в черновиках
         */
        /*$oCommentOnline=Engine::GetEntity('Comment_CommentOnline');
        $oCommentOnline->setTargetId($oCommentNew->getTargetId());
        $oCommentOnline->setTargetType($oCommentNew->getTargetType());
        $oCommentOnline->setTargetParentId($oCommentNew->getTargetParentId());
        $oCommentOnline->setCommentId($oCommentNew->getId());

        $this->Comment_AddCommentOnline($oCommentOnline);
      }
      /**
       * Сохраняем дату последнего коммента для юзера
       */
      /*$this->oUserCurrent->setDateCommentLast(date("Y-m-d H:i:s"));
      $this->User_Update($this->oUserCurrent);

      /**
       * Список емайлов на которые не нужно отправлять уведомление
       */
      /*$aExcludeMail=array($this->oUserCurrent->getMail());
      /**
       * Отправляем уведомление тому на чей коммент ответили
       */
      /*if ($oCommentParent and $oCommentParent->getUserId()!=$oTopic->getUserId() and $oCommentNew->getUserId()!=$oCommentParent->getUserId()) {
        $oUserAuthorComment=$oCommentParent->getUser();
        $aExcludeMail[]=$oUserAuthorComment->getMail();
        $this->Notify_SendCommentReplyToAuthorParentComment($oUserAuthorComment,$oTopic,$oCommentNew,$this->oUserCurrent);
      }
      /**
       * Отправка уведомления автору топика
       */
      /*$this->Subscribe_Send('topic_new_comment',$oTopic->getId(),'notify.comment_new.tpl',$this->Lang_Get('notify_subject_comment_new'),array(
        'oTopic' => $oTopic,
        'oComment' => $oCommentNew,
        'oUserComment' => $this->oUserCurrent,
      ),$aExcludeMail);
      /**
       * Добавляем событие в ленту
       */
      /*$this->Stream_write($oCommentNew->getUserId(), 'add_comment', $oCommentNew->getId(), $oTopic->getPublish() && $oTopic->getBlog()->getType()!='close');
    } else {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
    }
  }*/

  /**
   * Получение новых комментариев
   *
   */
  /*protected function AjaxResponseComment() {
    /**
     * Устанавливаем формат Ajax ответа
     */
    /*$this->Viewer_SetResponseAjax('json');
    /**
     * Пользователь авторизован?
     */
    /*if (!$this->oUserCurrent) {
      $this->Message_AddErrorSingle($this->Lang_Get('need_authorization'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Топик существует?
     */
    /*$idTopic=getRequest('idTarget',null,'post');
    if (!($oTopic=$this->Topic_GetTopicById($idTopic))) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }

    $idCommentLast=getRequest('idCommentLast',null,'post');
    $selfIdComment=getRequest('selfIdComment',null,'post');
    $aComments=array();
    /**
     * Если используется постраничность, возвращаем только добавленный комментарий
     */
    /*if (getRequest('bUsePaging',null,'post') and $selfIdComment) {
      if ($oComment=$this->Comment_GetCommentById($selfIdComment) and $oComment->getTargetId()==$oTopic->getId() and $oComment->getTargetType()=='topic') {
        $oViewerLocal=$this->Viewer_GetLocalViewer();
        $oViewerLocal->Assign('oUserCurrent',$this->oUserCurrent);
        $oViewerLocal->Assign('bOneComment',true);

        $oViewerLocal->Assign('oComment',$oComment);
        $sText=$oViewerLocal->Fetch($this->Comment_GetTemplateCommentByTarget($oTopic->getId(),'topic'));
        $aCmt=array();
        $aCmt[]=array(
          'html' => $sText,
          'obj'  => $oComment,
        );
      } else {
        $aCmt=array();
      }
      $aReturn['comments']=$aCmt;
      $aReturn['iMaxIdComment']=$selfIdComment;
    } else {
      $aReturn=$this->Comment_GetCommentsNewByTargetId($oTopic->getId(),'topic',$idCommentLast);
    }
    $iMaxIdComment=$aReturn['iMaxIdComment'];

    $oTopicRead=Engine::GetEntity('Topic_TopicRead');
    $oTopicRead->setTopicId($oTopic->getId());
    $oTopicRead->setUserId($this->oUserCurrent->getId());
    $oTopicRead->setCommentCountLast($oTopic->getCountComment());
    $oTopicRead->setCommentIdLast($iMaxIdComment);
    $oTopicRead->setDateRead(date("Y-m-d H:i:s"));
    $this->Topic_SetTopicRead($oTopicRead);

    $aCmts=$aReturn['comments'];
    if ($aCmts and is_array($aCmts)) {
      foreach ($aCmts as $aCmt) {
        $aComments[]=array(
          'html' => $aCmt['html'],
          'idParent' => $aCmt['obj']->getPid(),
          'id' => $aCmt['obj']->getId(),
        );
      }
    }

    $this->Viewer_AssignAjax('iMaxIdComment',$iMaxIdComment);
    $this->Viewer_AssignAjax('aComments',$aComments);
  }*/

  /**
   * Обработка ajax запроса на отправку
   * пользователям приглашения вступить в закрытый блог
   */
  /*protected function AjaxAddBlogInvite() {
    /**
     * Устанавливаем формат Ajax ответа
     */
    /*$this->Viewer_SetResponseAjax('json');
    $sUsers=getRequest('users',null,'post');
    $sBlogId=getRequest('idBlog',null,'post');
    /**
     * Если пользователь не авторизирован, возвращаем ошибку
     */
    /*if (!$this->User_IsAuthorization()) {
      $this->Message_AddErrorSingle($this->Lang_Get('need_authorization'),$this->Lang_Get('error'));
      return;
    }
    $this->oUserCurrent=$this->User_GetUserCurrent();
    /**
     * Проверяем существование блога
     */
    /*if(!$oBlog=$this->Blog_GetBlogById($sBlogId) or !is_string($sUsers)) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверяем, имеет ли право текущий пользователь добавлять invite в blog
     */
    /*$oBlogUser=$this->Blog_GetBlogUserByBlogIdAndUserId($oBlog->getId(),$this->oUserCurrent->getId());
    $bIsAdministratorBlog=$oBlogUser ? $oBlogUser->getIsAdministrator() : false;
    if ($oBlog->getOwnerId()!=$this->oUserCurrent->getId()  and !$this->oUserCurrent->isAdministrator() and !$bIsAdministratorBlog) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Получаем список пользователей блога (любого статуса)
     * Это полный АХТУНГ - исправить!
     */
    /*$aBlogUsersResult = $this->Blog_GetBlogUsersByBlogId(
      $oBlog->getId(),
      array(
        ModuleBlog::BLOG_USER_ROLE_BAN,
        ModuleBlog::BLOG_USER_ROLE_REJECT,
        ModuleBlog::BLOG_USER_ROLE_INVITE,
        ModuleBlog::BLOG_USER_ROLE_USER,
        ModuleBlog::BLOG_USER_ROLE_MODERATOR,
        ModuleBlog::BLOG_USER_ROLE_ADMINISTRATOR
      ),null // пока костылем
    );
    $aBlogUsers=$aBlogUsersResult['collection'];
    $aUsers=explode(',',$sUsers);

    $aResult=array();
    /**
     * Обрабатываем добавление по каждому из переданных логинов
     */
    /*foreach ($aUsers as $sUser) {
      $sUser=trim($sUser);
      if ($sUser=='') {
        continue;
      }
      /**
       * Если пользователь пытается добавить инвайт
       * самому себе, возвращаем ошибку
       */
      /*if(strtolower($sUser)==strtolower($this->oUserCurrent->getLogin())) {
        $aResult[]=array(
          'bStateError'=>true,
          'sMsgTitle'=>$this->Lang_Get('error'),
          'sMsg'=>$this->Lang_Get('blog_user_invite_add_self')
        );
        continue;
      }
      /**
       * Если пользователь не найден или неактивен,
       * возвращаем ошибку
       */
      /*if (!$oUser=$this->User_GetUserByLogin($sUser) or $oUser->getActivate()!=1) {
        $aResult[]=array(
          'bStateError'=>true,
          'sMsgTitle'=>$this->Lang_Get('error'),
          'sMsg'=>$this->Lang_Get('user_not_found',array('login'=>htmlspecialchars($sUser))),
          'sUserLogin'=>htmlspecialchars($sUser)
        );
        continue;
      }

      if(!isset($aBlogUsers[$oUser->getId()])) {
        /**
         * Создаем нового блог-пользователя со статусом INVITED
         */
        /*$oBlogUserNew=Engine::GetEntity('Blog_BlogUser');
        $oBlogUserNew->setBlogId($oBlog->getId());
        $oBlogUserNew->setUserId($oUser->getId());
        $oBlogUserNew->setUserRole(ModuleBlog::BLOG_USER_ROLE_INVITE);

        if($this->Blog_AddRelationBlogUser($oBlogUserNew)) {
          $aResult[]=array(
            'bStateError'=>false,
            'sMsgTitle'=>$this->Lang_Get('attention'),
            'sMsg'=>$this->Lang_Get('blog_user_invite_add_ok',array('login'=>htmlspecialchars($sUser))),
            'sUserLogin'=>htmlspecialchars($sUser),
            'sUserWebPath'=>$oUser->getUserWebPath(),
            'sUserAvatar48'=>$oUser->getProfileAvatarPath(48)
          );
          $this->SendBlogInvite($oBlog,$oUser);
        } else {
          $aResult[]=array(
            'bStateError'=>true,
            'sMsgTitle'=>$this->Lang_Get('error'),
            'sMsg'=>$this->Lang_Get('system_error'),
            'sUserLogin'=>htmlspecialchars($sUser)
          );
        }
      } else {
        /**
         * Попытка добавить приглашение уже существующему пользователю,
         * возвращаем ошибку (сначала определяя ее точный текст)
         */
        /*switch (true) {
          case ($aBlogUsers[$oUser->getId()]->getUserRole()==ModuleBlog::BLOG_USER_ROLE_INVITE):
            $sErrorMessage=$this->Lang_Get('blog_user_already_invited',array('login'=>htmlspecialchars($sUser)));
            break;
          case ($aBlogUsers[$oUser->getId()]->getUserRole()>ModuleBlog::BLOG_USER_ROLE_GUEST):
            $sErrorMessage=$this->Lang_Get('blog_user_already_exists',array('login'=>htmlspecialchars($sUser)));
            break;
          case ($aBlogUsers[$oUser->getId()]->getUserRole()==ModuleBlog::BLOG_USER_ROLE_REJECT):
            $sErrorMessage=$this->Lang_Get('blog_user_already_reject',array('login'=>htmlspecialchars($sUser)));
            break;
          default:
            $sErrorMessage=$this->Lang_Get('system_error');
        }
        $aResult[]=array(
          'bStateError'=>true,
          'sMsgTitle'=>$this->Lang_Get('error'),
          'sMsg'=>$sErrorMessage,
          'sUserLogin'=>htmlspecialchars($sUser)
        );
        continue;
      }
    }
    /**
     * Передаем во вьевер массив с результатами обработки по каждому пользователю
     */
    /*$this->Viewer_AssignAjax('aUsers',$aResult);
  }*/

  /**
   * Обработка ajax запроса на отправку
   * повторного приглашения вступить в закрытый блог
   */
  /*protected function AjaxReBlogInvite() {
    /**
     * Устанавливаем формат Ajax ответа
     */
    /*$this->Viewer_SetResponseAjax('json');
    $sUserId=getRequest('idUser',null,'post');
    $sBlogId=getRequest('idBlog',null,'post');
    /**
     * Если пользователь не авторизирован, возвращаем ошибку
     */
    /*if (!$this->User_IsAuthorization()) {
      $this->Message_AddErrorSingle($this->Lang_Get('need_authorization'),$this->Lang_Get('error'));
      return;
    }
    $this->oUserCurrent=$this->User_GetUserCurrent();
    /**
     * Проверяем существование блога
     */
    /*if(!$oBlog=$this->Blog_GetBlogById($sBlogId)) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Пользователь существует и активен?
     */
    /*if (!$oUser=$this->User_GetUserById($sUserId) or $oUser->getActivate()!=1) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверяем, имеет ли право текущий пользователь добавлять invite в blog
     */
    /*$oBlogUser=$this->Blog_GetBlogUserByBlogIdAndUserId($oBlog->getId(),$this->oUserCurrent->getId());
    $bIsAdministratorBlog=$oBlogUser ? $oBlogUser->getIsAdministrator() : false;
    if ($oBlog->getOwnerId()!=$this->oUserCurrent->getId()  and !$this->oUserCurrent->isAdministrator() and !$bIsAdministratorBlog) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }

    $oBlogUser=$this->Blog_GetBlogUserByBlogIdAndUserId($oBlog->getId(),$oUser->getId());
    if ($oBlogUser->getUserRole()==ModuleBlog::BLOG_USER_ROLE_INVITE) {
      $this->SendBlogInvite($oBlog,$oUser);
      $this->Message_AddNoticeSingle($this->Lang_Get('blog_user_invite_add_ok',array('login'=>$oUser->getLogin())),$this->Lang_Get('attention'));
    } else {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
    }
  }*/

  /**
   * Обработка ajax запроса на удаление вступить в закрытый блог
   */
  /*protected function AjaxRemoveBlogInvite() {
    /**
     * Устанавливаем формат Ajax ответа
     */
    /*$this->Viewer_SetResponseAjax('json');
    $sUserId=getRequest('idUser',null,'post');
    $sBlogId=getRequest('idBlog',null,'post');
    /**
     * Если пользователь не авторизирован, возвращаем ошибку
     */
    /*if (!$this->User_IsAuthorization()) {
      $this->Message_AddErrorSingle($this->Lang_Get('need_authorization'),$this->Lang_Get('error'));
      return;
    }
    $this->oUserCurrent=$this->User_GetUserCurrent();
    /**
     * Проверяем существование блога
     */
    /*if(!$oBlog=$this->Blog_GetBlogById($sBlogId)) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Пользователь существует и активен?
     */
    /*if (!$oUser=$this->User_GetUserById($sUserId) or $oUser->getActivate()!=1) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Проверяем, имеет ли право текущий пользователь добавлять invite в blog
     */
    /*$oBlogUser=$this->Blog_GetBlogUserByBlogIdAndUserId($oBlog->getId(),$this->oUserCurrent->getId());
    $bIsAdministratorBlog=$oBlogUser ? $oBlogUser->getIsAdministrator() : false;
    if ($oBlog->getOwnerId()!=$this->oUserCurrent->getId()  and !$this->oUserCurrent->isAdministrator() and !$bIsAdministratorBlog) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }

    $oBlogUser=$this->Blog_GetBlogUserByBlogIdAndUserId($oBlog->getId(),$oUser->getId());
    if ($oBlogUser->getUserRole()==ModuleBlog::BLOG_USER_ROLE_INVITE) {
      /**
       * Удаляем связь/приглашение
       */
      /*$this->Blog_DeleteRelationBlogUser($oBlogUser);
      $this->Message_AddNoticeSingle($this->Lang_Get('blog_user_invite_remove_ok',array('login'=>$oUser->getLogin())),$this->Lang_Get('attention'));
    } else {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
    }
  }*/

  /**
   * Выполняет отправку приглашения в блог
   * (по внутренней почте и на email)
   *
   * @param ModuleBlog_EntityBlog $oBlog
   * @param ModuleUser_EntityUser $oUser
   */
  /*protected function SendBlogInvite($oBlog,$oUser) {
    $sTitle=$this->Lang_Get(
      'blog_user_invite_title',
      array(
        'blog_title'=>$oBlog->getTitle()
      )
    );

    require_once Config::Get('path.root.engine').'/lib/external/XXTEA/encrypt.php';
    /**
     * Формируем код подтверждения в URL
     */
    /*$sCode=$oBlog->getId().'_'.$oUser->getId();
    $sCode=rawurlencode(base64_encode(xxtea_encrypt($sCode, Config::Get('module.blog.encrypt'))));

    $aPath=array(
      'accept'=>Router::GetPath('blog').'invite/accept/?code='.$sCode,
      'reject'=>Router::GetPath('blog').'invite/reject/?code='.$sCode
    );

    $sText=$this->Lang_Get(
      'blog_user_invite_text',
      array(
        'login'=>$this->oUserCurrent->getLogin(),
        'accept_path'=>$aPath['accept'],
        'reject_path'=>$aPath['reject'],
        'blog_title'=>$oBlog->getTitle()
      )
    );
    $oTalk=$this->Talk_SendTalk($sTitle,$sText,$this->oUserCurrent,array($oUser),false,false);
    /**
     * Отправляем пользователю заявку
     */
    /*$this->Notify_SendBlogUserInvite(
      $oUser,$this->oUserCurrent,$oBlog,
      Router::GetPath('talk').'read/'.$oTalk->getId().'/'
    );
    /**
     * Удаляем отправляющего юзера из переписки
     */
    /*$this->Talk_DeleteTalkUserByArray($oTalk->getId(),$this->oUserCurrent->getId());
  }*/

  /**
   * Обработка отправленого пользователю приглашения вступить в блог
   */
  /*protected function EventInviteBlog() {
    require_once Config::Get('path.root.engine').'/lib/external/XXTEA/encrypt.php';
    /**
     * Получаем код подтверждения из ревеста и дешефруем его
     */
    /*$sCode=xxtea_decrypt(base64_decode(rawurldecode((string)getRequest('code'))), Config::Get('module.blog.encrypt'));
    if (!$sCode) {
      return $this->EventNotFound();
    }
    list($sBlogId,$sUserId)=explode('_',$sCode,2);

    $sAction=$this->GetParam(0);
    /**
     * Получаем текущего пользователя
     */
    /*if(!$this->User_IsAuthorization()) {
      return $this->EventNotFound();
    }
    $this->oUserCurrent = $this->User_GetUserCurrent();
    /**
     * Если приглашенный пользователь не является авторизированным
     */
    /*if($this->oUserCurrent->getId()!=$sUserId) {
      return $this->EventNotFound();
    }
    /**
     * Получаем указанный блог
     */
    /*if((!$oBlog=$this->Blog_GetBlogById($sBlogId)) || $oBlog->getType()!='close') {
      return $this->EventNotFound();
    }
    /**
     * Получаем связь "блог-пользователь" и проверяем,
     * чтобы ее тип был INVITE или REJECT
     */
    /*if(!$oBlogUser=$this->Blog_GetBlogUserByBlogIdAndUserId($oBlog->getId(),$this->oUserCurrent->getId())) {
      return $this->EventNotFound();
    }
    if($oBlogUser->getUserRole()>ModuleBlog::BLOG_USER_ROLE_GUEST) {
      $sMessage=$this->Lang_Get('blog_user_invite_already_done');
      $this->Message_AddError($sMessage,$this->Lang_Get('error'),true);
      Router::Location(Router::GetPath('talk'));
      return ;
    }
    if(!in_array($oBlogUser->getUserRole(),array(ModuleBlog::BLOG_USER_ROLE_INVITE,ModuleBlog::BLOG_USER_ROLE_REJECT))) {
      $this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'),true);
      Router::Location(Router::GetPath('talk'));
      return ;
    }
    /**
     * Обновляем роль пользователя до читателя
     */
    /*$oBlogUser->setUserRole(($sAction=='accept')?ModuleBlog::BLOG_USER_ROLE_USER:ModuleBlog::BLOG_USER_ROLE_REJECT);
    if(!$this->Blog_UpdateRelationBlogUser($oBlogUser)) {
      $this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'),true);
      Router::Location(Router::GetPath('talk'));
      return ;
    }
    if ($sAction=='accept') {
      /**
       * Увеличиваем число читателей блога
       */
      /*$oBlog->setCountUser($oBlog->getCountUser()+1);
      $this->Blog_UpdateBlog($oBlog);
      $sMessage=$this->Lang_Get('blog_user_invite_accept');
      /**
       * Добавляем событие в ленту
       */
      /*$this->Stream_write($oBlogUser->getUserId(), 'join_blog', $oBlog->getId());
    } else {
      $sMessage=$this->Lang_Get('blog_user_invite_reject');
    }
    $this->Message_AddNotice($sMessage,$this->Lang_Get('attention'),true);
    /**
     * Перенаправляем на страницу личной почты
     */
    /*Router::Location(Router::GetPath('talk'));
  }*/

  /**
   * Удаление блога
   *
   */
  protected function EventDeleteBlog() {
    $this->Security_ValidateSendForm();
    /**
     * Проверяем передан ли в УРЛе номер блога
     */
    $sBlogId=$this->GetParam(0);
    if (!$oBlog=$this->Blog_GetBlogById($sBlogId)) {
      return parent::EventNotFound();
    }
    /**
     * Проверям авторизован ли пользователь
     */
    if (!$this->User_IsAuthorization()) {
      $this->Message_AddErrorSingle($this->Lang_Get('not_access'),$this->Lang_Get('error'));
      return Router::Action('error');
    }
    /**
     * проверяем есть ли право на удаление топика
     */
    if (!$bAccess=$this->ACL_IsAllowDeleteBlog($oBlog,$this->oUserCurrent)) {
      return parent::EventNotFound();
    }
    $aTopics =  $this->Topic_GetTopicsByBlogId($sBlogId);
    switch ($bAccess) {
      case ModuleACL::CAN_DELETE_BLOG_EMPTY_ONLY :
        if(is_array($aTopics) and count($aTopics)) {
          $this->Message_AddErrorSingle($this->Lang_Get('blog_admin_delete_not_empty'),$this->Lang_Get('error'),true);
          Router::Location($oBlog->getUrlFull());
        }
        break;
      case ModuleACL::CAN_DELETE_BLOG_WITH_TOPICS :
        /**
         * Если указан идентификатор блога для перемещения,
         * то делаем попытку переместить топики.
         *
         * (-1) - выбран пункт меню "удалить топики".
         */
        if($sBlogIdNew=getRequest('topic_move_to') and ($sBlogIdNew!=-1) and is_array($aTopics) and count($aTopics)) {
          if(!$oBlogNew = $this->Blog_GetBlogById($sBlogIdNew)){
            $this->Message_AddErrorSingle($this->Lang_Get('blog_admin_delete_move_error'),$this->Lang_Get('error'),true);
            Router::Location($oBlog->getUrlFull());
          }
          /**
           * Если выбранный блог является персональным, возвращаем ошибку
           */
          if($oBlogNew->getType()=='personal') {
            $this->Message_AddErrorSingle($this->Lang_Get('blog_admin_delete_move_personal'),$this->Lang_Get('error'),true);
            Router::Location($oBlog->getUrlFull());
          }
          /**
           * Перемещаем топики
           */
          $this->Topic_MoveTopics($sBlogId,$sBlogIdNew);
        }
        break;
      default:
        return parent::EventNotFound();
    }
    /**
     * Удаляяем блог и перенаправляем пользователя к списку блогов
     */
    if($this->Blog_DeleteBlog($sBlogId)) {
      $this->Message_AddNoticeSingle($this->Lang_Get('blog_admin_delete_success'),$this->Lang_Get('attention'),true);
      Router::Location(Router::GetPath('blogs'));
    } else {
      Router::Location($oBlog->getUrlFull());
    }
  }
  /**
   * Получение описания блога
   *
   */
  /*protected function AjaxBlogInfo() {
    /**
     * Устанавливаем формат Ajax ответа
     */
    /*$this->Viewer_SetResponseAjax('json');
    $sBlogId=getRequest('idBlog',null,'post');
    /**
     * Определяем тип блога и получаем его
     */
    /*if ($sBlogId==0) {
      if ($this->oUserCurrent) {
        $oBlog=$this->Blog_GetPersonalBlogByUserId($this->oUserCurrent->getId());
      }
    } else {
      $oBlog=$this->Blog_GetBlogById($sBlogId);
    }
    /**
     * если блог найден, то возвращаем описание
     */
    /*if ($oBlog) {
      $sText=$oBlog->getDescription();
      $this->Viewer_AssignAjax('sText',$sText);
    }
  }*/

  /**
   * Подключение/отключение к игре
   *
   */
  protected function AjaxGameJoin() {
    /**
     * Устанавливаем формат Ajax ответа
     */
    $this->Viewer_SetResponseAjax('json');
    /**
     * Пользователь авторизован?
     */
    if (!$this->oUserCurrent) {
      $this->Message_AddErrorSingle($this->Lang_Get('need_authorization'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Игра существует?
     */
    $idGame=getRequest('idGame',null,'post');
    if (!($oGame=$this->PluginGamerpress_Game_GetGameById($idGame))) {
      $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
      return;
    }
    /**
     * Получаем текущий статус пользователя в блоге
     */
    $oGameUser=$this->PluginGamerpress_Game_GetGameUserByGameIdAndUserId($oGame->getId(),$this->oUserCurrent->getId());
    if (!$oGameUser || ($oGameUser->getUserRole()<PluginGamerpress_ModuleGame::GAME_USER_ROLE_GUEST)) {
      if ($oGame->getOwnerId()!=$this->oUserCurrent->getId()) {
        /**
         * Присоединяем юзера к игре
         */
        $bResult=false;
        if($oGameUser) {
          $oGameUser->setUserRole(PluginGamerpress_ModuleGame::GAME_USER_ROLE_USER);
          $bResult = $this->PluginGamerpress_Game_UpdateRelationGameUser($oGameUser);
        } else {
          $oGameUserNew=Engine::GetEntity('PluginGamerpress_Game_GameUser');
          $oGameUserNew->setGameId($oGame->getId());
          $oGameUserNew->setUserId($this->oUserCurrent->getId());
          $oGameUserNew->setUserRole(PluginGamerpress_ModuleGame::GAME_USER_ROLE_USER);
          $bResult = $this->PluginGamerpress_Game_AddRelationGameUser($oGameUserNew);
        }
        if ($bResult) {
          $this->Message_AddNoticeSingle($this->Lang_Get('plugin.gamerpress.game_join_ok'),$this->Lang_Get('attention'));
          $this->Viewer_AssignAjax('bState',true);
          /**
           * Увеличиваем число читателей игры
           */
          $oGame->setCountUser($oGame->getCountUser()+1);
          $this->PluginGamerpress_Game_UpdateGame($oGame);
          $this->Viewer_AssignAjax('iCountUser',$oGame->getCountUser());
          /**
           * Добавляем событие в ленту
           */
          $this->Stream_write($this->oUserCurrent->getId(), 'join_game', $oGame->getId());
          /**
           * Добавляем подписку на этот блог в ленту пользователя
           */
          $this->Userfeed_subscribeUser($this->oUserCurrent->getId(), ModuleUserfeed::SUBSCRIBE_TYPE_BLOG, $oGame->getId());
        }
      } else {
        $this->Message_AddErrorSingle($this->Lang_Get('plugin.gamerpress.game_join_error_self'),$this->Lang_Get('attention'));
        return;
      }
    }
    if ($oGameUser && $oGameUser->getUserRole()>PluginGamerpress_ModuleGame::GAME_USER_ROLE_GUEST) {
      /**
       * Покидаем блог
       */
      if ($this->PluginGamerpress_Game_DeleteRelationGameUser($oGameUser)) {
        $this->Message_AddNoticeSingle($this->Lang_Get('plugin.gamerpress.game_leave_ok'),$this->Lang_Get('attention'));
        $this->Viewer_AssignAjax('bState',false);
        /**
         * Уменьшаем число читателей блога
         */
        $oGame->setCountUser($oGame->getCountUser()-1);
        $this->PluginGamerpress_Game_UpdateGame($oGame);
        $this->Viewer_AssignAjax('iCountUser',$oGame->getCountUser());
        /**
         * Удаляем подписку на этот блог в ленте пользователя
         */
        $this->Userfeed_unsubscribeUser($this->oUserCurrent->getId(), PluginGamerpress_ModuleUserfeed::SUBSCRIBE_TYPE_GAME, $oGame->getId());
      } else {
        $this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
        return;
      }
    }
  }
  /**
   * Выполняется при завершении работы экшена
   *
   */
  public function EventShutdown() {
    /**
     * Загружаем в шаблон необходимые переменные
     */
    $this->Viewer_Assign('sMenuHeadItemSelect',$this->sMenuHeadItemSelect);
    $this->Viewer_Assign('sMenuItemSelect',$this->sMenuItemSelect);
    $this->Viewer_Assign('sMenuSubItemSelect',$this->sMenuSubItemSelect);
    $this->Viewer_Assign('sMenuSubGameUrl',$this->sMenuSubGameUrl);
    /*$this->Viewer_Assign('iCountTopicsCollectiveNew',$this->iCountTopicsCollectiveNew);
    $this->Viewer_Assign('iCountTopicsPersonalNew',$this->iCountTopicsPersonalNew);
    $this->Viewer_Assign('iCountTopicsGameNew',$this->iCountTopicsGameNew);
    $this->Viewer_Assign('iCountTopicsNew',$this->iCountTopicsNew);*/

    $this->Viewer_Assign('GAME_USER_ROLE_GUEST', PluginGamerpress_ModuleGame::GAME_USER_ROLE_GUEST);
    $this->Viewer_Assign('GAME_USER_ROLE_USER', PluginGamerpress_ModuleGame::GAME_USER_ROLE_USER);
    $this->Viewer_Assign('GAME_USER_ROLE_MODERATOR', PluginGamerpress_ModuleGame::GAME_USER_ROLE_MODERATOR);
    $this->Viewer_Assign('GAME_USER_ROLE_ADMINISTRATOR', PluginGamerpress_ModuleGame::GAME_USER_ROLE_ADMINISTRATOR);
    $this->Viewer_Assign('GAME_USER_ROLE_INVITE', PluginGamerpress_ModuleGame::GAME_USER_ROLE_INVITE);
    $this->Viewer_Assign('GAME_USER_ROLE_REJECT', PluginGamerpress_ModuleGame::GAME_USER_ROLE_REJECT);
    $this->Viewer_Assign('GAME_USER_ROLE_BAN', PluginGamerpress_ModuleGame::GAME_USER_ROLE_BAN);
  }
}
?>
