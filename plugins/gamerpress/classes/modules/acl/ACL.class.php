<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
---------------------------------------------------------
*/

/**
 * ACL(Access Control List)
 * Модуль для разруливания ограничений по карме/рейтингу юзера
 *
 * @package modules.acl
 * @since 0.1
 */
class PluginGamerpress_ModuleACL extends PluginGamerpress_Inherit_ModuleACL {
  /**
   * Коды ответов на запрос о возможности
   * пользователя голосовать за игру
   */
  const CAN_VOTE_GAME_FALSE = 0;
  const CAN_VOTE_GAME_TRUE = 1;
  /**
   * Коды механизма удаления игры
   */
  const CAN_DELETE_GAME_EMPTY_ONLY  = 1;
  const CAN_DELETE_GAME_WITH_TOPICS = 2;

  /**
   * Проверяет может ли пользователь создавать топики в определенной игре
   *
   * @param ModuleUser_EntityUser $oUser  Пользователь
   * @param PluginGamerpress_ModuleGame_EntityGame $oGame  Игра
   * @return bool
   */
  public function CanAddTopicToGame(ModuleUser_EntityUser $oUser, PluginGamerpress_ModuleGame_EntityGame $oGame) {
    /**
     * Если юзер является создателем игры то разрешаем ему постить
     */
    if ($oUser->getId()==$oGame->getOwnerId()) {
      return true;
    }
    /**
     * Если рейтинг юзера больше либо равен порогу постинга в игре то разрешаем постинг
     */
    if ($oUser->getRating()>=$oGame->getLimitRatingTopic()) {
      return true;
    }
    return false;
  }

  /**
   * Проверяет может ли пользователь создавать игры
   *
   * @param ModuleUser_EntityUser $oUser  Пользователь
   * @return bool
   */
  public function CanCreateGame(ModuleUser_EntityUser $oUser) {
    if ($oUser->getRating()>=Config::Get('plugin.gamerpress.acl.create.game.rating')) {
      return true;
    }
    return false;
  }

  /**
   * Проверяет может ли пользователь голосовать за конкретную игру
   *
   * @param ModuleUser_EntityUser $oUser  Пользователь
   * @param PluginGamerpress_ModuleGame_EntityGame $oGame  Игра
   * @return bool
   */
  public function CanVoteGame(ModuleUser_EntityUser $oUser, PluginGamerpress_ModuleGame_EntityGame $oGame) {
    if ($oUser->getRating()>=Config::Get('plugin.gamerpress.acl.vote.game.rating')) {
      return self::CAN_VOTE_GAME_TRUE;
    }
    return self::CAN_VOTE_GAME_FALSE;
  }

  /**
   * Проверяет можно или нет юзеру постить в данную игру
   *
   * @param PluginGamerpress_ModuleGame_EntityGame $oGame  Игра
   * @param ModuleUser_EntityUser $oUser  Пользователь
   */
  public function IsAllowGame($oGame,$oUser) {
    if ($oUser->isAdministrator()) {
      return true;
    }
    if ($oUser->getRating()<=Config::Get('acl.create.topic.limit_rating')) {
      return false;
    }
    if ($oGame->getOwnerId()==$oUser->getId()) {
      return true;
    }
    if ($oGameUser=$this->PluginGamerpress_Game_GetGameUserByGameIdAndUserId($oGame->getId(),$oUser->getId())) {
      if ($this->ACL_CanAddTopicToGame($oUser,$oGame) or $oGameUser->getIsAdministrator() or $oGameUser->getIsModerator()) {
        return true;
      }
    }
    return false;
  }
  /**
   * Проверяет можно или нет пользователю удалять данную игру
   *
   * @param PluginGamerpress_ModuleGame_EntityGame $oGame  Игра
   * @param ModuleUser_EntityUser $oUser  Пользователь
   * @return bool
   */
  public function IsAllowDeleteGame($oGame,$oUser) {
    /**
     * Разрешаем если это админ сайта или автор игры
     */
    if ($oUser->isAdministrator()) {
      return self::CAN_DELETE_GAME_WITH_TOPICS;
    }
    /**
     * Разрешаем удалять администраторам игры и автору, но только пустой
     */
    if($oGame->getOwnerId()==$oUser->getId()) {
      return self::CAN_DELETE_GAME_EMPTY_ONLY;
    }

    $oGameUser=$this->PluginGamerpress_Game_GetGameUserByGameIdAndUserId($oGame->getId(),$oUser->getId());
    if($oGameUser and $oGameUser->getIsAdministrator()) {
      return self::CAN_DELETE_GAME_EMPTY_ONLY;
    }
    return false;
  }
  /**
   * Проверяет можно или нет пользователю редактировать данную игру
   *
   * @param  PluginGamerpress_ModuleGame_EntityGame $oGame Игра
   * @param  ModuleUser_EntityUser $oUser Пользователь
   * @return bool
   */
  public function IsAllowEditGame($oGame,$oUser) {
    if ($oUser->isAdministrator()) {
      return true;
    }
    /**
     * Разрешаем если это создатель игры
     */
    if ($oGame->getOwnerId() == $oUser->getId()) {
      return true;
    }
    /**
     * Явлется ли авторизованный пользователь администратором игры
     */
    $oGameUser = $this->PluginGamerpress_Game_GetGameUserByGameIdAndUserId($oGame->getId(), $oUser->getId());

    if ($oGameUser && $oGameUser->getIsAdministrator()) {
      return true;
    }
    return false;
  }
  /**
   * Проверяет можно или нет пользователю управлять пользователями игры
   *
   * @param  PluginGamerpress_ModuleGame_EntityGame $oGame Игра
   * @param  ModuleUser_EntityUser $oUser Пользователь
   * @return bool
   */
  public function IsAllowAdminGame($oGame,$oUser) {
    if ($oUser->isAdministrator()) {
      return true;
    }
    /**
     * Разрешаем если это создатель игры
     */
    if ($oGame->getOwnerId() == $oUser->getId()) {
      return true;
    }
    /**
     * Явлется ли авторизованный пользователь администратором игры
     */
    $oGameUser = $this->PluginGamerpress_Game_GetGameUserByGameIdAndUserId($oGame->getId(), $oUser->getId());
    if ($oGameUser && $oGameUser->getIsAdministrator()) {
      return true;
    }
    return false;
  }
}
?>
