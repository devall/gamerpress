<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Валидатор жанров - строка с перечислением жанров
 *
 * @package engine.modules.validate
 * @since 0.1
 */
class PluginGamerpress_ModuleValidate_EntityValidatorGenres extends ModuleValidate_EntityValidator {
  /**
   * Максимальня длина жанра
   *
   * @var int
   */
  public $max=50;
  /**
   * Минимальня длина жанра
   *
   * @var int
   */
  public $min=2;
  /**
   * Минимальное количество жанров
   *
   * @var int
   */
  public $count=15;
  /**
   * Разделитель жанров
   *
   * @var string
   */
  public $sep=',';
  /**
   * Допускать или нет пустое значение
   *
   * @var bool
   */
  public $allowEmpty=false;

  /**
   * Запуск валидации
   *
   * @param mixed $sValue Данные для валидации
   *
   * @return bool|string
   */
  public function validate($sValue) {
    if (is_array($sValue)) {
      return $this->getMessage($this->Lang_Get('plugin.gamerpress.validate_genres_empty',null,false),'msg',array('min'=>$this->min,'max'=>$this->max));
    }
    if($this->allowEmpty && $this->isEmpty($sValue)) {
      return true;
    }

    $aGenres=explode($this->sep,trim($sValue,"\r\n\t\0\x0B ."));
    $aGenresNew=array();
    $aGenresNewLow=array();
    foreach ($aGenres as $sGenre) {
      $sGenre=trim($sGenre,"\r\n\t\0\x0B .");
      $iLength=mb_strlen($sGenre, 'UTF-8');
      if ($iLength>=$this->min and $iLength<=$this->max and !in_array(mb_strtolower($sGenre,'UTF-8'),$aGenresNewLow)) {
        $aGenresNew[]=$sGenre;
        $aGenresNewLow[]=mb_strtolower($sGenre,'UTF-8');
      }
    }
    $iCount=count($aGenresNew);
    if ($iCount>$this->count) {
      return $this->getMessage($this->Lang_Get('plugin.gamerpress.validate_genres_count_more',null,false),'msg',array('count'=>$this->count));
    } elseif (!$iCount) {
      return $this->getMessage($this->Lang_Get('plugin.gamerpress.validate_genres_empty',null,false),'msg',array('min'=>$this->min,'max'=>$this->max));
    }
    /**
     * Если проверка от сущности, то возвращаем обновленное значение
     */
    if ($this->oEntityCurrent) {
      $this->setValueOfCurrentEntity($this->sFieldCurrent,join($this->sep,$aGenresNew));
    }
    return true;
  }
}
?>
