<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Валидатор разработчиков - строка с перечислением разработчиков
 *
 * @package engine.modules.validate
 * @since 0.1
 */
class PluginGamerpress_ModuleValidate_EntityValidatorDevelopers extends ModuleValidate_EntityValidator {
  /**
   * Максимальня длина разработчика
   *
   * @var int
   */
  public $max=50;
  /**
   * Минимальня длина разработчика
   *
   * @var int
   */
  public $min=2;
  /**
   * Минимальное количество разработчиков
   *
   * @var int
   */
  public $count=15;
  /**
   * Разделитель разработчиков
   *
   * @var string
   */
  public $sep=',';
  /**
   * Допускать или нет пустое значение
   *
   * @var bool
   */
  public $allowEmpty=false;

  /**
   * Запуск валидации
   *
   * @param mixed $sValue Данные для валидации
   *
   * @return bool|string
   */
  public function validate($sValue) {
    if (is_array($sValue)) {
      return $this->getMessage($this->Lang_Get('plugin.gamerpress.validate_developers_empty',null,false),'msg',array('min'=>$this->min,'max'=>$this->max));
    }
    if($this->allowEmpty && $this->isEmpty($sValue)) {
      return true;
    }

    $aDevelopers=explode($this->sep,trim($sValue,"\r\n\t\0\x0B ."));
    $aDevelopersNew=array();
    $aDevelopersNewLow=array();
    foreach ($aDevelopers as $sDeveloper) {
      $sDeveloper=trim($sDeveloper,"\r\n\t\0\x0B .");
      $iLength=mb_strlen($sDeveloper, 'UTF-8');
      if ($iLength>=$this->min and $iLength<=$this->max and !in_array(mb_strtolower($sDeveloper,'UTF-8'),$aDevelopersNewLow)) {
        $aDevelopersNew[]=$sDeveloper;
        $aDevelopersNewLow[]=mb_strtolower($sDeveloper,'UTF-8');
      }
    }
    $iCount=count($aDevelopersNew);
    if ($iCount>$this->count) {
      return $this->getMessage($this->Lang_Get('plugin.gamerpress.validate_developers_count_more',null,false),'msg',array('count'=>$this->count));
    } elseif (!$iCount) {
      return $this->getMessage($this->Lang_Get('plugin.gamerpress.validate_developers_empty',null,false),'msg',array('min'=>$this->min,'max'=>$this->max));
    }
    /**
     * Если проверка от сущности, то возвращаем обновленное значение
     */
    if ($this->oEntityCurrent) {
      $this->setValueOfCurrentEntity($this->sFieldCurrent,join($this->sep,$aDevelopersNew));
    }
    return true;
  }
}
?>
