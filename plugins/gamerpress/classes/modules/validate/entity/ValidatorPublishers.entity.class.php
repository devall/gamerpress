<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Валидатор издателей - строка с перечислением издателей
 *
 * @package engine.modules.validate
 * @since 0.1
 */
class PluginGamerpress_ModuleValidate_EntityValidatorPublishers extends ModuleValidate_EntityValidator {
  /**
   * Максимальня длина издателя
   *
   * @var int
   */
  public $max=50;
  /**
   * Минимальня длина издателя
   *
   * @var int
   */
  public $min=2;
  /**
   * Минимальное количество издателей
   *
   * @var int
   */
  public $count=15;
  /**
   * Разделитель издателей
   *
   * @var string
   */
  public $sep=',';
  /**
   * Допускать или нет пустое значение
   *
   * @var bool
   */
  public $allowEmpty=false;

  /**
   * Запуск валидации
   *
   * @param mixed $sValue Данные для валидации
   *
   * @return bool|string
   */
  public function validate($sValue) {
    if (is_array($sValue)) {
      return $this->getMessage($this->Lang_Get('plugin.gamerpress.validate_publishers_empty',null,false),'msg',array('min'=>$this->min,'max'=>$this->max));
    }
    if($this->allowEmpty && $this->isEmpty($sValue)) {
      return true;
    }

    $aPublishers=explode($this->sep,trim($sValue,"\r\n\t\0\x0B ."));
    $aPublishersNew=array();
    $aPublishersNewLow=array();
    foreach ($aPublishers as $sPublisher) {
      $sPublisher=trim($sPublisher,"\r\n\t\0\x0B .");
      $iLength=mb_strlen($sPublisher, 'UTF-8');
      if ($iLength>=$this->min and $iLength<=$this->max and !in_array(mb_strtolower($sPublisher,'UTF-8'),$aPublishersNewLow)) {
        $aPublishersNew[]=$sPublisher;
        $aPublishersNewLow[]=mb_strtolower($sPublisher,'UTF-8');
      }
    }
    $iCount=count($aPublishersNew);
    if ($iCount>$this->count) {
      return $this->getMessage($this->Lang_Get('plugin.gamerpress.validate_publishers_count_more',null,false),'msg',array('count'=>$this->count));
    } elseif (!$iCount) {
      return $this->getMessage($this->Lang_Get('plugin.gamerpress.validate_publishers_empty',null,false),'msg',array('min'=>$this->min,'max'=>$this->max));
    }
    /**
     * Если проверка от сущности, то возвращаем обновленное значение
     */
    if ($this->oEntityCurrent) {
      $this->setValueOfCurrentEntity($this->sFieldCurrent,join($this->sep,$aPublishersNew));
    }
    return true;
  }
}
?>
