<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Сущность связи пользователя и играми
 *
 * @package modules.game
 * @since 0.1
 */
class PluginGamerpress_ModuleGame_EntityGameUser extends Entity {
  /**
   * Возвращает ID игры
   *
   * @return int|null
   */
  public function getGameId() {
    return $this->_getDataOne('game_id');
  }
  /**
   * Возвращает ID пользователя
   *
   * @return int|null
   */
  public function getUserId() {
    return $this->_getDataOne('user_id');
  }
  /**
   * Возвращает статус модератор пользователь или нет
   *
   * @return bool
   */
  public function getIsModerator() {
    return ($this->getUserRole()==PluginGamerpress_ModuleGame::GAME_USER_ROLE_MODERATOR);
  }
  /**
   * Возвращает статус администратор пользователь или нет
   *
   * @return bool
   */
  public function getIsAdministrator() {
    return ($this->getUserRole()==PluginGamerpress_ModuleGame::GAME_USER_ROLE_ADMINISTRATOR);
  }
  /**
   * Возвращает текущую роль пользователя в игре
   *
   * @return int|null
   */
  public function getUserRole() {
    return $this->_getDataOne('user_role');
  }
  /**
   * Возвращает объект игры
   *
   * @return PluginGamerpress_ModuleGame_EntityGame|null
   */
  public function getGame() {
    return $this->_getDataOne('game');
  }
  /**
   * Возвращает объект пользователя
   *
   * @return ModuleUser_EntityUser|null
   */
  public function getUser() {
    return $this->_getDataOne('user');
  }


  /**
   * Устанавливает ID игры
   *
   * @param int $data
   */
  public function setGameId($data) {
    $this->_aData['game_id']=$data;
  }
  /**
   * Устанавливает ID пользователя
   *
   * @param int $data
   */
  public function setUserId($data) {
    $this->_aData['user_id']=$data;
  }
  /**
   * Устанавливает статус модератора игры
   *
   * @param bool $data
   */
  public function setIsModerator($data) {
    if($data && !$this->getIsModerator()) {
      /**
       * Повышаем статус до модератора
       */
      $this->setUserRole(PluginGamerpress_ModuleGame::GAME_USER_ROLE_MODERATOR);
    }
  }
  /**
   * Устанавливает статус администратора игры
   *
   * @param bool $data
   */
  public function setIsAdministrator($data) {
    if($data && !$this->getIsAdministrator()) {
      /**
       * Повышаем статус до администратора
       */
      $this->setUserRole(PluginGamerpress_ModuleGame::GAME_USER_ROLE_ADMINISTRATOR);
    }
  }
  /**
   * Устанавливает роль пользователя
   *
   * @param int $data
   */
  public function setUserRole($data) {
    $this->_aData['user_role']=$data;
  }
  /**
   * Устанавливает игру
   *
   * @param PluginGamerpress_ModuleGame_EntityGame $data
   */
  public function setBlog($data) {
    $this->_aData['blog']=$data;
  }
  /**
   * Устанавливаем пользователя
   *
   * @param ModuleUser_EntityUser $data
   */
  public function setUser($data) {
    $this->_aData['user']=$data;
  }
}
?>
