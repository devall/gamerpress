<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexey Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Объект сущности игры
 * @package modules.game
 * @since 0.1
 */
class PluginGamerpress_ModuleGame_EntityGame extends Entity {

  /**
   * Определяем правила валидации
   */
  public function Init() {
    parent::Init();
    $this->aValidateRules[]=array('game_title','string','max'=>200,'min'=>2,'allowEmpty'=>false,'label'=>$this->Lang_Get('plugin.gamerpress.game_create_title'));
    $this->aValidateRules[]=array('game_title','title_unique');
    $this->aValidateRules[]=array('game_genres','genres','count'=>15,'label'=>$this->Lang_Get('plugin.gamerpress.game_create_genres'),'allowEmpty'=>false);
    $this->aValidateRules[]=array('game_developers','developers','count'=>15,'label'=>$this->Lang_Get('plugin.gamerpress.game_create_developers'),'allowEmpty'=>false);
    $this->aValidateRules[]=array('game_publishers','publishers','count'=>15,'label'=>$this->Lang_Get('plugin.gamerpress.game_create_publishers'),'allowEmpty'=>true);
    $this->aValidateRules[]=array('game_date_release','date','allowEmpty'=>true,'label'=>$this->Lang_Get('plugin.gamerpress.game_create_date_release'));
    $this->aValidateRules[]=array('game_link','url','allowEmpty'=>false,'label'=>$this->Lang_Get('plugin.gamerpress.game_create_link'));
    $this->aValidateRules[]=array('game_url','url_check');
    $this->aValidateRules[]=array('game_url','url_unique');
    $this->aValidateRules[]=array('game_description','string','max'=>3000,'min'=>10,'allowEmpty'=>false,'label'=>$this->Lang_Get('plugin.gamerpress.game_create_description'));
  }

  /**
   * Проверка заголовка на уникальность
   *
   * @param string $sValue  Проверяемое значение
   * @param array $aParams  Параметры
   * @return bool|string
   */
  public function ValidateTitleUnique($sValue,$aParams) {
    /*if ($oGameEquivalent=$this->PluginGamerpress_Game_GetGameByTitle($sValue)) {
      if ($iId=$this->getId() or $iId==$oGameEquivalent->getId()) {
        return true;
      }
      return $this->Lang_Get('plugin.gamerpress.game_create_title_error_unique');
    }*/
    return true;
  }

  /**
   * Проверка разрешенности URL
   *
   * @param string $sValue  Проверяемое значение
   * @param array $aParams  Параметры
   * @return bool|string
   */
  public function ValidateUrlCheck($sValue,$aParams) {
    if (!func_check($sValue,'login',2,50)) {
      return $this->Lang_Get('plugin.gamerpress.game_create_url_error');
    }

    if (!$this->PluginGamerpress_Game_IsAllowGameURL($sValue)) {
      return $this->Lang_Get('plugin.gamerpress.game_create_url_error_badword').' '.join(', ',$this->PluginGamerpress_Game_GetGameURLs());
    }
    return true;
  }

  /**
   * Проверка URL на уникальность
   *
   * @param string $sValue  Проверяемое значение
   * @param array $aParams  Параметры
   * @return bool|string
   */
  public function ValidateUrlUnique($sValue,$aParams) {
    /**
     * Проверяем есть ли уже игра с таким URL
     */
    /*if ($oGameExists=$this->PluginGamerpress_Game_GetGameByUrl($sValue)) {
      if ($iId=$this->getId() or $iId==$oGameExists->getId()) {
        return true;
      }
      return $this->Lang_Get('plugin.gamerpress.game_create_url_error_unique');
    }*/
    return true;
  }


  /**
   * Возвращает ID игры
   *
   * @return int|null
   */
  public function getId() {
    return $this->_getDataOne('game_id');
  }
  /**
   * Возвращает ID пользователя
   *
   * @return int|null
   */
  public function getOwnerId() {
    return $this->_getDataOne('user_owner_id');
  }
  /**
   * Возвращает заголовок игры
   *
   * @return string|null
   */
  public function getTitle() {
    return $this->_getDataOne('game_title');
  }
  /**
   * Возвращает описание игры
   *
   * @return string|null
   */
  public function getDescription() {
    return $this->_getDataOne('game_description');
  }
  /**
   * Возвращает строку со списком жанров через запятую
   *
   * @return string|null
   */
  public function getGenres() {
    return $this->_getDataOne('game_genres');
  }
  /**
   * Возвращает массив жанров
   *
   * @return array
   */
  public function getGenresArray() {
    if ($this->getGenres()) {
      return explode(',',$this->getGenres());
    }
    return array();
  }
  /**
   * Возвращает строку со списком имен разработчиков через запятую
   *
   * @return string|null
   */
  public function getDevelopers() {
    return $this->_getDataOne('game_developers');
  }
  /**
   * Возвращает массив разработчиков
   *
   * @return array
   */
  public function getDevelopersArray() {
    if ($this->getDevelopers()) {
      return explode(',',$this->getDevelopers());
    }
    return array();
  }
  /**
   * Возвращает строку со списком имен издателей через запятую
   *
   * @return string|null
   */
  public function getPublishers() {
    return $this->_getDataOne('game_publishers');
  }
  /**
   * Возвращает дату выпуска игры
   *
   * @return string|null
   */
  public function getDateRelease() {
    return $this->_getDataOne('game_date_release');
  }
  /**
   * Возвращает дату создания игры
   *
   * @return string|null
   */
  public function getDateAdd() {
    return $this->_getDataOne('game_date_add');
  }
  /**
   * Возвращает дату редактирования игры
   *
   * @return string|null
   */
  public function getDateEdit() {
    return $this->_getDataOne('game_date_edit');
  }
  /**
   * Возвращает сайт игры
   *
   * @return string
   */
  public function getLink() {
    return $this->_getDataOne('game_link');
  }
  /**
   * Возвращает полный URL до игры
   *
   * @return string
   */
  public function getUrl() {
    return $this->_getDataOne('game_url');
  }
  /**
   * Возвращает полный серверный путь до аватара игры
   *
   * @return string|null
   */
  public function getAvatar() {
    return $this->_getDataOne('game_avatar');
  }
  /**
   * Возвращает полный серверный путь до аватара игры определенного размера
   *
   * @param int $iSize  Размер аватара
   * @return string
   */
  public function getAvatarPath($iSize=48) {
    if ($sPath=$this->getAvatar()) {
      return preg_replace("#_\d{1,3}x\d{1,3}(\.\w{3,4})$#", ((($iSize==0)?"":"_{$iSize}x{$iSize}") . "\\1"),$sPath);
    } else {
      return Config::Get('path.static.skin').'/images/avatar_game_'.$iSize.'x'.$iSize.'.png';
    }
  }
  /**
   * Возвращает полный URL игры
   *
   * @return string
   */
  public function getUrlFull() {
    return Router::GetPath('game').$this->getUrl().'/';
  }

  /**
   * Возвращает рейтинг игры
   *
   * @return string
   */
  public function getRating() {
    return number_format(round($this->_getDataOne('game_rating'),1), 1, '.', '');
  }
  /**
   * Возврщает количество проголосовавших за игру
   *
   * @return int|null
   */
  public function getCountVote() {
    return $this->_getDataOne('game_count_vote');
  }
  /**
   * Возвращает объект голосования за игру
   *
   * @return ModuleVote_EntityVote|null
   */
  public function getVote() {
    return $this->_getDataOne('vote');
  }

  //*************************************************************************************************************************************************

  /**
   * Устанавливает ID игры
   *
   * @param int $data
   */
  public function setId($data) {
    $this->_aData['game_id']=$data;
  }
  /**
   * Устанавливает ID пользователя
   *
   * @param int $data
   */
  public function setOwnerId($data) {
    $this->_aData['user_owner_id']=$data;
  }
  /**
   * Устанавливает заголовок игры
   *
   * @param string $data
   */
  public function setTitle($data) {
    $this->_aData['game_title']=$data;
  }
  /**
   * Устанавливает описание игры
   *
   * @param string $data
   */
  public function setDescription($data) {
    $this->_aData['game_description']=$data;
  }
  /**
   * Устанавливает список жанров в виде строки
   *
   * @param string $data
   */
  public function setGenres($data) {
    $this->_aData['game_genres']=$data;
  }
  /**
   * Устанавливает список имен разработчиков в виде строки
   *
   * @param string $data
   */
  public function setDevelopers($data) {
    $this->_aData['game_developers']=$data;
  }
  /**
   * Устанавливает список имен издателей в виде строки
   *
   * @param string $data
   */
  public function setPublishers($data) {
    $this->_aData['game_publishers']=$data;
  }
  /**
   * Устанавливает дату выпуска игры
   *
   * @param string $data
   */
  public function setDateRelease($data) {
    $this->_aData['game_date_release']=$data;
  }
  /**
   * Устанавливает дату создания игры
   *
   * @param string $data
   */
  public function setDateAdd($data) {
    $this->_aData['game_date_add']=$data;
  }
  /**
   * Устанавливает дату редактирования игры
   *
   * @param string $data
   */
  public function setDateEdit($data) {
    $this->_aData['game_date_edit']=$data;
  }
  /**
   * Устанавливает сайт игры
   *
   * @param string $data
   */
  public function setLink($data) {
    $this->_aData['game_link']=$data;
  }
  /**
   * Устанавливает URL игры
   *
   * @param string $data
   */
  public function setUrl($data) {
    $this->_aData['game_url']=$data;
  }
  /**
   * Устанавливает объект голосования за игру
   *
   * @param ModuleVote_EntityVote $data
   */
  public function setVote($data) {
    $this->_aData['vote']=$data;
  }
  /**
   * Устаналивает количество проголосовавших
   *
   * @param int $data
   */
  public function setCountVote($data) {
    $this->_aData['game_count_vote']=$data;
  }
  /**
   * Устанавливает рейтинг игры
   *
   * @return string
   */
  public function setRating($data) {
    $this->_aData['game_rating']=$data;
  }
  /**
   * Устанавливает полный серверный путь до аватара игры
   *
   * @param string $data
   */
  public function setAvatar($data) {
    $this->_aData['game_avatar']=$data;
  }
}

?>
