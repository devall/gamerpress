<?php

class PluginGamerpress_ModuleGame extends Module {
  /**
   * Возможные роли пользователя в игре
   */
  const GAME_USER_ROLE_GUEST         = 0;
  const GAME_USER_ROLE_USER          = 1;
  const GAME_USER_ROLE_MODERATOR     = 2;
  const GAME_USER_ROLE_ADMINISTRATOR = 4;
  /**
   * Пользователь, приглашенный админом блога в игру
   */
  const GAME_USER_ROLE_INVITE        = -1;
  /**
   * Пользователь, отклонивший приглашение админа
   */
  const GAME_USER_ROLE_REJECT        = -2;
  /**
   * Забаненный в игре пользователь
   */
  const GAME_USER_ROLE_BAN           = -4;

  /**
   * Объект маппера
   *
   * @var PluginGamerpress_ModuleGame_MapperGame
   */
  protected $oMapperGame = null;

  /**
   * Список URL с котрыми запрещено создавать игру
   *
   * @var array
   */
  protected $aBadGameUrl=array('new','good','bad','discussed','top','edit','add','admin','delete','invite','ajaxaddcomment','ajaxaddgameinvite','ajaxresponsecomment','ajaxregameinvite','ajaxgameinfo','ajaxgamejoin');


  public function Init() {
    $this->oMapperGame = Engine::GetMapper(__CLASS__);
    $this->oUserCurrent = $this->User_GetUserCurrent();
  }

  /**
   * Добавляет игру
   *
   * @param PluginGamerpress_ModuleGame_EntityGame $oGame Игру
   * @return PluginGamerpress_ModuleGame_EntityGame|bool
   */
  public function AddGame(PluginGamerpress_ModuleGame_EntityGame $oGame) {
    if ($sId=$this->oMapperGame->AddGame($oGame)) {
      $oGame->setId($sId);
      //чистим зависимые кеши
      $this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('game_new'));
      return $oGame;
    }
    return false;
  }
  /**
   * Обновляет игру
   *
   * @param PluginGamerpress_ModuleGame_EntityGame $oGame  Игру
   * @return PluginGamerpress_ModuleGame_EntityGame|bool
   */
  public function UpdateGame(PluginGamerpress_ModuleGame_EntityGame $oGame) {
    $oGame->setDateEdit(date("Y-m-d H:i:s"));
    $res=$this->oMapperGame->UpdateGame($oGame);
    if ($res) {
      //чистим зависимые кеши
      /**
       * TODO: topic_update
       */
      $this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('game_update',"game_update_{$oGame->getId()}"/*,"topic_update"*/));
      $this->Cache_Delete("game_{$oGame->getId()}");
      return true;
    }
    return false;
  }
  /**
   * Добавляет отношение юзера к игре, по сути присоединяет к игре
   *
   * @param PluginGamerpress_ModuleGame_EntityGameUser $oGameUser  Объект связи(отношения) игры с пользователем
   * @return bool
   */
  public function AddRelationGameUser(PluginGamerpress_ModuleGame_EntityGameUser $oGameUser) {
    if ($this->oMapperGame->AddRelationGameUser($oGameUser)) {
      $this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("game_relation_change_{$oGameUser->getUserId()}","game_relation_change_game_{$oGameUser->getGameId()}"));
      $this->Cache_Delete("game_relation_user_{$oGameUser->getGameId()}_{$oGameUser->getUserId()}");
      return true;
    }
    return false;
  }
  /**
   * Обновляет отношения пользователя с игрой
   *
   * @param PluginGamerpress_ModuleGame_EntityGameUser $oGameUser Объект отновшения
   * @return bool
   */
  public function UpdateRelationGameUser(PluginGamerpress_ModuleGame_EntityGameUser $oGameUser) {
    $this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("game_relation_change_{$oGameUser->getUserId()}","game_relation_change_game_{$oGameUser->getGameId()}"));
    $this->Cache_Delete("game_relation_user_{$oGameUser->getGameId()}_{$oGameUser->getUserId()}");
    return $this->oMapperGame->UpdateRelationGameUser($oGameUser);
  }
  /**
   * Удалет отношение юзера к игре, по сути отключает от игры
   *
   * @param PluginGamerpress_ModuleGame_EntityGameUser $oGameUser  Объект связи(отношения) игры с пользователем
   * @return bool
   */
  public function DeleteRelationGameUser(PluginGamerpress_ModuleGame_EntityGameUser $oGameUser) {
    if ($this->oMapperGame->DeleteRelationGameUser($oGameUser)) {
      $this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("game_relation_change_{$oGameUser->getUserId()}","game_relation_change_game_{$oGameUser->getGameId()}"));
      $this->Cache_Delete("game_relation_user_{$oGameUser->getGameId()}_{$oGameUser->getUserId()}");
      return true;
    }
    return false;
  }
  /**
   * Получает список игр по хозяину
   *
   * @param int $sUserId        ID пользователя
   * @param bool $bReturnIdOnly Возвращать только ID игр или полные объекты
   * @return array
   */
  public function GetGamesByOwnerId($sUserId,$bReturnIdOnly=false) {
    $data=$this->oMapperGame->GetGamesByOwnerId($sUserId);
    /**
     * Возвращаем только иденитификаторы
     */
    if($bReturnIdOnly) return $data;

    $data=$this->GetGamesAdditionalData($data);
    return $data;
  }
  /**
   * Получает список пользователей игры.
   * Если роль не указана, то считаем что поиск производиться по положительным значениям (статусом выше GUEST).
   *
   * @param int $sGameId    ID игры
   * @param int|null $iRole Роль пользователей в игре
   * @param int $iPage      Номер текущей страницы
   * @param int $iPerPage   Количество элементов на одну страницу
   * @return array
   */
  public function GetGameUsersByGameId($sGameId,$iRole=null,$iPage=1,$iPerPage=100) {
    $aFilter=array(
      'game_id'=> $sGameId,
    );
    if($iRole!==null) {
      $aFilter['user_role']=$iRole;
    }
    $s=serialize($aFilter);
    if (false === ($data = $this->Cache_Get("game_relation_user_by_filter_{$s}_{$iPage}_{$iPerPage}"))) {
      $data = array('collection'=>$this->oMapperGame->GetGameUsers($aFilter,$iCount,$iPage,$iPerPage),'count'=>$iCount);
      $this->Cache_Set($data, "game_relation_user_by_filter_{$s}_{$iPage}_{$iPerPage}", array("game_relation_change_game_{$sGameId}"), 60*60*24*3);
    }
    /**
     * Достаем дополнительные данные, для этого формируем список юзеров и делаем мульти-запрос
     */
    if ($data['collection']) {
      $aUserId=array();
      foreach ($data['collection'] as $oGameUser) {
        $aUserId[]=$oGameUser->getUserId();
      }
      $aUsers=$this->User_GetUsersAdditionalData($aUserId);
      $aGames=$this->GetGamesAdditionalData($sGameId);

      $aResults=array();
      foreach ($data['collection'] as $oGameUser) {
        if (isset($aUsers[$oGameUser->getUserId()])) {
          $oGameUser->setUser($aUsers[$oGameUser->getUserId()]);
        } else {
          $oGameUser->setUser(null);
        }
        if (isset($aGames[$oGameUser->getGameId()])) {
          $oGameUser->setGame($aGames[$oGameUser->getGameId()]);
        } else {
          $oGameUser->setGame(null);
        }
        $aResults[$oGameUser->getUserId()]=$oGameUser;
      }
      $data['collection']=$aResults;
    }
    return $data;
  }
  /**
   * Получает отношения юзера к играм(состоит в игре или нет)
   *
   * @param int $sUserId  ID пользователя
   * @param int|null $iRole Роль пользователя в игре
   * @param bool $bReturnIdOnly Возвращать только ID игр или полные объекты
   * @return array
   */
  public function GetGameUsersByUserId($sUserId,$iRole=null,$bReturnIdOnly=false) {
    $aFilter=array(
      'user_id'=> $sUserId
    );
    if($iRole!==null) {
      $aFilter['user_role']=$iRole;
    }
    $s=serialize($aFilter);
    if (false === ($data = $this->Cache_Get("game_relation_user_by_filter_$s"))) {
      $data = $this->oMapperGame->GetGameUsers($aFilter);
      $this->Cache_Set($data, "game_relation_user_by_filter_$s", array("game_update", "game_relation_change_{$sUserId}"), 60*60*24*3);
    }
    /**
     * Достаем дополнительные данные, для этого формируем список блогов и делаем мульти-запрос
     */
    $aGameId=array();
    if ($data) {
      foreach ($data as $oGameUser) {
        $aGameId[]=$oGameUser->getGameId();
      }
      /**
       * Если указано возвращать полные объекты
       */
      if(!$bReturnIdOnly) {
        $aUsers=$this->User_GetUsersAdditionalData($sUserId);
        $aGames=$this->PluginGamerpress_Game_GetGamesAdditionalData($aGameId);
        foreach ($data as $oGameUser) {
          if (isset($aUsers[$oGameUser->getUserId()])) {
            $oGameUser->setUser($aUsers[$oGameUser->getUserId()]);
          } else {
            $oGameUser->setUser(null);
          }
          if (isset($aGames[$oGameUser->getGameId()])) {
            $oGameUser->setGame($aGames[$oGameUser->getGameId()]);
          } else {
            $oGameUser->setGame(null);
          }
        }
      }
    }
    return ($bReturnIdOnly) ? $aGameId : $data;
  }
  /**
   * Получить игру по УРЛу
   *
   * @param string $sGameUrl  URL игры
   * @return PluginGamerpress_ModuleGame_EntityGame|null
   */
  public function GetGameByUrl($sGameUrl) {
    if (false === ($id = $this->Cache_Get("game_url_{$sGameUrl}"))) {
      if ($id = $this->oMapperGame->GetGameByUrl($sGameUrl)) {
        $this->Cache_Set($id, "game_url_{$sGameUrl}", array("game_update_{$id}"), 60*60*24*2);
      } else {
        $this->Cache_Set(null, "game_url_{$sGameUrl}", array('game_update','game_new'), 60*60);
      }
    }
    return $this->GetGameById($id);
  }

  /**
   * Получить игру по айдишнику(номеру)
   *
   * @param int $sGameId  ID игры
   * @return PluginGamerpress_ModuleGame_EntityGame|null
   */
  public function GetGameById($sGameId) {
    if (!is_numeric($sGameId)) {
      return null;
    }
    $aGames=$this->GetGamesAdditionalData($sGameId);
    if (isset($aGames[$sGameId])) {
      return $aGames[$sGameId];
    }
    return null;
  }

  /**
   * Получает дополнительные данные(объекты) для игр по их ID
   *
   * @param array $aGameId  Список ID игр
   * @param array $aAllowData Список типов дополнительных данных, которые нужно получить для игр
   * @param array $aOrder Порядок сортировки
   * @return array
   */
  public function GetGamesAdditionalData($aGameId,$aAllowData=null,$aOrder=null) {
    if (is_null($aAllowData)) {
      $aAllowData=array('vote','owner'=>array(),'relation_user');
    }
    func_array_simpleflip($aAllowData);
    if (!is_array($aGameId)) {
      $aGameId=array($aGameId);
    }
    /**
     * Получаем игры
     */
    $aGames=$this->GetGamesByArrayId($aGameId,$aOrder);
    /**
     * Формируем ID дополнительных данных, которые нужно получить
     */
    $aUserId=array();
    foreach ($aGames as $oGame) {
      if (isset($aAllowData['owner'])) {
        $aUserId[]=$oGame->getOwnerId();
      }
    }
    /**
     * Получаем дополнительные данные
     */
    $aGameUsers=array();
    $aGamesVote=array();
    $aUsers=isset($aAllowData['owner']) && is_array($aAllowData['owner']) ? $this->User_GetUsersAdditionalData($aUserId,$aAllowData['owner']) : $this->User_GetUsersAdditionalData($aUserId);
    if (isset($aAllowData['relation_user']) and $this->oUserCurrent) {
      $aGameUsers=$this->GetGameUsersByArrayGame($aGameId,$this->oUserCurrent->getId());
    }
    if (isset($aAllowData['vote']) and $this->oUserCurrent) {
      $aGamesVote=$this->Vote_GetVoteByArray($aGameId,'game',$this->oUserCurrent->getId());
    }
    /**
     * Добавляем данные к результату - списку блогов
     */
    foreach ($aGames as $oGame) {
      if (isset($aUsers[$oGame->getOwnerId()])) {
        $oGame->setOwner($aUsers[$oGame->getOwnerId()]);
      } else {
        $oGame->setOwner(null); // или $oGame->setOwner(new ModuleUser_EntityUser());
      }
      if (isset($aGameUsers[$oGame->getId()])) {
        $oGame->setUserIsJoin(true);
        $oGame->setUserIsAdministrator($aGameUsers[$oGame->getId()]->getIsAdministrator());
        $oGame->setUserIsModerator($aGameUsers[$oGame->getId()]->getIsModerator());
      } else {
        $oGame->setUserIsJoin(false);
        $oGame->setUserIsAdministrator(false);
        $oGame->setUserIsModerator(false);
      }
      if (isset($aGamesVote[$oGame->getId()])) {
        $oGame->setVote($aGamesVote[$oGame->getId()]);
      } else {
        $oGame->setVote(null);
      }
    }
    return $aGames;
  }

  /**
   * Возвращает список игр по ID
   *
   * @param array $aGameId  Список ID игр
   * @param array|null $aOrder  Порядок сортировки
   * @return array
   */
  public function GetGamesByArrayId($aGameId,$aOrder=null) {
    if (!$aGameId) {
      return array();
    }
    if (Config::Get('sys.cache.solid')) {
      return $this->GetGamesByArrayIdSolid($aGameId,$aOrder);
    }
    if (!is_array($aGameId)) {
      $aGameId=array($aGameId);
    }
    $aGameId=array_unique($aGameId);
    $aGames=array();
    $aGameIdNotNeedQuery=array();
    /**
     * Делаем мульти-запрос к кешу
     */
    $aCacheKeys=func_build_cache_keys($aGameId,'game_');
    if (false !== ($data = $this->Cache_Get($aCacheKeys))) {
      /**
       * проверяем что досталось из кеша
       */
      foreach ($aCacheKeys as $sValue => $sKey ) {
        if (array_key_exists($sKey,$data)) {
          if ($data[$sKey]) {
            $aGames[$data[$sKey]->getId()]=$data[$sKey];
          } else {
            $aGameIdNotNeedQuery[]=$sValue;
          }
        }
      }
    }
    /**
     * Смотрим каких игр не было в кеше и делаем запрос в БД
     */
    $aGameIdNeedQuery=array_diff($aGameId,array_keys($aGames));
    $aGameIdNeedQuery=array_diff($aGameIdNeedQuery,$aGameIdNotNeedQuery);
    $aGameIdNeedStore=$aGameIdNeedQuery;
    if ($data = $this->oMapperGame->GetGamesByArrayId($aGameIdNeedQuery)) {
      foreach ($data as $oGame) {
        /**
         * Добавляем к результату и сохраняем в кеш
         */
        $aGames[$oGame->getId()]=$oGame;
        $this->Cache_Set($oGame, "game_{$oGame->getId()}", array(), 60*60*24*4);
        $aGameIdNeedStore=array_diff($aGameIdNeedStore,array($oGame->getId()));
      }
    }
    /**
     * Сохраняем в кеш запросы не вернувшие результата
     */
    foreach ($aGameIdNeedStore as $sId) {
      $this->Cache_Set(null, "game_{$sId}", array(), 60*60*24*4);
    }
    /**
     * Сортируем результат согласно входящему массиву
     */
    $aGames=func_array_sort_by_keys($aGames,$aGameId);
    return $aGames;
  }
  /**
   * Возвращает список игр по ID, но используя единый кеш
   *
   * @param array $aGameId  Список ID игр
   * @param array|null $aOrder  Сортировка игр
   * @return array
   */
  public function GetGamesByArrayIdSolid($aGameId,$aOrder=null) {
    if (!is_array($aGameId)) {
      $aGameId=array($aGameId);
    }
    $aGameId=array_unique($aGameId);
    $aGames=array();
    $s=join(',',$aGameId);
    if (false === ($data = $this->Cache_Get("game_id_{$s}"))) {
      $data = $this->oMapperGame->GetGamesByArrayId($aGameId,$aOrder);
      foreach ($data as $oGame) {
        $aGames[$oGame->getId()]=$oGame;
      }
      $this->Cache_Set($aGames, "game_id_{$s}", array("game_update"), 60*60*24*1);
      return $aGames;
    }
    return $data;
  }

  /**
   * Состоит ли юзер в конкретной игре
   *
   * @param int $sGameId  ID игры
   * @param int $sUserId  ID пользователя
   * @return PluginGamerpress_ModuleGame_EntityGameUser|null
   */
  public function GetGameUserByGameIdAndUserId($sGameId,$sUserId) {
    if ($aGameUser=$this->GetGameUsersByArrayGame($sGameId,$sUserId)) {
      if (isset($aGameUser[$sGameId])) {
        return $aGameUser[$sGameId];
      }
    }
    return null;
  }

  /**
   * Получить список отношений игра-юзер по списку айдишников
   *
   * @param array $aGameId  Список ID игр
   * @param int $sUserId  ID пользователя
   * @return array
   */
  public function GetGameUsersByArrayGame($aGameId,$sUserId) {
    if (!$aGameId) {
      return array();
    }
    if (Config::Get('sys.cache.solid')) {
      return $this->GetGameUsersByArrayGameSolid($aGameId,$sUserId);
    }
    if (!is_array($aGameId)) {
      $aGameId=array($aGameId);
    }
    $aGameId=array_unique($aGameId);
    $aGameUsers=array();
    $aGameIdNotNeedQuery=array();
    /**
     * Делаем мульти-запрос к кешу
     */
    $aCacheKeys=func_build_cache_keys($aGameId,'game_relation_user_','_'.$sUserId);
    if (false !== ($data = $this->Cache_Get($aCacheKeys))) {
      /**
       * проверяем что досталось из кеша
       */
      foreach ($aCacheKeys as $sValue => $sKey ) {
        if (array_key_exists($sKey,$data)) {
          if ($data[$sKey]) {
            $aGameUsers[$data[$sKey]->getGameId()]=$data[$sKey];
          } else {
            $aGameIdNotNeedQuery[]=$sValue;
          }
        }
      }
    }
    /**
     * Смотрим каких игр не было в кеше и делаем запрос в БД
     */
    $aGameIdNeedQuery=array_diff($aGameId,array_keys($aGameUsers));
    $aGameIdNeedQuery=array_diff($aGameIdNeedQuery,$aGameIdNotNeedQuery);
    $aGameIdNeedStore=$aGameIdNeedQuery;
    if ($data = $this->oMapperGame->GetGameUsersByArrayGame($aGameIdNeedQuery,$sUserId)) {
      foreach ($data as $oGameUser) {
        /**
         * Добавляем к результату и сохраняем в кеш
         */
        $aGameUsers[$oGameUser->getGameId()]=$oGameUser;
        $this->Cache_Set($oGameUser, "game_relation_user_{$oGameUser->getGameId()}_{$oGameUser->getUserId()}", array(), 60*60*24*4);
        $aGameIdNeedStore=array_diff($aGameIdNeedStore,array($oGameUser->getGameId()));
      }
    }
    /**
     * Сохраняем в кеш запросы не вернувшие результата
     */
    foreach ($aGameIdNeedStore as $sId) {
      $this->Cache_Set(null, "game_relation_user_{$sId}_{$sUserId}", array(), 60*60*24*4);
    }
    /**
     * Сортируем результат согласно входящему массиву
     */
    $aGameUsers=func_array_sort_by_keys($aGameUsers,$aGameId);
    return $aGameUsers;
  }

  /**
   * Получить список отношений игра-юзер по списку айдишников используя общий кеш
   *
   * @param array $aGameId  Список ID игр
   * @param int $sUserId  ID пользователя
   * @return array
   */
  public function GetGameUsersByArrayGameSolid($aGameId,$sUserId) {
    if (!is_array($aGameId)) {
      $aGameId=array($aGameId);
    }
    $aGameId=array_unique($aGameId);
    $aGameUsers=array();
    $s=join(',',$aGameId);
    if (false === ($data = $this->Cache_Get("game_relation_user_{$sUserId}_id_{$s}"))) {
      $data = $this->oMapperGame->GetGameUsersByArrayGame($aGameId,$sUserId);
      foreach ($data as $oGameUser) {
        $aGameUsers[$oGameUser->getGameId()]=$oGameUser;
      }
      $this->Cache_Set($aGameUsers, "game_relation_user_{$sUserId}_id_{$s}", array("game_update", "game_relation_change_{$sUserId}"), 60*60*24*1);
      return $aGameUsers;
    }
    return $data;
  }


  /**
   * Возвращает список игр по фильтру
   *
   * @param array $aFilter  Фильтр выборки игр
   * @param array $aOrder   Сортировка блогов
   * @param int $iCurrPage  Номер текущей страницы
   * @param int $iPerPage   Количество элементов на одну страницу
   * @param array $aAllowData Список типов данных, которые нужно подтянуть к списку игр
   * @return array('collection'=>array,'count'=>int)
   */
  public function GetGamesByFilter($aFilter,$aOrder,$iCurrPage,$iPerPage,$aAllowData=null) {
    if (is_null($aAllowData)) {
      $aAllowData=array('owner'=>array(),'relation_user');
    }
    $sKey="game_filter_".serialize($aFilter).serialize($aOrder)."_{$iCurrPage}_{$iPerPage}";
    if (false === ($data = $this->Cache_Get($sKey))) {
      $data = array('collection'=>$this->oMapperGame->GetGamesByFilter($aFilter,$aOrder,$iCount,$iCurrPage,$iPerPage),'count'=>$iCount);
      $this->Cache_Set($data, $sKey, array("game_update","game_new"), 60*60*24*2);
    }
    $data['collection']=$this->GetGamesAdditionalData($data['collection'],$aAllowData);
    return $data;
  }

  /**
   * Получает список всех игр
   *
   * @param bool $bReturnIdOnly Возвращать только ID игр или полные объекты
   * @return array
   */
  public function GetGames($bReturnIdOnly=false) {
    $data=$this->oMapperGame->GetGames();
    /**
     * Возвращаем только иденитификаторы
     */
    if($bReturnIdOnly) return $data;

    $data=$this->GetGamesAdditionalData($data);
    return $data;
  }

  /**
   * Возвращает список разрешенных URL игр
   *
   * @return array
   */
  public function GetGameURLs() {
    return $this->aBadGameUrl;
  }

  /**
   * Проверяет разрешен ли данный URL игры
   *
   * @param string $sURL URL
   * @return bool
   */
  public function IsAllowGameURL($sURL) {
    return !in_array($sURL,$this->aBadGameUrl);
  }

  /**
   * Пересчет количества топиков в конкретной игре
   *
   * @param int $iGameId  ID игре
   * @return bool
   */
  public function RecalculateCountTopicByGameId($iGameId) {
    //чистим зависимые кеши
    $this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('game_update',"game_update_{$iGameId}"));
    $this->Cache_Delete("game_{$iBlogId}");
    return $this->oMapperGame->RecalculateCountTopic($iGameId);
  }

  /**
   * Загружает аватар в игры
   *
   * @param array $aFile  Массив $_FILES при загрузке аватара
   * @param PluginGamerpress_ModuleGame_EntityGame $oGame Игры
   * @return bool
   */
  public function UploadGameAvatar($aFile,$oGame) {
    if(!is_array($aFile) || !isset($aFile['tmp_name'])) {
      return false;
    }

    $sFileTmp=Config::Get('sys.cache.dir').func_generator();
    if (!move_uploaded_file($aFile['tmp_name'],$sFileTmp)) {
      return false;
    }

    $sPath=$this->Image_GetIdDir($oGame->getOwnerId());
    $aParams=$this->Image_BuildParams('avatar');

    $oImage=$this->Image_CreateImageObject($sFileTmp);
    /**
     * Если объект изображения не создан,
     * возвращаем ошибку
     */
    if($sError=$oImage->get_last_error()) {
      // Вывод сообщения об ошибки, произошедшей при создании объекта изображения
      // $this->Message_AddError($sError,$this->Lang_Get('error'));
      @unlink($sFileTmp);
      return false;
    }
    /**
     * Срезаем квадрат
     */
    $oImage = $this->Image_CropSquare($oImage);

    $aSize=Config::Get('plugin.gamerpress.module.game.avatar_size');
    rsort($aSize,SORT_NUMERIC);
    $sSizeBig=array_shift($aSize);
    if ($oImage && $sFileAvatar=$this->Image_Resize($sFileTmp,$sPath,"avatar_game_{$oGame->getUrl()}_{$sSizeBig}x{$sSizeBig}",Config::Get('view.img_max_width'),Config::Get('view.img_max_height'),$sSizeBig,$sSizeBig,false,$aParams,$oImage)) {
      foreach ($aSize as $iSize) {
        if ($iSize==0) {
          $this->Image_Resize($sFileTmp,$sPath,"avatar_game_{$oGame->getUrl()}",Config::Get('view.img_max_width'),Config::Get('view.img_max_height'),null,null,false,$aParams,$oImage);
        } else {
          $this->Image_Resize($sFileTmp,$sPath,"avatar_game_{$oGame->getUrl()}_{$iSize}x{$iSize}",Config::Get('view.img_max_width'),Config::Get('view.img_max_height'),$iSize,$iSize,false,$aParams,$oImage);
        }
      }
      @unlink($sFileTmp);
      /**
       * Если все нормально, возвращаем расширение загруженного аватара
       */
      return $this->Image_GetWebPath($sFileAvatar);
    }
    @unlink($sFileTmp);
    /**
     * В случае ошибки, возвращаем false
     */
    return false;
  }
  /**
   * Удаляет аватар игры с сервера
   *
   * @param PluginGamerpress_ModuleGame_EntityGame $oGame Игры
   */
  public function DeleteGameAvatar($oGame) {
    /**
     * Если аватар есть, удаляем его и его рейсайзы
     */
    if($oGame->getAvatar()) {
      $aSize=array_merge(Config::Get('plugin.gamerpress.module.game.avatar_size'),array(48));
      foreach ($aSize as $iSize) {
        $this->Image_RemoveFile($this->Image_GetServerPath($oGame->getAvatarPath($iSize)));
      }
    }
  }
}
?>
