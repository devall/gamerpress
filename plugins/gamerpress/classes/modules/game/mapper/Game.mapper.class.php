<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Маппер для работы с БД по части игр
 *
 * @package modules.game
 * @since 0.1
 */
class PluginGamerpress_ModuleGame_MapperGame extends Mapper {
  /**
   * Добавляет игру в БД
   *
   * @param PluginGamerpress_ModuleGame_EntityGame $oGame  Объект игры
   * @return int|bool
   */
  public function AddGame(PluginGamerpress_ModuleGame_EntityGame $oGame) {
    $sql = "INSERT INTO ".Config::Get('plugin.gamerpress.db.table.game')."
      (user_owner_id,
      game_title,
      game_description,
      game_genres,
      game_developers,
      game_publishers,
      game_date_release,
      game_date_add,
      "./*blog_limit_rating_topic,*/"
      game_link,
      game_url,
      game_avatar
      )
      VALUES(?d,  ?,  ?,  ?,  ?,  ?,  ?,  ?,  ? ,  ?,  ?)
    ";
    if ($iId=$this->oDb->query($sql,$oGame->getOwnerId(),
                                    $oGame->getTitle(),
                                    $oGame->getDescription(),
                                    $oGame->getGenres(),
                                    $oGame->getDevelopers(),
                                    $oGame->getPublishers(),
                                    $oGame->getDateRelease(),
                                    $oGame->getDateAdd(),
                                    /*$oGame->getLimitRatingTopic(),*/
                                    $oGame->getLink(),
                                    $oGame->getUrl(),
                                    $oGame->getAvatar())) {
      return $iId;
    }
    return false;
  }
  /**
   * Обновляет игру в БД
   *
   * @param PluginGamerpress_ModuleGame_EntityGame $oGame Объект игры
   * @return bool
   */
  public function UpdateGame(PluginGamerpress_ModuleGame_EntityGame $oGame) {
    $sql = "UPDATE ".Config::Get('plugin.gamerpress.db.table.game')."
      SET
        game_title= ?,
        game_description= ?,
        game_genres= ?,
        game_developers= ?,
        game_publishers= ?,
        game_date_release= ?,
        game_date_edit= ?,
        game_rating= ?f,
        game_count_vote = ?d,
        game_count_user= ?d,
        game_count_topic= ?d,
        "./*game_limit_rating_topic= ?f ,*/"
        game_link= ?,
        game_url= ?,
        game_avatar= ?
      WHERE
        game_id = ?d
    ";
    if ($this->oDb->query($sql,$oGame->getTitle(),
                               $oGame->getDescription(),
                               $oGame->getGenres(),
                               $oGame->getDevelopers(),
                               $oGame->getPublishers(),
                               $oGame->getDateRelease(),
                               $oGame->getDateEdit(),
                               $oGame->getRating(),
                               $oGame->getCountVote(),
                               $oGame->getCountUser(),
                               $oGame->getCountTopic(),
                               /*$oGame->getLimitRatingTopic(),*/
                               $oGame->getLink(),
                               $oGame->getUrl(),
                               $oGame->getAvatar(),
                               $oGame->getId())) {
      return true;
    }
    return false;
  }
  /**
   * Получает игру по URL
   *
   * @param string $sUrl URL игры
   * @return PluginGamerpress_ModuleGame_EntityGame|null
   */
  public function GetGameByUrl($sUrl) {
    $sql = "SELECT
        b.game_id
      FROM
        ".Config::Get('plugin.gamerpress.db.table.game')." as b
      WHERE
        b.game_url = ?
        ";
    if ($aRow=$this->oDb->selectRow($sql,$sUrl)) {
      return $aRow['game_id'];
    }
    return null;
  }

  /**
   * Получает список игр по ID
   *
   * @param array $aArrayId Список ID игр
   * @param array|null $aOrder  Сортировка игр
   * @return array
   */
  public function GetGamesByArrayId($aArrayId,$aOrder=null) {
    if (!is_array($aArrayId) or count($aArrayId)==0) {
      return array();
    }

    if (!is_array($aOrder)) $aOrder=array($aOrder);
    $sOrder='';
    foreach ($aOrder as $key=>$value) {
      $value=(string)$value;
      if (!in_array($key,array('game_id','game_title','game_rating','game_count_user','game_date_add'))) {
        unset($aOrder[$key]);
      } elseif (in_array($value,array('asc','desc'))) {
        $sOrder.=" {$key} {$value},";
      }
    }
    $sOrder=trim($sOrder,',');

    $sql = "SELECT
          *
        FROM
          ".Config::Get('plugin.gamerpress.db.table.game')."
        WHERE
          game_id IN(?a)
        ORDER BY
          { FIELD(game_id,?a) } ";
    if ($sOrder!='') $sql.=$sOrder;

    $aGames=array();
    if ($aRows=$this->oDb->select($sql,$aArrayId,$sOrder=='' ? $aArrayId : DBSIMPLE_SKIP)) {
      foreach ($aRows as $aGame) {
        $aGames[]=Engine::GetEntity('PluginGamerpress_Game',$aGame);
      }
    }
    return $aGames;
  }
  /**
   * Добавляет свзяь пользователя с игрой в БД
   *
   * @param PluginGamerpress_ModuleGame_EntityGameUser $oGameUser Объект отношения пользователя с игрой
   * @return bool
   */
  public function AddRelationGameUser(PluginGamerpress_ModuleGame_EntityGameUser $oGameUser) {
    $sql = "INSERT INTO ".Config::Get('plugin.gamerpress.db.table.game_user')."
      (game_id,
      user_id,
      user_role
      )
      VALUES(?d,  ?d, ?d)
    ";
    if ($this->oDb->query($sql,$oGameUser->getGameId(),$oGameUser->getUserId(),$oGameUser->getUserRole())===0) {
      return true;
    }
    return false;
  }
  /**
   * Обновляет отношение пользователя с игрой
   *
   * @param PluginGamerpress_ModuleGame_EntityGameUser $oGameUser  Объект отношения пользователя с игрой
   * @return bool
   */
  public function UpdateRelationGameUser(PluginGamerpress_ModuleGame_EntityGameUser $oGameUser) {
    $sql = "UPDATE ".Config::Get('plugin.gamerpress.db.table.game_user')."
      SET
        user_role = ?d
      WHERE
        game_id = ?d
        AND
        user_id = ?d
    ";
    if ($this->oDb->query($sql,$oGameUser->getUserRole(),$oGameUser->getGameId(),$oGameUser->getUserId())) {
      return true;
    }
    return false;
  }
  /**
   * Удаляет отношение пользователя с игрой
   *
   * @param PluginGamerpress_ModuleGame_EntityGameUser $oGameUser  Объект отношения пользователя с игрой
   * @return bool
   */
  public function DeleteRelationGameUser(PluginGamerpress_ModuleGame_EntityGameUser $oGameUser) {
    $sql = "DELETE FROM ".Config::Get('plugin.gamerpress.db.table.game_user')."
      WHERE
        game_id = ?d
        AND
        user_id = ?d
    ";
    if ($this->oDb->query($sql,$oGameUser->getGameId(),$oGameUser->getUserId())) {
      return true;
    }
    return false;
  }
  /**
   * Получает список отношений пользователей с играми
   *
   * @param array $aFilter  Фильтр поиска отношений
   * @param int $iCount     Возвращает общее количество элементов
   * @param int $iCurrPage  Номер текущей страницы
   * @param int $iPerPage   Количество элементов на одну страницу
   * @return array
   */
  public function GetGameUsers($aFilter,&$iCount=null,$iCurrPage=null,$iPerPage=null) {
    $sWhere=' 1=1 ';
    if (isset($aFilter['game_id'])) {
      $sWhere.=" AND bu.game_id =  ".(int)$aFilter['game_id'];
    }
    if (isset($aFilter['user_id'])) {
      $sWhere.=" AND bu.user_id =  ".(int)$aFilter['user_id'];
    }
    if (isset($aFilter['user_role'])) {
      if(!is_array($aFilter['user_role'])) {
        $aFilter['user_role']=array($aFilter['user_role']);
      }
      $sWhere.=" AND bu.user_role IN ('".join("', '",$aFilter['user_role'])."')";
    } else {
      $sWhere.=" AND bu.user_role>".PluginGamerpress_ModuleGame::GAME_USER_ROLE_GUEST;
    }

    $sql = "SELECT
          bu.*
        FROM
          ".Config::Get('plugin.gamerpress.db.table.game_user')." as bu
        WHERE
          ".$sWhere." ";

    if (is_null($iCurrPage)) {
      $aRows=$this->oDb->select($sql);
    } else {
      $sql.=" LIMIT ?d, ?d ";
      $aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage);
    }

    $aGameUsers=array();
    if ($aRows) {
      foreach ($aRows as $aUser) {
        $aGameUsers[]=Engine::GetEntity('PluginGamerpress_Game_GameUser',$aUser);
      }
    }
    return $aGameUsers;
  }
  /**
   * Получает список отношений пользователя к играм
   *
   * @param array $aArrayId Список ID игр
   * @param int $sUserId ID игр
   * @return array
   */
  public function GetGameUsersByArrayGame($aArrayId,$sUserId) {
    if (!is_array($aArrayId) or count($aArrayId)==0) {
      return array();
    }

    $sql = "SELECT
          bu.*
        FROM
          ".Config::Get('plugin.gamerpress.db.table.game_user')." as bu
        WHERE
          bu.game_id IN(?a)
          AND
          bu.user_id = ?d ";
    $aGameUsers=array();
    if ($aRows=$this->oDb->select($sql,$aArrayId,$sUserId)) {
      foreach ($aRows as $aUser) {
        $aGameUsers[]=Engine::GetEntity('PluginGamerpress_Game_GameUser',$aUser);
      }
    }
    return $aGameUsers;
  }

  /**
   * Получает список игр по фильтру
   *
   * @param array $aFilter  Фильтр выборки
   * @param array $aOrder   Сортировка
   * @param int $iCount     Возвращает общее количество элментов
   * @param int $iCurrPage  Номер текущей страницы
   * @param int $iPerPage   Количество элементов на одну страницу
   * @return array
   */
  public function GetGamesByFilter($aFilter,$aOrder,&$iCount,$iCurrPage,$iPerPage) {
    $aOrderAllow=array('game_id','game_title','game_rating','game_count_user','game_count_topic');
    $sOrder='';
    foreach ($aOrder as $key=>$value) {
      if (!in_array($key,$aOrderAllow)) {
        unset($aOrder[$key]);
      } elseif (in_array($value,array('asc','desc'))) {
        $sOrder.=" {$key} {$value},";
      }
    }
    $sOrder=trim($sOrder,',');
    if ($sOrder=='') {
      $sOrder=' game_id desc ';
    }

    $sql = "SELECT
          game_id
        FROM
          ".Config::Get('plugin.gamerpress.db.table.game')."
        WHERE
          1 = 1
          { AND game_id = ?d }
          { AND user_owner_id = ?d }
          { AND game_url = ? }
          { AND game_title LIKE ? }
        ORDER by {$sOrder}
        LIMIT ?d, ?d ;
          ";
    $aResult=array();
    if ($aRows=$this->oDb->selectPage($iCount,$sql,
                      isset($aFilter['id']) ? $aFilter['id'] : DBSIMPLE_SKIP,
                      isset($aFilter['user_owner_id']) ? $aFilter['user_owner_id'] : DBSIMPLE_SKIP,
                      isset($aFilter['url']) ? $aFilter['url'] : DBSIMPLE_SKIP,
                      isset($aFilter['title']) ? $aFilter['title'] : DBSIMPLE_SKIP,
                      ($iCurrPage-1)*$iPerPage, $iPerPage
    )) {
      foreach ($aRows as $aRow) {
        $aResult[]=$aRow['game_id'];
      }
    }
    return $aResult;
  }
  /**
   * Получить список игр по хозяину
   *
   * @param int $sUserId ID пользователя
   * @return array
   */
  public function GetGamesByOwnerId($sUserId) {
    $sql = "SELECT
      g.game_id
      FROM
        ".Config::Get('plugin.gamerpress.db.table.game')." as g
      WHERE
        g.user_owner_id = ?
        ";
    $aGames=array();
    if ($aRows=$this->oDb->select($sql,$sUserId)) {
      foreach ($aRows as $aGame) {
        $aGames[]=$aGame['game_id'];
      }
    }
    return $aGames;
  }
  /**
   * Возвращает список всех игр
   *
   * @return array
   */
  public function GetGames() {
    $sql = "SELECT
        game_id
      FROM
        ".Config::Get('plugin.gamerpress.db.table.game')."";
    $aGames=array();
    if ($aRows=$this->oDb->select($sql)) {
      foreach ($aRows as $aGame) {
        $aGames[]=$aGame['game_id'];
      }
    }
    return $aGames;
  }
  /**
   * Пересчитывает число топиков в играх
   *
   * @param int|null $iGameId ID игры
   * @return bool
   */
  public function RecalculateCountTopic($iGameId=null) {
    $sql = "
                UPDATE ".Config::Get('plugin.gamerpress.db.table.game')." g
                SET g.Game_count_topic = (
                    SELECT count(*)
                    FROM ".Config::Get('db.table.topic')." t
                    WHERE
                        t.game_id = g.game_id
                    AND
                        t.topic_publish = 1
                )
                WHERE 1=1
                  { and g.game_id = ?d }
            ";
    if ($this->oDb->query($sql,is_null($iGameId) ? DBSIMPLE_SKIP : $iGameId)) {
      return true;
    }
    return false;
  }
}

?>
