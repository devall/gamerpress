<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Модуль для работы с топиками
 *
 * @package modules.topic
 * @since 0.1
 */
class PluginGamerpress_ModuleTopic extends PluginGamerpress_Inherit_ModuleTopic {

  /**
   * Добавляет топик
   *
   * @param ModuleTopic_EntityTopic $oTopic Объект топика
   * @return ModuleTopic_EntityTopic|bool
   */
  public function AddTopic(ModuleTopic_EntityTopic $oTopic) {
    if ($sId=$this->oMapperTopic->AddTopic($oTopic)) {
      $oTopic->setId($sId);
      if ($oTopic->getPublish() and $oTopic->getTags()) {
        $aTags=explode(',',$oTopic->getTags());
        foreach ($aTags as $sTag) {
          $oTag=Engine::GetEntity('Topic_TopicTag');
          $oTag->setTopicId($oTopic->getId());
          $oTag->setUserId($oTopic->getUserId());
          $oTag->setBlogId($oTopic->getBlogId());
          $oTag->setGameId($oTopic->getGameId());
          $oTag->setText($sTag);
          $this->AddTopicTag($oTag);
        }
      }
      //чистим зависимые кеши
      $this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('topic_new',"topic_update_user_{$oTopic->getUserId()}","topic_new_blog_{$oTopic->getBlogId()}"));
      return $oTopic;
    }
    return false;
  }
  /**
   * Список топиков из игр
   *
   * @param PluginGamerpress_ModuleGame_EntityGame $oGame  Объект игры
   * @param int $iPage  Номер страницы
   * @param int $iPerPage Количество элементов на страницу
   * @param string $sShowType Тип выборки топиков
   * @param string $sPeriod Период в виде секунд или конкретной даты
   * @return array
   */
  public function GetTopicsByGame($oGame,$iPage,$iPerPage,$sShowType='good',$sPeriod=null) {
    if (is_numeric($sPeriod)) {
      // количество последних секунд
      $sPeriod=date("Y-m-d H:00:00",time()-$sPeriod);
    }
    $aFilter=array(
      'topic_publish' => 1,
      'game_id' => $oGame->getId(),
    );
    if ($sPeriod) {
      $aFilter['topic_date_more'] = $sPeriod;
    }
    switch ($sShowType) {
      case 'good':
        $aFilter['topic_rating']=array(
          'value' => Config::Get('module.blog.collective_good'),
          'type'  => 'top',
        );
        break;
      case 'bad':
        $aFilter['topic_rating']=array(
          'value' => Config::Get('module.blog.collective_good'),
          'type'  => 'down',
        );
        break;
      case 'new':
        $aFilter['topic_new']=date("Y-m-d H:00:00",time()-Config::Get('module.topic.new_time'));
        break;
      case 'newall':
        // нет доп фильтра
        break;
      case 'discussed':
        $aFilter['order']=array('t.topic_count_comment desc','t.topic_id desc');
        break;
      case 'top':
        $aFilter['order']=array('t.topic_rating desc','t.topic_id desc');
        break;
      default:
        break;
    }
    return $this->GetTopicsByFilter($aFilter,$iPage,$iPerPage);
  }

  /**
   * Получает число новых топиков из игры
   *
   * @param PluginGamerpress_ModuleGame_EntityGame $oGame Объект игры
   * @return int
   */
  public function GetCountTopicsByGameNew($oGame) {
    $sDate=date("Y-m-d H:00:00",time()-Config::Get('module.topic.new_time'));
    $aFilter=array(
      'topic_publish' => 1,
      'game_id' => $oGame->getId(),
      'topic_new' => $sDate,

    );
    return $this->GetCountTopicsByFilter($aFilter);
  }
  /**
   * Получает дополнительные данные(объекты) для топиков по их ID
   *
   * @param array $aTopicId Список ID топиков
   * @param array|null $aAllowData Список типов дополнительных данных, которые нужно подключать к топикам
   * @return array
   */
  public function GetTopicsAdditionalData($aTopicId,$aAllowData=null) {
    if (is_null($aAllowData)) {
      $aAllowData=array(
        'user'=>array(),
        'blog'=>array('owner'=>array(),'relation_user'),
        'game'=>array('owner'=>array(),'relation_user'),
        'vote',
        'favourite',
        'comment_new'
      );
    }
    func_array_simpleflip($aAllowData);
    if (!is_array($aTopicId)) {
      $aTopicId=array($aTopicId);
    }
    /**
     * Получаем "голые" топики
     */
    $aTopics=$this->GetTopicsByArrayId($aTopicId);
    /**
     * Формируем ID дополнительных данных, которые нужно получить
     */
    $aUserId=array();
    $aBlogId=array();
    $aGameId=array();
    $aTopicIdQuestion=array();
    $aPhotoMainId=array();
    foreach ($aTopics as $oTopic) {
      if (isset($aAllowData['user'])) {
        $aUserId[]=$oTopic->getUserId();
      }
      if (isset($aAllowData['blog'])) {
        $aBlogId[]=$oTopic->getBlogId();
      }
      if (isset($aAllowData['game'])) {
        $aGameId[]=$oTopic->getGameId();
      }
      if ($oTopic->getType()=='question') {
        $aTopicIdQuestion[]=$oTopic->getId();
      }
      if ($oTopic->getType()=='photoset' and $oTopic->getPhotosetMainPhotoId()) {
        $aPhotoMainId[]=$oTopic->getPhotosetMainPhotoId();
      }
    }
    /**
     * Получаем дополнительные данные
     */
    $aTopicsVote=array();
    $aFavouriteTopics=array();
    $aTopicsQuestionVote=array();
    $aTopicsRead=array();
    $aUsers=isset($aAllowData['user']) && is_array($aAllowData['user']) ? $this->User_GetUsersAdditionalData($aUserId,$aAllowData['user']) : $this->User_GetUsersAdditionalData($aUserId);
    $aBlogs=isset($aAllowData['blog']) && is_array($aAllowData['blog']) ? $this->Blog_GetBlogsAdditionalData($aBlogId,$aAllowData['blog']) : $this->Blog_GetBlogsAdditionalData($aBlogId);
    $aGames=isset($aAllowData['game']) && is_array($aAllowData['game']) ? $this->PluginGamerpress_Game_GetGamesAdditionalData($aGameId,$aAllowData['game']) : $this->PluginGamerpress_Game_GetGamesAdditionalData($aGameId);
    if (isset($aAllowData['vote']) and $this->oUserCurrent) {
      $aTopicsVote=$this->Vote_GetVoteByArray($aTopicId,'topic',$this->oUserCurrent->getId());
      $aTopicsQuestionVote=$this->GetTopicsQuestionVoteByArray($aTopicIdQuestion,$this->oUserCurrent->getId());
    }
    if (isset($aAllowData['favourite']) and $this->oUserCurrent) {
      $aFavouriteTopics=$this->GetFavouriteTopicsByArray($aTopicId,$this->oUserCurrent->getId());
    }
    if (isset($aAllowData['comment_new']) and $this->oUserCurrent) {
      $aTopicsRead=$this->GetTopicsReadByArray($aTopicId,$this->oUserCurrent->getId());
    }
    $aPhotosetMainPhotos=$this->GetTopicPhotosByArrayId($aPhotoMainId);
    /**
     * Добавляем данные к результату - списку топиков
     */
    foreach ($aTopics as $oTopic) {
      if (isset($aUsers[$oTopic->getUserId()])) {
        $oTopic->setUser($aUsers[$oTopic->getUserId()]);
      } else {
        $oTopic->setUser(null); // или $oTopic->setUser(new ModuleUser_EntityUser());
      }
      if (isset($aBlogs[$oTopic->getBlogId()])) {
        $oTopic->setBlog($aBlogs[$oTopic->getBlogId()]);
      } else {
        $oTopic->setBlog(null); // или $oTopic->setBlog(new ModuleBlog_EntityBlog());
      }
      if (isset($aGames[$oTopic->getGameId()])) {
        $oTopic->setGame($aGames[$oTopic->getGameId()]);
      } else {
        $oTopic->setGame(null); // или $oTopic->setGame(new PluginGamerpress_ModuleGame_EntityGame());
      }
      if (isset($aTopicsVote[$oTopic->getId()])) {
        $oTopic->setVote($aTopicsVote[$oTopic->getId()]);
      } else {
        $oTopic->setVote(null);
      }
      if (isset($aFavouriteTopics[$oTopic->getId()])) {
        $oTopic->setFavourite($aFavouriteTopics[$oTopic->getId()]);
      } else {
        $oTopic->setFavourite(null);
      }
      if (isset($aTopicsQuestionVote[$oTopic->getId()])) {
        $oTopic->setUserQuestionIsVote(true);
      } else {
        $oTopic->setUserQuestionIsVote(false);
      }
      if (isset($aTopicsRead[$oTopic->getId()]))  {
        $oTopic->setCountCommentNew($oTopic->getCountComment()-$aTopicsRead[$oTopic->getId()]->getCommentCountLast());
        $oTopic->setDateRead($aTopicsRead[$oTopic->getId()]->getDateRead());
      } else {
        $oTopic->setCountCommentNew(0);
        $oTopic->setDateRead(date("Y-m-d H:i:s"));
      }
      if (isset($aPhotosetMainPhotos[$oTopic->getPhotosetMainPhotoId()])) {
        $oTopic->setPhotosetMainPhoto($aPhotosetMainPhotos[$oTopic->getPhotosetMainPhotoId()]);
      } else {
        $oTopic->setPhotosetMainPhoto(null);
      }
    }
    return $aTopics;
  }
}
?>
