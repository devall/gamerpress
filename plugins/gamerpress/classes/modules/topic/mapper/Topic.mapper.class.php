<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Объект маппера для работы с БД
 *
 * @package modules.topic
 * @since 0.1
 */
class PluginGamerpress_ModuleTopic_MapperTopic extends PluginGamerpress_Inherit_ModuleTopic_MapperTopic {
  /**
   * Добавляет топик
   *
   * @param ModuleTopic_EntityTopic $oTopic Объект топика
   * @return int|bool
   */
  public function AddTopic(ModuleTopic_EntityTopic $oTopic) {
    $sql = "INSERT INTO ".Config::Get('db.table.topic')."
      (blog_id,
      game_id,
      user_id,
      topic_type,
      topic_title,
      topic_tags,
      topic_date_add,
      topic_user_ip,
      topic_publish,
      topic_publish_draft,
      topic_publish_index,
      topic_cut_text,
      topic_forbid_comment,
      topic_text_hash
      )
      VALUES(?d, ?d, ?d,  ?,  ?,  ?,  ?, ?, ?d, ?d, ?d, ?, ?, ?)
    ";
    if ($iId=$this->oDb->query($sql,$oTopic->getBlogId(),$oTopic->getGameId(),$oTopic->getUserId(),$oTopic->getType(),$oTopic->getTitle(),
                   $oTopic->getTags(),$oTopic->getDateAdd(),$oTopic->getUserIp(),$oTopic->getPublish(),$oTopic->getPublishDraft(),$oTopic->getPublishIndex(),$oTopic->getCutText(),$oTopic->getForbidComment(),$oTopic->getTextHash()))
    {
      $oTopic->setId($iId);
      $this->AddTopicContent($oTopic);
      return $iId;
    }
    return false;
  }
  /**
   * Добавление тега к топику
   *
   * @param ModuleTopic_EntityTopicTag $oTopicTag Объект тега топика
   * @return int
   */
  public function AddTopicTag(ModuleTopic_EntityTopicTag $oTopicTag) {
    $sql = "INSERT INTO ".Config::Get('db.table.topic_tag')."
      (topic_id,
      user_id,
      blog_id,
      game_id,
      topic_tag_text
      )
      VALUES(?d,  ?d,  ?d,  ?d,  ?)
    ";
    if ($iId=$this->oDb->query($sql,$oTopicTag->getTopicId(),$oTopicTag->getUserId(),$oTopicTag->getBlogId(),$oTopicTag->getGameId(),$oTopicTag->getText()))
    {
      return $iId;
    }
    return false;
  }
  /**
   * Список топиков по фильтру
   *
   * @param  array $aFilter Фильтр
   * @param  int   $iCount  Возвращает общее число элементов
   * @param  int   $iCurrPage Номер страницы
   * @param  int   $iPerPage  Количество элементов на страницу
   * @return array
   */
  public function GetTopics($aFilter,&$iCount,$iCurrPage,$iPerPage) {
    $sWhere=$this->buildFilter($aFilter);

    if(!isset($aFilter['order'])) {
      $aFilter['order'] = 't.topic_date_add desc';
    }
    if (!is_array($aFilter['order'])) {
      $aFilter['order'] = array($aFilter['order']);
    }

    $sql = "SELECT
            t.topic_id
          FROM
            ".Config::Get('db.table.topic')." as t,
            ".Config::Get('db.table.blog')." as b,
            ".Config::Get('plugin.gamerpress.db.table.game')." as g
          WHERE
            1=1

            ".$sWhere."

            AND
            (
              t.blog_id=b.blog_id
            OR
              t.game_id=g.game_id
            )
          GROUP BY
            t.topic_id
          ORDER BY ".
      implode(', ', $aFilter['order'])
      ."
          LIMIT ?d, ?d";
    $aTopics=array();
    if ($aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage)) {
      foreach ($aRows as $aTopic) {
        $aTopics[]=$aTopic['topic_id'];
      }
    }
    return $aTopics;
  }
  /**
   * Количество топиков по фильтру
   *
   * @param array $aFilter  Фильтр
   * @return int
   */
  public function GetCountTopics($aFilter) {
    $sWhere=$this->buildFilter($aFilter);
    $sql = "SELECT
          count(t.topic_id) as count
        FROM
          ".Config::Get('db.table.topic')." as t,
          ".Config::Get('db.table.blog')." as b,
          ".Config::Get('plugin.gamerpress.db.table.game')." as g
        WHERE
          1=1

          ".$sWhere."

          AND
          (
            t.blog_id=b.blog_id
          OR
            t.game_id=g.game_id
          )
          GROUP BY
            t.topic_id
          ";
    if ($aRow=$this->oDb->selectRow($sql)) {
      return $aRow['count'];
    }
    return false;
  }
  /**
   * Строит строку условий для SQL запроса топиков
   *
   * @param array $aFilter  Фильтр
   * @return string
   */
  protected function buildFilter($aFilter) {
    $sWhere='';
    if (isset($aFilter['topic_date_more'])) {
      $sWhere.=" AND t.topic_date_add >  '".mysql_real_escape_string($aFilter['topic_date_more'])."'";
    }
    if (isset($aFilter['topic_publish'])) {
      $sWhere.=" AND t.topic_publish =  ".(int)$aFilter['topic_publish'];
    }
    if (isset($aFilter['topic_rating']) and is_array($aFilter['topic_rating'])) {
      $sPublishIndex='';
      if (isset($aFilter['topic_rating']['publish_index']) and $aFilter['topic_rating']['publish_index']==1) {
        $sPublishIndex=" or topic_publish_index=1 ";
      }
      if ($aFilter['topic_rating']['type']=='top') {
        $sWhere.=" AND ( t.topic_rating >= ".(float)$aFilter['topic_rating']['value']." {$sPublishIndex} ) ";
      } else {
        $sWhere.=" AND ( t.topic_rating < ".(float)$aFilter['topic_rating']['value']."  ) ";
      }
    }
    if (isset($aFilter['topic_new'])) {
      $sWhere.=" AND t.topic_date_add >=  '".$aFilter['topic_new']."'";
    }
    if (isset($aFilter['user_id'])) {
      $sWhere.=is_array($aFilter['user_id'])
        ? " AND t.user_id IN(".implode(', ',$aFilter['user_id']).")"
        : " AND t.user_id =  ".(int)$aFilter['user_id'];
    }
    if (isset($aFilter['blog_id'])) {
      if(!is_array($aFilter['blog_id'])) {
        $aFilter['blog_id']=array($aFilter['blog_id']);
      }
      $sWhere.=" AND t.blog_id IN ('".join("','",$aFilter['blog_id'])."')";
    }
    if (isset($aFilter['game_id'])) {
      if(!is_array($aFilter['game_id'])) {
        $aFilter['game_id']=array($aFilter['game_id']);
      }
      $sWhere.=" AND t.game_id IN ('".join("','",$aFilter['game_id'])."')";
    }
    if (isset($aFilter['blog_type']) and is_array($aFilter['blog_type'])) {
      $aBlogTypes = array();
      foreach ($aFilter['blog_type'] as $sType=>$aBlogId) {
        /**
         * Позиция вида 'type'=>array('id1', 'id2')
         */
        if(!is_array($aBlogId) && is_string($sType)){
          $aBlogId=array($aBlogId);
        }
        /**
         * Позиция вида 'type'
         */
        if(is_string($aBlogId) && is_int($sType)) {
          $sType=$aBlogId;
          $aBlogId=array();
        }

        $sInclude='';
        if($sType == 'personal') {
          $sInclude .= ' AND t.game_id IS NULL';
        } else if($sType == 'open') {
          $sInclude .= ' OR t.game_id IS NOT NULL';
        }

        $aBlogTypes[] = (count($aBlogId)==0)
          ? "(b.blog_type='".$sType."'".$sInclude.")"
          : "(b.blog_type='".$sType."'".$sInclude." AND t.blog_id IN ('".join("','",$aBlogId)."'))";
      }
      $sWhere.=" AND (".join(" OR ",(array)$aBlogTypes).")";
    }
    if (isset($aFilter['topic_type'])) {
      if(!is_array($aFilter['topic_type'])) {
        $aFilter['topic_type']=array($aFilter['topic_type']);
      }
      $sWhere.=" AND t.topic_type IN ('".join("','",array_map('mysql_real_escape_string',$aFilter['topic_type']))."')";
    }
    return $sWhere;
  }
}
?>
