<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Объект сущности топика
 *
 * @package modules.topic
 * @since 1.0
 */
class PluginGamerpress_ModuleTopic_EntityTopic extends PluginGamerpress_Inherit_ModuleTopic_EntityTopic {

  /**
   * Определяем правила валидации
   */
  public function Init() {
    parent::Init();
    $this->aValidateRules[]=array('game_id','game_id','on'=>array('topic','link','question','photoset'));
  }
  /**
   * Валидация ID игры
   *
   * @param string $sValue  Проверяемое значение
   * @param array $aParams  Параметры
   * @return bool|string
   */
  public function ValidateGameId($sValue,$aParams) {
    if ($sValue==0) {
      return true;
    }
    if ($this->PluginGamerpress_Game_GetGameById((string)$sValue)) {
      return true;
    }
    return $this->Lang_Get('plugin.gamerpress.topic_create_game_error_unknown');
  }

  /**
   * Возвращает объект игры, в котором находится топик
   *
   * @return PluginGamerpress_ModuleGame_EntityGame|null
   */
  public function getGame() {
    return $this->_getDataOne('game');
  }
  /**
   * Возвращает полный URL до топика
   *
   * @return string
   */
  public function getUrl() {
    if ($this->getGame()) {
      return Router::GetPath('game').$this->getGame()->getUrl().'/'.$this->getId().'.html';
    } else if ($this->getBlog() and $this->getBlog()->getType()=='personal') {
      return Router::GetPath('blog').$this->getId().'.html';
    } else {
      return Router::GetPath('blog').$this->getBlog()->getUrl().'/'.$this->getId().'.html';
    }
  }

  /**
   * Возвращает ID игры
   *
   * @return int|null
   */
  public function getGameId() {
    return $this->_getDataOne('game_id');
  }


  //*************************************************************************************************************************************************

  /**
   * Устанавливает ID игры
   *
   * @param int $data
   */
  public function setGameId($data) {
    $this->_aData['game_id']=$data;
  }
  /**
   * Устанавливает объект игры
   *
   * @param PluginGamerpress_ModuleGame_EntityGame $data
   */
  public function setGame($data) {
    $this->_aData['game']=$data;
  }
}
?>
