<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

/**
 * Объект сущности тега топика
 *
 * @package modules.topic
 * @since 1.0
 */
class PluginGamerpress_ModuleTopic_EntityTopicTag extends PluginGamerpress_Inherit_ModuleTopic_EntityTopicTag {

  /**
   * Возвращает ID игры
   *
   * @return int|null
   */
  public function getGameId() {
    return $this->_getDataOne('game_id');
  }

  /**
   * Устанавливает ID игры
   *
   * @param int $data
   */
  public function setGameId($data) {
    $this->_aData['game_id']=$data;
  }
}
?>
