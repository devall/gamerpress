<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Модуль управления рейтингами и силой
 *
 * @package modules.rating
 * @since 0.1
 */
class PluginGamerpress_ModuleRating extends PluginGamerpress_Inherit_ModuleRating {

  /**
   * Расчет рейтинга и силы при голосовании за игру
   *
   * @param ModuleUser_EntityUser $oUser  Объект пользователя, который голосует
   * @param PluginGamerpress_ModuleGame_EntityGame $oGame  Объект игры
   * @param int $iValue
   * @return int
   */
  public function VoteGame(ModuleUser_EntityUser $oUser, PluginGamerpress_ModuleGame_EntityGame $oGame, $iValue) {
    /**
     * Устанавливаем рейтинг игры
     */
    $iValue = $oGame->getRating()+$iValue;
    /**
     * Сохраняем рейтинг
     */
    $oGame->setRating($iValue);
    return $iValue;
  }
}
?>
