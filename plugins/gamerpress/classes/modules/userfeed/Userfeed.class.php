<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Модуль пользовательских лент контента (топиков)
 *
 * @package modules.userfeed
 * @since 0.1
 */
class PluginGamerpress_ModuleUserfeed extends PluginGamerpress_Inherit_ModuleUserfeed {
  /**
   * Подписки на топики по игре
   */
  const SUBSCRIBE_TYPE_GAME = 3;
  /**
   * Получить список подписок пользователя
   *
   * @param int $iUserId ID пользователя, для которого загружаются подписки
   * @return array
   */
  public function getUserSubscribes($iUserId) {
    $aUserSubscribes = $this->oMapper->getUserSubscribes($iUserId);
    $aResult = array('blogs' => array(), 'users' => array(), 'games' => array());
    if (count($aUserSubscribes['blogs'])) {
      $aBlogs = $this->Blog_GetBlogsByArrayId($aUserSubscribes['blogs']);
      foreach ($aBlogs as $oBlog) {
        $aResult['blogs'][$oBlog->getId()] = $oBlog;
      }
    }
    if (count($aUserSubscribes['users'])) {
      $aUsers = $this->User_GetUsersByArrayId($aUserSubscribes['users']);
      foreach ($aUsers as $oUser) {
        $aResult['users'][$oUser->getId()] = $oUser;
      }
    }
    if (count($aUserSubscribes['games'])) {
      $aGames = $this->PluginGamerpress_Game_GetGamesByArrayId($aUserSubscribes['games']);
      foreach ($aGames as $oGame) {
        $aResult['games'][$oGame->getId()] = $oGame;
      }
    }

    return $aResult;
  }
}
