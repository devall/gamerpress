<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Маппер для работы с БД
 *
 * @package modules.userfeed
 * @since 0.1
 */
class PluginGamerpress_ModuleUserfeed_MapperUserfeed extends PluginGamerpress_Inherit_ModuleUserfeed_MapperUserfeed {
  /**
   * Получить список подписок пользователя
   *
   * @param int $iUserId ID пользователя, для которого загружаются подписки
   * @return array
   */
  public function getUserSubscribes($iUserId) {
    $sql = 'SELECT subscribe_type, target_id FROM ' . Config::Get('db.table.userfeed_subscribe') . ' WHERE user_id = ?d';
    $aSubscribes  = $this->oDb->select($sql, $iUserId);
    $aResult = array('blogs' => array(), 'users' => array());

    if (!count($aSubscribes)) return $aResult;

    foreach ($aSubscribes as $aSubscribe) {
      if($aSubscribe['subscribe_type'] == ModuleUserfeed::SUBSCRIBE_TYPE_BLOG) {
        $aResult['blogs'][] = $aSubscribe['target_id'];
      } elseif ($aSubscribe['subscribe_type'] == ModuleUserfeed::SUBSCRIBE_TYPE_USER) {
        $aResult['users'][] = $aSubscribe['target_id'];
      } elseif ($aSubscribe['subscribe_type'] == PluginGamerpress_ModuleUserfeed::SUBSCRIBE_TYPE_GAME) {
        $aResult['games'][] = $aSubscribe['target_id'];
      }
    }
    return $aResult;
  }
  /**
   * Получить ленту топиков по подписке
   *
   * @param array $aUserSubscribes Список подписок пользователя
   * @param int $iCount Число получаемых записей (если null, из конфига)
   * @param int $iFromId Получить записи, начиная с указанной
   * @return array
   */
  public function readFeed($aUserSubscribes, $iCount, $iFromId) {
    /*$sql = "
              SELECT
                t.topic_id
              FROM
                ".Config::Get('db.table.topic')." as t,
                ".Config::Get('db.table.blog')." as b,
                ".Config::Get('plugin.gamerpress.db.table.game')." as g
              WHERE
                t.topic_publish = 1
                { AND t.topic_id < ?d }
                AND
                ((
                  t.blog_id=b.blog_id
                  AND
                  b.blog_type!='close'
                  { AND t.blog_id IN (?a) }
                )
                OR
                (t.game_id=g.game_id
                  { AND t.game_id IN (?a) }
                ))

                  { AND t.user_id IN (?a) }
                            ORDER BY t.topic_id DESC
                            { LIMIT 0, ?d }";*/
$sql = "
              SELECT
                t.topic_id
              FROM
                ".Config::Get('db.table.topic')." as t,
                ".Config::Get('db.table.blog')." as b,
                ".Config::Get('plugin.gamerpress.db.table.game')." as g
              WHERE
                t.topic_publish = 1
                { AND t.topic_id < ?d }
                AND
                ((
                  t.blog_id=b.blog_id
                  AND
                  b.blog_type!='close'
                )
                OR
                  t.game_id=g.game_id
                )

                  AND ( 1=0 { OR t.blog_id IN (?a) } { OR t.user_id IN (?a) } { OR t.game_id IN (?a) } )
                            GROUP BY t.topic_id
                            ORDER BY t.topic_id DESC
                            { LIMIT 0, ?d }";
    $aTopics=$aTopics=$this->oDb->selectCol($sql,
                        $iFromId ? $iFromId : DBSIMPLE_SKIP,
                        count($aUserSubscribes['blogs']) ? $aUserSubscribes['blogs'] : DBSIMPLE_SKIP,
                        count($aUserSubscribes['users']) ? $aUserSubscribes['users'] : DBSIMPLE_SKIP,
                        count($aUserSubscribes['games']) ? $aUserSubscribes['games'] : DBSIMPLE_SKIP,
                        $iCount ? $iCount : DBSIMPLE_SKIP
    );
    return $aTopics;
  }
}
