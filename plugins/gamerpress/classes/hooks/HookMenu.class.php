<?php

/*-------------------------------------------------------
*
*   Copyright © 2012 Alexey Yelizarov
*
---------------------------------------------------------
*/

/**
 * Хуки меню
 *
 * @package hooks
 * @since 0.1
 */
class PluginGamerpress_HookMenu extends Hook {

    /*
     * Регистрация событий на хуки
     */
    public function RegisterHook() {
      // header_top.tpl
      $this->AddHook('template_main_menu_item','MainItem',null,100);
      // menu.create.content.tpl
      $this->AddHook('template_menu_create_item_select','CreateItemSelect');
      $this->AddHook('template_menu_create_item','CreateItem');
    }

    public function MainItem() {
      return $this->Viewer_Fetch(Plugin::GetTemplatePath(__CLASS__).'/menu.main_item.tpl');
    }

    public function CreateItemSelect() {
      return $this->Viewer_Fetch(Plugin::GetTemplatePath(__CLASS__).'/menu.create_item_select.tpl');
    }
    public function CreateItem() {
      return $this->Viewer_Fetch(Plugin::GetTemplatePath(__CLASS__).'/menu.create_item.tpl');
    }
}
?>
