<?php
/*-------------------------------------------------------
*
*   Copyright © 2012 Alexei Yelizarov
*
*--------------------------------------------------------
*/

/**
 * Блок настройки списка игр в ленте
 *
 * @package blocks
 * @since 0.1
 */
class PluginGamerpress_BlockUserfeedGames extends Block {
  /**
   * Запуск обработки
   */
  public function Exec() {
    /**
     * Пользователь авторизован?
     */
    if ($oUserCurrent = $this->User_getUserCurrent()) {
      $aUserSubscribes = $this->Userfeed_getUserSubscribes($oUserCurrent->getId());
      /**
       * Получаем список ID игр, в которых состоит пользователь
       */
      $aGamesId = $this->PluginGamerpress_Game_GetGameUsersByUserId($oUserCurrent->getId(), array(PluginGamerpress_ModuleGame::GAME_USER_ROLE_USER,PluginGamerpress_ModuleGame::GAME_USER_ROLE_MODERATOR,PluginGamerpress_ModuleGame::GAME_USER_ROLE_ADMINISTRATOR),true);
      /**
       * Получаем список ID игр, которые создал пользователь
       */
      $aGamesOwnerId=$this->PluginGamerpress_Game_GetGamesByOwnerId($oUserCurrent->getId(),true);
      $aGamesId=array_merge($aGamesId,$aGamesOwnerId);

      $aGames=$this->PluginGamerpress_Game_GetGamesAdditionalData($aGamesId,array('owner'=>array()),array('game_title'=>'asc'));
      /**
       * Выводим в шаблон
       */
      $this->Viewer_Assign('aUserfeedSubscribedGames', $aUserSubscribes['games']);
      $this->Viewer_Assign('aUserfeedGames', $aGames);
    }
  }
}
