<?php

$config=array();



$config['block_heroes_count'] = 10;
$config['ratio'] = 0.95;


/*
Config::Set('block.rule_heroes' , array(
    'path'  => array(
        '___path.root.web___/$',
    ),
    'action'  => array('index'),
    'blocks'  => array(
        'right' => array(
            'heroes'=>array('params'=>array('plugin'=>'level'), 'priority'=>10)
        )
    )
));
*/

$config['levels'] = array(
    'acl.create.blog.rating' => Config::Get('acl.create.blog.rating'),                 // 1 порог рейтинга при котором юзер может создать коллективный блог
    'acl.create.comment.rating' => Config::Get('acl.create.comment.rating'),           // -10 порог рейтинга при котором юзер может добавлять комментарии
    'acl.create.comment.limit_time_rating' => Config::Get('acl.create.comment.limit_time_rating'),// -1 рейтинг, выше которого перестаёт действовать ограничение по времени на постинг комментов. Не имеет смысла при $config['acl']['create']['comment']['limit_time']=0
    'acl.create.topic.limit_time_rating' => Config::Get('acl.create.topic.limit_time_rating'),    // 5 рейтинг, выше которого перестаёт действовать ограничение по времени на создание записей
    'acl.create.talk.limit_time_rating' => Config::Get('acl.create.talk.limit_time_rating'),      // 1 рейтинг, выше которого перестаёт действовать ограничение по времени на отправку инбоксов
    'acl.create.talk_comment.limit_time_rating' => Config::Get('acl.create.talk_comment.limit_time_rating'), // 5 рейтинг, выше которого перестаёт действовать ограничение по времени на отправку инбоксов
    'acl.vote.comment.rating' => Config::Get('acl.vote.comment.rating'),           // -3 порог рейтинга при котором юзер может голосовать за комментарии
    'acl.vote.blog.rating' => Config::Get('acl.vote.blog.rating'),                 // -5 порог рейтинга при котором юзер может голосовать за блог
    'acl.vote.topic.rating' => Config::Get('acl.vote.topic.rating'),               // -7 порог рейтинга при котором юзер может голосовать за топик
    'acl.vote.user.rating' => Config::Get('acl.vote.user.rating'),                 // -1 порог рейтинга при котором юзер может голосовать за пользователя
);
return $config;
?>
