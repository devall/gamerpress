<?php

class PluginLevel_BlockHeroes extends Block {
    public function Exec() {
        if($aResult=$this->User_GetUsersByFilter('good',1,Config::Get('plugin.level.block_heroes_count'))) {
            $aUsersRating=$aResult['collection'];

            foreach($aUsersRating as $oUserRating) {
                $iLevel = $this->PluginLevel_Level_GetUserLevel($oUserRating);
                $oUserRating->setLevel($iLevel);
            }

            $oViewer=$this->Viewer_GetLocalViewer();
            $oViewer->Assign('aUsersRating',$aUsersRating);
            $sTextResult=$oViewer->Fetch(Plugin::GetTemplatePath(__CLASS__) . "block.heroes_result.tpl");
            $this->Viewer_Assign('sUsersRating',$sTextResult);
        }
    }
}
?>
