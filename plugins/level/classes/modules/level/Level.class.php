<?php

class PluginLevel_ModuleLevel extends Module {

    public function Init() {

    }

    protected function RatingToLevel($fRating) {
        //return $fRating + log($fRating);
        return pow($fRating,Config::Get('plugin.level.ratio'));
    }

    protected function FractionValue($fRating) {
        $fLevel = $this->RatingToLevel($fRating);
        return $fLevel - floor($fLevel);
    }

    public function GetUserLevel($oUser,$iMin=null,$iMax=null) {
        $iId = $oUser->getId();

        if (false === ($iLevel = $this->Cache_Get("user_level_{$iId}"))) {
            $iRating = $oUser->getRating();
            $aConfig = Config::Get('plugin.level.levels');

            if(!($iMin and $iMax)) {
                // Минимум, максимум
                $iMin = 0;
                foreach($aConfig as $value) {
                    if((!$iMax and $value > $iRating) or ($value >= $iRating and $value < $iMax) ) {
                        $iMax = $value;
                    } else if((!$iMin and $value < $iRating) or ($value <= $iRating and $value > $iMin) ) {
                        $iMin = $value;
                    }
                }
            }

            // Уровень
            sort($aConfig);
            $aConfig = array_unique($aConfig);

            foreach($aConfig as $key=>$value) {
                if($value > 0 ) {
                    $aPlusConfig[] = $value;
                } else {
                    $aMinusConfig[] = $value;
                }
            }

            if(!$iMax) {
                $iLevel = floor($this->RatingToLevel($iRating - $iMin) + count($aPlusConfig));
            } else {
                rsort($aMinusConfig);
                if($iRating >= 0 ) {
                    foreach($aPlusConfig as $key=>$value) {
                        if($value == $iRating) {
                            $iLevel = $key+1;
                            break;
                        }

                        if($iMin < 0) {
                            $iLevel = $key;
                            break;
                        }

                        if($iRating <= $value and $value <= $iMax) {
                            $iLevel = $key;
                            break;
                        }
                        $iLevel = $key+1;
                    }
                } else {
                    foreach($aMinusConfig as $key=>$value) {
                        if($value == $iRating) {
                            $iLevel = -$key-1;
                            break;
                        }

                        if($iMax > 0) {
                            $iLevel = -1;
                            break;
                        }

                        if($iRating >= $value and $value >= $iMin) {
                            $iLevel = -$key-1;
                            break;
                        }
                        $iLevel = -$key;
                    }
                }
            }

            $this->Cache_Set($iLevel, "user_level_{$iId}", array('user_update'), 60*60*24*1);
        }

        return $iLevel;
    }
}
?>
