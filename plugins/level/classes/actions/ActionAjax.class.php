<?php

class PluginLevel_ActionAjax extends PluginLevel_Inherit_ActionAjax {


    protected function RegisterEvent() {
        parent::RegisterEvent();
        $this->AddEventPreg('/^level$/i','/^good$/','EventLevelGood');
        $this->AddEventPreg('/^level$/i','/^bad$/','EventLevelBad');
    }

    protected function EventLevelGood() {
        if($aResult=$this->User_GetUsersRating('good',1,Config::Get('plugin.level.block_heroes_count'))) {
            $aUsersRating=$aResult['collection'];

            foreach($aUsersRating as $oUserRating) {
                $iLevel = $this->PluginLevel_Level_GetUserLevel($oUserRating);
                $oUserRating->setLevel($iLevel);
            }

            $oViewer=$this->Viewer_GetLocalViewer();
            $oViewer->Assign('aUsersRating',$aUsersRating);
            $sTextResult=$oViewer->Fetch(Plugin::GetTemplatePath(__CLASS__) . "block.heroes_result.tpl");
            $this->Viewer_AssignAjax('sText',$sTextResult);
        } else {
            $this->Message_AddErrorSingle($this->Lang_Get('plugin.level.level_block_heroes_good_no'),$this->Lang_Get('attention'));
            return;
        }
    }

    protected function EventLevelBad() {
        if($aResult=$this->User_GetUsersRating('bad',1,Config::Get('plugin.level.block_heroes_count'))) {
            $aUsersRating=$aResult['collection'];

            foreach($aUsersRating as $oUserRating) {
                $iLevel = $this->PluginLevel_Level_GetUserLevel($oUserRating);
                $oUserRating->setLevel($iLevel);
            }

            $oViewer=$this->Viewer_GetLocalViewer();
            $oViewer->Assign('aUsersRating',$aUsersRating);
            $sTextResult=$oViewer->Fetch(Plugin::GetTemplatePath(__CLASS__) . "block.heroes_result.tpl");
            $this->Viewer_AssignAjax('sText',$sTextResult);
        } else {
            $this->Message_AddErrorSingle($this->Lang_Get('plugin.level.level_block_heroes_bad_no'),$this->Lang_Get('attention'));
            return;
        }
    }

}
?>
