<?php

/**
 * Регистрация хука для вывода меню страниц
 *
 */
class PluginLevel_HookLevel extends Hook {
    public function RegisterHook() {
        $this->AddHook('template_profile_whois_item_after_privat','ShowLevel');
    }

    protected function RatingToLevel($fRating) {
        //return $fRating + log($fRating);
        return pow($fRating,0.95);
    }

    protected function FractionValue($fRating) {
        $fLevel = $this->RatingToLevel($fRating);
        return $fLevel - floor($fLevel);
    }

    public function ShowLevel($aParams) {
        $oUserProfile = $aParams['oUserProfile'];
        $aConfig = Config::Get('plugin.level.levels');
        $iRating = $oUserProfile->getRating();

        // Минимум, максимум
        $iMin = 0;
        foreach($aConfig as $value) {
            if((!$iMax and $value > $iRating) or ($value >= $iRating and $value < $iMax) ) {
                $iMax = $value;
            } else if((!$iMin and $value < $iRating) or ($value <= $iRating and $value > $iMin) ) {
                $iMin = $value;
            }
        }

        // Имена языков
        reset($aConfig);
        while ($key = key($aConfig)) {
            if($iMax == $aConfig[$key]) {
                $aLangs[] = $key;
            }
            next($aConfig);
        }

        // Подгрузка языков
        if($aLangs) {
            foreach($aLangs as $sLang) {
                $aFutures[] = $this->Lang_Get('plugin.level.level_'.str_replace('.','_',$sLang));
            }
        }

        // Позиция
        if($iMax and $iMax >= $iRating) {
            if($iRating > 0) {
                if($iMin < 0) $iMin=0;
                $iDelta = $iMax - $iMin;

                if($iDelta == 0)
                    $iPos = 0;
                else
                    $iPos = ($iRating - $iMin) / (($iDelta) / 100);
            } else {
                if($iMax > 0) $iMax=0;
                $iDelta = $iMax - $iMin;

                if($iDelta == 0)
                    $iPos = 0;
                else
                    $iPos = 100 - ($iRating - $iMin) / (($iDelta) / 100);
            }
        } else {
            $iDelta = $this->FractionValue($iRating - $iMin);
            $iPos = $iDelta * 100;
        }

        if($iPos == 100) $iPos = 0;

        $iLevel = $this->PluginLevel_Level_GetUserLevel($oUserProfile);
        $this->Viewer_Assign('iPos',number_format($iPos,2,'.',' '));
        $this->Viewer_Assign('aFutures',$aFutures);
        $this->Viewer_Assign('iLevel',$iLevel);

        return $this->Viewer_Fetch(Plugin::GetTemplatePath(__CLASS__).'level_show.tpl');
    }
}
?>
