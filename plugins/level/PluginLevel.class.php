<?php

/**
 * Запрещаем напрямую через браузер обращение к этому файлу.
 */
if (!class_exists('Plugin')) {
    die('Hacking attempt!');
}

class PluginLevel extends Plugin {

    protected $aInherits = array(
        'action' => array(
            'ActionAjax'
        ),
    );

    public function Activate() {
        return true;
    }

    public function Init() {
        $this->Viewer_AppendStyle(Plugin::GetTemplatePath(__CLASS__).'css/style.css');
        $this->Viewer_AppendScript(Plugin::GetTemplatePath(__CLASS__).'js/script.js');
    }
}
?>
