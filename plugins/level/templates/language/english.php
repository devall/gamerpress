<?php

return array(
    'level_acl_create_blog_rating' => 'Создавать коллективные блоги',
    'level_acl_create_comment_rating' => 'Добавлять комментарии',
    'level_acl_create_comment_limit_time_rating' => 'Снятие ограничений по времени на добавление комментариев',
    'level_acl_create_topic_limit_time_rating' => 'Снятие ограничений по времени на создание записей',
    'level_acl_create_talk_limit_time_rating' => 'Снятие ограничений по времени на отправку инбоксов',
    'level_acl_create_talk_comment_limit_time_rating' => 'Снятие ограничений по времени на комментирование инбоксов',
    'level_acl_vote_comment_rating' => 'Голосовать за комментарии',
    'level_acl_vote_blog_rating' => 'Голосовать за блог',
    'level_acl_vote_topic_rating' => 'Голосовать за топик',
    'level_acl_vote_user_rating' => 'Голосовать за пользователя',

    'level_level' => 'Уровень',

    'level_block_heroes' => 'Пользователи',
    'level_block_heroes_all' => 'Все пользователи',
    'level_block_heroes_good' => 'Позитивные',
    'level_block_heroes_bad' => 'Негативные',
    'level_block_heroes_good_no' => 'Отсутствуют позитивные пользователи',
    'level_block_heroes_bad_no' => 'Отсутствуют негативные пользователи',
);

?>
