<div class="level">
    <span class="var">{$aLang.plugin.level.level_level}:<h2>{$iLevel}</h2></span>
    <!--<div class="stats"></div>-->
    <div style="text-align: center; font-family: Helvetica, Sans-serif, Verdana; font-size: 11px; color: #888;">Осталось до уровня {if $iLevel > 0}{100-$iPos}{else}{$iPos+0}{/if}%</div>
    <div class="progress"><i class="{if $iLevel >= 0}plus{else}minus{/if}" style="{if $iPos > 0}width: {$iPos}%;{else}display: none;{/if}"></i></div>

    <div  style="font-family: Helvetica, Sans-serif, Verdana; font-size: 11px;">
        {if $aFutures} {* and $oUserCurrent->getId() == $oUserProfile->getId() *}
            При достижение уровня у вас появится возможность:<br>
            <ul>
            {foreach from=$aFutures item=sFuture}
                <li>- {$sFuture}</li>
            {/foreach}
            </ul>
        {/if}
    </div>
</div>
