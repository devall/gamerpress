<div class="block heroes" id="block_heroes">
    <h2>{$aLang.plugin.level.level_block_heroes}</h2>


    <ul class="switcher">
        <li id="block_heroes_item_good" class="active">{$aLang.plugin.level.level_block_heroes_good}</li>
        <li id="block_heroes_item_bad">{$aLang.plugin.level.level_block_heroes_bad}</li>
    </ul>

    <div class="block-content" id="block_heroes_content">
        {$sUsersRating}
    </div>

    <script>
        jQuery(document).ready(function($){
            $('[id^="block_heroes_item"]').click(function(){
                ls.blocks.load(this, 'block_heroes');
                return false;
            });
        });
    </script>
</div>


