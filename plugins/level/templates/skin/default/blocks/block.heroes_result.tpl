{if $aUsersRating}
    <table class="table table-people">
        <thead>
            <tr>
                <td>{$aLang.user}</td>
                <td align="center" width="40">{$aLang.user_skill}</td>
                <!--<td align="center" width="80">{$aLang.user_rating}</td>-->
                <td align="center" width="80">{$aLang.level_level}</td>
            </tr>
        </thead>

        <tbody>
        {foreach from=$aUsersRating item=oUser}
            <tr>
                <td><a href="{$oUser->getUserWebPath()}"><img src="{$oUser->getProfileAvatarPath(24)}" alt="" class="avatar" /></a><a href="{$oUser->getUserWebPath()}" class="username">{$oUser->getLogin()}</a></td>
                <td align="center" class="strength">{$oUser->getSkill()}</td>
                <!--<td align="center" class="rating"><strong>{$oUser->getRating()}</strong></td>-->
                <td align="center" class="level"><strong>{$oUser->getLevel()}</strong></td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{else}
    {$aLang.user_empty}
{/if}

<div class="bottom">
    <a href="{router page='people'}">{$aLang.level_block_heroes_all}</a>
</div>
